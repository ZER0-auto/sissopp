cmake_minimum_required(VERSION 3.20.2)

# Include External Project options
include( ExternalProject )

# set the project name
project(sisso++ VERSION 1.1 LANGUAGES CXX C)

# Cmake modules/macros are in a subdirectory to keep this file cleaner
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

enable_testing()
option(SISSO_ENABLE_TESTING "Enable SISSO tests" ON)

# Check if CMAKE_INSTALL_PREFIX is ste and if not set it to a default
if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
  set(CMAKE_INSTALL_PREFIX "${CMAKE_CURRENT_SOURCE_DIR}" CACHE PATH "..." FORCE)
endif()

# Compiler tests
include(CheckCXXCompilerFlag)

# Check for C++17 standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

# Get the compiler toolset from the Compiler ID
include(${CMAKE_SOURCE_DIR}/cmake/get_toolset_name.cmake)
get_toolset_name(COMPILER_TOOLSET ${CMAKE_CXX_COMPILER_ID})

set(COMPILER_VERSION "${CMAKE_CXX_COMPILER_VERSION}")
if(COMPILER_TOOLSET STREQUAL "intel-llvm-linux")
    set(BOOST_TOOLSET "intel-linux")
elseif(COMPILER_TOOLSET STREQUAL "intel-llvm-darwin")
    set(BOOST_TOOLSET "intel-llvm-darwin")
else()
    set(BOOST_TOOLSET "${COMPILER_TOOLSET}")
endif()
message(STATUS "Will use the ${COMPILER_TOOLSET} (version: ${COMPILER_VERSION}) compiler toolset.")

# Add the libraries stored in third_party into CMAKE_PREFIX_PATH
set(CMAKE_PREFIX_PATH "${CMAKE_PREFIX_PATH};${CMAKE_CURRENT_LIST_DIR}/third_party/boost/1.79.0/${COMPILER_TOOLSET}/${COMPILER_VERSION}/")
list(APPEND CMAKE_PREFIX_PATH "${CMAKE_CURRENT_LIST_DIR}/third_party/fmt/${COMPILER_TOOLSET}/${COMPILER_VERSION}/")
list(APPEND CMAKE_PREFIX_PATH "${CMAKE_CURRENT_LIST_DIR}/third_party/gtest/${COMPILER_TOOLSET}/${COMPILER_VERSION}/")
list(APPEND CMAKE_PREFIX_PATH "${CMAKE_CURRENT_LIST_DIR}/third_party/nlopt/${COMPILER_TOOLSET}/${COMPILER_VERSION}/")
list(APPEND CMAKE_PREFIX_PATH "${CMAKE_CURRENT_LIST_DIR}/third_party/coin-or/${COMPILER_TOOLSET}/${COMPILER_VERSION}/")
list(APPEND CMAKE_PREFIX_PATH "${CMAKE_CURRENT_LIST_DIR}/third_party/Caliper/${COMPILER_TOOLSET}/${COMPILER_VERSION}/")

# Remove all warnings from CXX_FLAGS for compiling third party codes
string(REPLACE " " ";" LIST_CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")

# If use intel-llvm compilers ensure you are using precise fp-model
if ((COMPILER_TOOLSET STREQUAL "intel-llvm-linux")  OR (COMPILER_TOOLSET STREQUAL "intel-llvm-darwin"))
    message(STATUS "Ensuring -fp-model is set to precise.")
    list(FILTER LIST_CMAKE_CXX_FLAGS EXCLUDE REGEX "^-fp-model+")
    list(FILTER LIST_CMAKE_CXX_FLAGS EXCLUDE REGEX "^-ffp-model+")
    list(APPEND LIST_CMAKE_CXX_FLAGS "-fp-model=precise")
    string(REPLACE ";" " " CMAKE_CXX_FLAGS "${LIST_CMAKE_CXX_FLAGS}")
endif()
list(FILTER LIST_CMAKE_CXX_FLAGS EXCLUDE REGEX "^-W+")
list(FILTER LIST_CMAKE_CXX_FLAGS EXCLUDE REGEX "^-w+")
string(REPLACE ";" " " LIST_CMAKE_CXX_FLAGS "${NO_WARN_CMAKE_CXX_FLAGS}")

# Set base directory for external libraries if built by this build system
set(EXTERNAL_LIB_INST_DIR "${CMAKE_INSTALL_PREFIX}/lib/")

# Compiler Options
option(BUILD_SHARED_LIBS "If ON then build shared libraries" ON)
option(SETUP_PY "If ON then building using setup.py and not CMake directly" OFF)
option(BUILD_PYTHON "Whether to compile with python binding support" ON)
option(BUILD_PARAMS "If true use non-linear parameterization" OFF)
option(BUILD_CALIPER "If true use Caliper profiler" OFF)
option(BUILD_TESTS "Whether to compile C++ unit tests" OFF)
option(BUILD_EXE "Whether to compile executable" ON)
option(SISSO_ENABLE_CUDA "Enable CUDA support" OFF)
option(THIRD_PARTY_USE_SYS_PATH "Enable searching the system path for third_party_libraries" ON)

if(BUILD_SHARED_LIBS)
    message(STATUS "Building dynamiclly linked libraries")
    set(BUILD_SHARED_LIBS TRUE)
else()
    message(STATUS "Building statically linked libraries")
    set(BUILD_SHARED_LIBS FALSE)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")
    set(NO_WARN_CMAKE_CXX_FLAGS "${NO_WARN_CMAKE_CXX_FLAGS} -fPIC")
endif()

if(BUILD_SHARED_LIBS AND SETUP_PY)
    message(FATAL_ERROR "Using SETUP_PY=ON, BUILD_SHARED_LIBS must be OFF")
endif()

if(BUILD_PARAMS)
    message(STATUS "Building with support for parameterized features.")
    set(BUILD_PARAMS TRUE)
else(BUILD_PARAMS)
    set(BUILD_PARAMS FALSE)
endif()

if(BUILD_CALIPER)
    message(STATUS "Building with support for profiling with Caliper.")
    set(BUILD_CALIPER TRUE)
else(BUILD_CALIPER)
    set(BUILD_CALIPER FALSE)
endif()

if(BUILD_PYTHON)
    message(STATUS "Building the python bindings")
    set(BUILD_PYTHON TRUE)
else(BUILD_PYTHON)
    set(BUILD_PYTHON FALSE)
endif()

if(BUILD_EXE)
    message(STATUS "Building the executable")
    set(BUILD_EXE TRUE)
else(BUILD_EXE)
    set(BUILD_EXE FALSE)
endif()

if(BUILD_TESTS)
    message(STATUS "Building C++ unit tests")
    set(BUILD_TESTS TRUE)
else(BUILD_TESTS)
    set(BUILD_TESTS FALSE)
endif()

if(THIRD_PARTY_USE_SYS_PATH)
    set(THIRD_PARTY_USE_NO_SYS_PATH FALSE)
    set(Boost_NO_SYSTEM_PATHS FALSE)
else(THIRD_PARTY_USE_SYS_PATH)
    message(STATUS "Not using the system path for third-party applications")
    set(THIRD_PARTY_USE_NO_SYS_PATH TRUE)
    set(Boost_NO_SYSTEM_PATHS TRUE)
endif()

# Set the number of external build processes to use
if(NOT DEFINED EXTERNAL_BUILD_N_PROCS)
    set(EXTERNAL_BUILD_N_PROCS 1 CACHE STRING "Number of processes to use when building external projects.")
endif()
message(STATUS "Using ${EXTERNAL_BUILD_N_PROCS} processes to build external projects.")

# Check for OpenMP
find_package(OpenMP REQUIRED)
if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    if((OpenMP_CXX_SPEC_DATE GREATER_EQUAL "201511"))
        message(STATUS "Using OpenMP 4.5 directives")
    else()
        message(STATUS "Not using OpenMP 4.5 directives: ${OpenMP_C_VERSION_MAJOR} ${OpenMP_C_VERSION_MINOR}")
    endif()
endif(OPENMP_FOUND)

# Check for Python
if(BUILD_PYTHON)
    # Check python  settings
    find_package(PythonInterp 3 REQUIRED)

    if(NOT PYTHON_INSTDIR)
        execute_process(
            COMMAND ${PYTHON_EXECUTABLE}
            -c "import distutils.sysconfig as cg; print(cg.get_python_lib(1,0))"
            OUTPUT_VARIABLE PYTHON_INSTDIR OUTPUT_STRIP_TRAILING_WHITESPACE
        )
    endif()

    execute_process(
        COMMAND ${PYTHON_EXECUTABLE}
        -c "import distutils.sysconfig as cg; print(cg.PREFIX)"
        OUTPUT_VARIABLE PYTHON_PREFIX
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
endif()

# Check for BLAS/LAPACK libraries giving preference to MKL
set(BLA_VENDOR Intel10_64lp)

find_package(LAPACK)
if(NOT LAPACK_FOUND)
    set(BLA_VENDOR Intel)
    find_package(LAPACK)
    if(NOT LAPACK_FOUND)
        set(BLA_VENDOR All)
        find_package(LAPACK REQUIRED)
    endif()
endif()

list(GET LAPACK_LIBRARIES 0 LAPACK_LIBRARY)
get_filename_component(LAPACK_DIR ${LAPACK_LIBRARY} DIRECTORY)

# Check for MPI
find_package(MPI REQUIRED)
include_directories(${MPI_CXX_INCLUDE_DIRS})

set(MPI_LIBRARIES, ${MPI_CXX_LIBRARIES})
list(GET MPI_CXX_LIBRARIES 0 MPI_LIBRARY)
get_filename_component(MPI_DIR ${MPI_LIBRARY} DIRECTORY)

# Set the RPATH for the libraries and executables
set(CMAKE_INSTALL_RPATH ${LAPACK_DIR};${MPI_DIR};${CMAKE_INSTALL_PREFIX}/lib/)

# Add the src directories as includes
include_directories(${CMAKE_CURRENT_LIST_DIR}/src)

# Add libsvm header as a system header
include_directories(SYSTEM ${CMAKE_CURRENT_LIST_DIR}/src/external/libsvm/)

# Setup the building of the SISSO++ libraries/executables
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/src)
if(BUILD_TESTS)
    add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/tests/googletest/)
endif()

# Use Doxygen for building documentation from cmake
# look for Doxygen package
find_package(Doxygen)
if (DOXYGEN_FOUND)
    # set input and output files
    set(DOXYGEN_IN ${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in)
    set(DOXYGEN_OUT ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile.out)

    # request to configure the file
    configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)
    message(STATUS "Doxygen build started")

    # Note: do not put "ALL" - this builds docs together with application EVERY TIME!
    add_custom_target( docs
        COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Generating API documentation with Doxygen"
        VERBATIM )
else (DOXYGEN_FOUND)
    message(STATUS "Doxygen needs to be installed to generate the doxygen documentation")
endif (DOXYGEN_FOUND)

# Check for third party codes and if they are not found then build them

set(NO_WARN_CMAKE_CXX_FLAGS "${NO_WARN_CMAKE_CXX_FLAGS} -Wno-deprecated-declarations")

# Check for the Boost libraries
find_package(Boost COMPONENTS filesystem system mpi serialization)

if(Boost_FOUND)
    set(Boost_LIBS ${Boost_LIBRARIES})
    get_filename_component(Boost_LIBRARY_DIRS ${Boost_INCLUDE_DIRS} DIRECTORY)
    set(Boost_LIBRARY_DIRS "${Boost_LIBRARY_DIRS}/lib/")
else(Boost_FOUND)
    set(Boost_URL "https://boostorg.jfrog.io/artifactory/main/release/1.79.0/source/boost_1_79_0.tar.bz2")
    set(Boost_SHA256 "475d589d51a7f8b3ba2ba4eda022b170e562ca3b760ee922c146b6c65856ef39")
    set(Boost_INSTALL_DIR ${CMAKE_CURRENT_BINARY_DIR}/external/boost)
    set(Boost_INCLUDE_DIRS ${Boost_INSTALL_DIR}/include)
    set(Boost_LIBRARY_DIRS "${EXTERNAL_LIB_INST_DIR}/boost/")
    set(ENV{BOOST_ROOT} ${Boost_INSTALL_DIR})

    message(STATUS "Building boost with toolset: ${BOOST_TOOLSET}")

    set(Boost_CONFIGURE_COMMAND bash ${CMAKE_CURRENT_LIST_DIR}/cmake/boost/boost_configure.sh ${Boost_INSTALL_DIR} ${COMPILER_TOOLSET} ${Boost_LIBRARY_DIRS})

    if(BUILD_SHARED_LIBS)
        set(Boost_BUILD_COMMAND ./b2 -j ${EXTERNAL_BUILD_N_PROCS} dll-path=${Boost_LIBRARY_DIRS} linkflags=-Wl,-rpath=${Boost_LIBRARY_DIRS},--enable-new-dtags link=shared runtime-link=shared cxxflags=${NO_WARN_CMAKE_CXX_FLAGS} toolset=${BOOST_TOOLSET})
        set(Boost_INSTALL_COMMAND ./b2 -j ${EXTERNAL_BUILD_N_PROCS} dll-path=${Boost_LIBRARY_DIRS} linkflags=-Wl,-rpath=${Boost_LIBRARY_DIRS},--enable-new-dtags link=shared runtime-link=shared cxxflags=${NO_WARN_CMAKE_CXX_FLAGS} toolset=${BOOST_TOOLSET} install)
    else()
        set(Boost_BUILD_COMMAND ./b2 -j ${EXTERNAL_BUILD_N_PROCS} link=static runtime-link=static cxxflags=${NO_WARN_CMAKE_CXX_FLAGS} toolset=${BOOST_TOOLSET})
        set(Boost_INSTALL_COMMAND ./b2 -j ${EXTERNAL_BUILD_N_PROCS} link=static runtime-link=static cxxflags=${NO_WARN_CMAKE_CXX_FLAGS} toolset=${BOOST_TOOLSET} install)
    endif()
    ExternalProject_Add( external_boost
        PREFIX external/boost
        URL ${Boost_URL}
        URL_HASH SHA256=${Boost_SHA256}
        BUILD_IN_SOURCE 1
        CONFIGURE_COMMAND ${Boost_CONFIGURE_COMMAND}
        BUILD_COMMAND ${Boost_BUILD_COMMAND}
        INSTALL_COMMAND ${Boost_INSTALL_COMMAND}
        INSTALL_DIR ${Boost_INSTALL_DIR}
    )

    if(BUILD_SHARED_LIBS)
        set( Boost_LIBRARY_SUFFIX .so )
    else()
        set( Boost_LIBRARY_SUFFIX .a )
    endif()
    set( Boost_LIBRARY_PREFIX lib )

    set(Boost_LIBRARIES ${Boost_LIBRARY_DIRS}/${Boost_LIBRARY_PREFIX}boost_mpi${Boost_LIBRARY_SUFFIX})
    list(APPEND Boost_LIBRARIES ${Boost_LIBRARY_DIRS}/${Boost_LIBRARY_PREFIX}boost_serialization${Boost_LIBRARY_SUFFIX})
    list(APPEND Boost_LIBRARIES ${Boost_LIBRARY_DIRS}/${Boost_LIBRARY_PREFIX}boost_system${Boost_LIBRARY_SUFFIX})
    list(APPEND Boost_LIBRARIES ${Boost_LIBRARY_DIRS}/${Boost_LIBRARY_PREFIX}boost_filesystem${Boost_LIBRARY_SUFFIX})

    add_dependencies(libsisso external_boost)
    if(BUILD_TESTS)
        target_include_directories(sisso_test SYSTEM PRIVATE ${Boost_INCLUDE_DIRS})
    endif()
endif()

list(APPEND CMAKE_INSTALL_RPATH ${Boost_LIBRARY_DIRS})
target_include_directories(libsisso SYSTEM PUBLIC ${Boost_INCLUDE_DIRS})
target_link_libraries(libsisso PUBLIC -Wl,--rpath=${Boost_LIB_DIR} ${Boost_LIBRARIES})

# Check for NLopt
if(BUILD_PARAMS)
    if(${THIRD_PARTY_USE_NO_SYS_PATH})
        find_package(NLopt NO_SYSTEM_ENVIRONMENT_PATH)
    else(${THIRD_PARTY_USE_NO_SYS_PATH})
        find_package(NLopt)
    endif()

    if(NOT DEFINED NLOPT_LIBRARIES)
        message(STATUS "Building the NLopt library.")
        set(NLOPT_BUILD_DIR "${CMAKE_CURRENT_BINARY_DIR}/external/nlopt/build/")
        set(NLOPT_INSTALL_DIR "${CMAKE_CURRENT_BINARY_DIR}/external/nlopt/bin/")
        set(NLOPT_INCLUDE_DIRS "${NLOPT_INSTALL_DIR}/include/")
        set(NLOPT_LIBRARY_DIRS "${EXTERNAL_LIB_INST_DIR}/nlopt/")
        set(NLOPT_CMAKE_ARGS "-DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER};-DCMAKE_C_COMPILER=${CMAKE_C_COMPILER};-DINSTALL_LIB_DIR=${NLOPT_LIBRARY_DIRS};-DCMAKE_INSTALL_PREFIX=${NLOPT_INSTALL_DIR};-DBUILD_SHARED_LIBS=${BUILD_SHARED_LIBS};-DCMAKE_CXX_FLAGS=${NO_WARN_CMAKE_CXX_FLAGS}")
        ExternalProject_Add(
            external_nlopt
            PREFIX "external/nlopt"
            GIT_REPOSITORY "https://github.com/stevengj/nlopt.git"
            GIT_TAG "v2.6.2"
            CMAKE_ARGS "${NLOPT_CMAKE_ARGS}"
            BINARY_DIR "${NLOPT_BUILD_DIR}"
            INSTALL_DIR "${NLOPT_INSTALL_DIR}"
         )
        target_include_directories(libsisso SYSTEM PUBLIC ${NLOPT_INCLUDE_DIRS})
        if(BUILD_SHARED_LIBS)
            set(NLOPT_LIBRARIES "${NLOPT_LIBRARY_DIRS}/libnlopt.so")
        else()
            set(NLOPT_LIBRARIES "${NLOPT_LIBRARY_DIRS}/libnlopt.a")
        endif()

        add_dependencies(libsisso external_nlopt)
    endif()
    list(APPEND CMAKE_INSTALL_RPATH ${NLOPT_LIBRARY_DIRS})
    target_link_libraries(libsisso PUBLIC ${NLOPT_LIBRARIES})
endif()

# Check for CoinUtils and Coin-Clp
if(${THIRD_PARTY_USE_NO_SYS_PATH})
    find_package(CoinUtils NO_SYSTEM_ENVIRONMENT_PATH)
    find_package(clp NO_SYSTEM_ENVIRONMENT_PATH)
else(${THIRD_PARTY_USE_NO_SYS_PATH})
    find_package(CoinUtils)
    find_package(clp)
endif()

# Setup the LAPACK libraries and CXX compiler for use in building either Coin-OR code
if(NOT clp_FOUND OR NOT CoinUtils_FOUND)
    get_filename_component(COIN_CXX ${CMAKE_CXX_COMPILER} NAME)
    set(COIN_LAPACK_LIBS "-L${LAPACK_DIR}")

    foreach(LAPACK_LIB_FILE IN LISTS LAPACK_LIBRARIES)
        get_filename_component(LAPACK_LIB ${LAPACK_LIB_FILE} NAME_WE)
        string(REPLACE "lib" "-l" LAPACK_LIB ${LAPACK_LIB})
        set(COIN_LAPACK_LIBS "${COIN_LAPACK_LIBS} ${LAPACK_LIB}")
    endforeach()
    set(COIN_LAPACK_LIBS "${COIN_LAPACK_LIBS}")
endif()

# Build CoinUtils
if(NOT CoinUtils_FOUND)
    set(COIN_UTILS_BUILD_DIR "${CMAKE_CURRENT_BINARY_DIR}/external/CoinUtils/build/")
    set(COIN_UTILS_INSTALL_DIR "${CMAKE_CURRENT_BINARY_DIR}/external/CoinUtils/bin/")
    set(COIN_UTILS_INCLUDE_DIRS "${COIN_UTILS_INSTALL_DIR}/include/coin")
    set(COIN_UTILS_LIBRARY_DIRS "${EXTERNAL_LIB_INST_DIR}/coin-or/")
    if(BUILD_SHARED_LIBS)
        set(COIN_UTILS_CONFIGURE_COMMAND bash ${CMAKE_CURRENT_LIST_DIR}/cmake/CoinUtils/coin_utils_configure.sh ${COIN_UTILS_INSTALL_DIR} ${COIN_LAPACK_LIBS} ${COIN_CXX} ${COIN_UTILS_LIBRARY_DIRS} "true")
        set(COIN_UTILS_MAKE_INSTALL_COMMAND bash ${CMAKE_CURRENT_LIST_DIR}/cmake/CoinUtils/coin_utils_make_install.sh ${COIN_UTILS_LIBRARY_DIRS}/libCoinUtils.so ${COIN_UTILS_INCLUDE_DIRS})
    else()
        set(COIN_UTILS_CONFIGURE_COMMAND bash ${CMAKE_CURRENT_LIST_DIR}/cmake/CoinUtils/coin_utils_configure.sh ${COIN_UTILS_INSTALL_DIR} ${COIN_LAPACK_LIBS} ${COIN_CXX} ${COIN_UTILS_LIBRARY_DIRS} "false")
        set(COIN_UTILS_MAKE_INSTALL_COMMAND bash ${CMAKE_CURRENT_LIST_DIR}/cmake/CoinUtils/coin_utils_make_install.sh ${COIN_UTILS_LIBRARY_DIRS}/libCoinUtils.a ${COIN_UTILS_INCLUDE_DIRS})
    endif()

    ExternalProject_Add(
        external_CoinUtils
        PREFIX "external/CoinUtils"
        GIT_REPOSITORY "https://github.com/coin-or/CoinUtils.git"
        GIT_TAG "releases/2.11.4"
        CONFIGURE_COMMAND "${COIN_UTILS_CONFIGURE_COMMAND}"
        BUILD_COMMAND make -j ${EXTERNAL_BUILD_N_PROCS}
        INSTALL_COMMAND "${COIN_UTILS_MAKE_INSTALL_COMMAND}"
        BINARY_DIR "${COIN_UTILS_BUILD_DIR}"
        INSTALL_DIR "${COIN_UTILS_INSTALL_DIR}"
     )

    add_dependencies(libsisso external_CoinUtils)
    if(BUILD_SHARED_LIBS)
        set(COIN_UTILS_LIBRARIES "${COIN_UTILS_LIBRARY_DIRS}/libCoinUtils.so")
    else()
        set(COIN_UTILS_LIBRARIES "${COIN_UTILS_LIBRARY_DIRS}/libCoinUtils.a")
    endif()
endif()
target_include_directories(libsisso SYSTEM PUBLIC ${COIN_UTILS_INCLUDE_DIRS})
list(APPEND CMAKE_INSTALL_RPATH ${COIN_UTILS_LIBRARY_DIRS})
target_link_libraries(libsisso PUBLIC ${COIN_UTILS_LIBRARIES})

# Build Coin-Clp
if(NOT clp_FOUND)
    set(COIN_CLP_BUILD_DIR "${CMAKE_CURRENT_BINARY_DIR}/external/coin-Clp/build/")
    set(COIN_CLP_INSTALL_DIR "${CMAKE_CURRENT_BINARY_DIR}/external/coin-Clp/bin/")
    set(COIN_CLP_INCLUDE_DIRS "${COIN_CLP_INSTALL_DIR}/include/")
    set(COIN_CLP_LIBRARY_DIRS "${EXTERNAL_LIB_INST_DIR}/coin-or/")

    if(BUILD_SHARED_LIBS)
        set(COIN_CLP_CONFIGURE_COMMAND bash ${CMAKE_CURRENT_LIST_DIR}/cmake/coin-Clp/clp_configure.sh ${COIN_CLP_INSTALL_DIR} ${COIN_LAPACK_LIBS} ${COIN_CXX} ${COIN_CLP_LIBRARY_DIRS}  "${COIN_UTILS_LIBRARY_DIRS}/libCoinUtils.so" ${COIN_UTILS_INCLUDE_DIRS} "true")
        set(COIN_CLP_MAKE_INSTALL_COMMAND bash ${CMAKE_CURRENT_LIST_DIR}/cmake/coin-Clp/clp_make_install.sh ${COIN_CLP_LIBRARY_DIRS}/libClp.so ${COIN_CLP_INCLUDE_DIRS})
    else()
        set(COIN_CLP_CONFIGURE_COMMAND bash ${CMAKE_CURRENT_LIST_DIR}/cmake/coin-Clp/clp_configure.sh ${COIN_CLP_INSTALL_DIR} ${COIN_LAPACK_LIBS} ${COIN_CXX} ${COIN_CLP_LIBRARY_DIRS}  "${COIN_UTILS_LIBRARY_DIRS}/libCoinUtils.la" ${COIN_UTILS_INCLUDE_DIRS} "false")
        set(COIN_CLP_MAKE_INSTALL_COMMAND bash ${CMAKE_CURRENT_LIST_DIR}/cmake/coin-Clp/clp_make_install.sh ${COIN_CLP_LIBRARY_DIRS}/libClp.a ${COIN_CLP_INCLUDE_DIRS})
    endif()

    ExternalProject_Add(
        external_Clp
        PREFIX "external/coin-Clp"
        GIT_REPOSITORY "https://github.com/coin-or/Clp.git"
        GIT_TAG "releases/1.17.6"
        CONFIGURE_COMMAND "${COIN_CLP_CONFIGURE_COMMAND}"
        BUILD_COMMAND make -j ${EXTERNAL_BUILD_N_PROCS}
        INSTALL_COMMAND ${COIN_CLP_MAKE_INSTALL_COMMAND}
        BINARY_DIR "${COIN_CLP_BUILD_DIR}"
        INSTALL_DIR "${COIN_CLP_INSTALL_DIR}"
    )
    if(NOT CoinUtils_FOUND)
        ExternalProject_Add_StepDependencies(external_Clp build external_CoinUtils)
    endif()

    if(BUILD_SHARED_LIBS)
        set(COIN_CLP_LIBRARIES "${COIN_CLP_LIBRARY_DIRS}/libClp.so")
    else()
        set(COIN_CLP_LIBRARIES "${COIN_CLP_LIBRARY_DIRS}/libClp.a")
    endif()

    add_dependencies(libsisso external_Clp)
endif()

target_include_directories(libsisso SYSTEM PUBLIC ${COIN_CLP_INCLUDE_DIRS})
list(APPEND CMAKE_INSTALL_RPATH ${COIN_CLP_LIBRARY_DIRS})
target_link_libraries(libsisso PUBLIC ${COIN_CLP_LIBRARIES})

# Check for google test libraries
if(BUILD_TESTS)
    if(${THIRD_PARTY_USE_NO_SYS_PATH})
        find_package(GTest NO_SYSTEM_ENVIRONMENT_PATH)
    else(${THIRD_PARTY_USE_NO_SYS_PATH})
        find_package(GTest)
    endif()

    if(NOT GTest_FOUND)
        message(STATUS "Building google test")
        set(GTEST_BUILD_DIR "${CMAKE_CURRENT_BINARY_DIR}/external/gtest/build/")
        set(GTEST_INSTALL_DIR "${CMAKE_CURRENT_BINARY_DIR}/external/gtest/bin/")
        set(GTEST_INCLUDE_DIRS "${GTEST_INSTALL_DIR}/include")
        set(GTEST_LIBRARY_DIRS "${EXTERNAL_LIB_INST_DIR}/gtest/")
        set(GTEST_CMAKE_ARGS "-DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER};-DCMAKE_C_COMPILER=${CMAKE_C_COMPILER};-DCMAKE_INSTALL_PREFIX=${GTEST_INSTALL_DIR};-DCMAKE_INSTALL_LIBDIR=${GTEST_LIBRARY_DIRS};-DBUILD_SHARED_LIBS=${BUILD_SHARED_LIBS};-DCMAKE_CXX_FLAGS=${NO_WARN_CMAKE_CXX_FLAGS}")

        ExternalProject_Add(
            external_gtest
            PREFIX "external/gtest"
            GIT_REPOSITORY "https://github.com/google/googletest.git"
            GIT_TAG "release-1.11.0"
            CMAKE_ARGS "${GTEST_CMAKE_ARGS}"
            BINARY_DIR "${GTEST_BUILD_DIR}"
            INSTALL_DIR "${GTEST_INSTALL_DIR}"
        )
        if(BUILD_SHARED_LIBS)
            set(GTEST_BOTH_LIBRARIES "${GTEST_LIBRARY_DIRS}/libgtest.so;${GTEST_LIBRARY_DIRS}/libgtest_main.so;${GTEST_LIBRARY_DIRS}/libgmock.so;${GTEST_LIBRARY_DIRS}/libgmock_main.so")
        else()
            set(GTEST_BOTH_LIBRARIES "${GTEST_LIBRARY_DIRS}/libgtest.a;${GTEST_LIBRARY_DIRS}/libgtest_main.a;${GTEST_LIBRARY_DIRS}/libgmock.a;${GTEST_LIBRARY_DIRS}/libgmock_main.a")
        endif()
        target_link_libraries(sisso_test libsisso -Wl,--rpath=${GTEST_LIBRARY_DIRS} ${GTEST_BOTH_LIBRARIES})
        target_include_directories(sisso_test SYSTEM PUBLIC ${GTEST_INCLUDE_DIRS})

        add_dependencies(sisso_test external_gtest)
    else()
        target_link_libraries(sisso_test libsisso -Wl,--rpath=${GTEST_LIBRARY_DIRS} "GTest::GTest")
    endif()

    list(APPEND CMAKE_INSTALL_RPATH ${GTEST_LIBRARY_DIRS})
endif()

# Check for libfmt
if(${THIRD_PARTY_USE_NO_SYS_PATH})
    find_package(fmt NO_SYSTEM_ENVIRONMENT_PATH)
else(${THIRD_PARTY_USE_NO_SYS_PATH})
    find_package(fmt)
endif()

if(NOT fmt_FOUND)
    message(STATUS "lib fmt")

    set(FMT_BUILD_DIR "${CMAKE_CURRENT_BINARY_DIR}/external/fmt/build/")
    set(FMT_INSTALL_DIR "${CMAKE_CURRENT_BINARY_DIR}/external/fmt/bin/")
    set(FMT_INCLUDE_DIRS "${FMT_INSTALL_DIR}/include/")
    set(FMT_LIBRARY_DIRS "${EXTERNAL_LIB_INST_DIR}/fmt/")
    set(FMT_CMAKE_ARGS "-DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER};-DCMAKE_C_COMPILER=${CMAKE_C_COMPILER};-DCMAKE_INSTALL_LIBDIR=${FMT_LIBRARY_DIRS};-DCMAKE_INSTALL_PREFIX=${FMT_INSTALL_DIR};-DFMT_DOC=OFF;-DFMT_TEST=OFF;-DFMT_OS=OFF;-DBUILD_SHARED_LIBS=${BUILD_SHARED_LIBS}")
    if(NOT BUILD_SHARED_LIBS)
        list(APPEND FMT_CMAKE_ARGS "-DCMAKE_CXX_FLAGS=${NO_WARN_CMAKE_CXX_FLAGS} -fPIC")
    else(NOT BUILD_SHARED_LIBS)
        list(APPEND FMT_CMAKE_ARGS "-DCMAKE_CXX_FLAGS=${NO_WARN_CMAKE_CXX_FLAGS}")
    endif()
    ExternalProject_Add(
        external_fmt
        PREFIX "external/fmt"
        GIT_REPOSITORY "https://github.com/fmtlib/fmt.git"
        GIT_TAG "7.1.2"
        CMAKE_ARGS "${FMT_CMAKE_ARGS}"
        BINARY_DIR "${FMT_BUILD_DIR}"
        INSTALL_DIR "${FMT_INSTALL_DIR}"
    )
    include_directories(SYSTEM ${FMT_INCLUDE_DIRS})
    target_include_directories(libsisso SYSTEM PUBLIC ${FMT_INCLUDE_DIRS})
    if(BUILD_SHARED_LIBS)
        set(FMT_LIBRARIES "${FMT_LIBRARY_DIRS}/libfmt.so")
    else()
        set(FMT_LIBRARIES "${FMT_LIBRARY_DIRS}/libfmt.a")
    endif()

    add_dependencies(libsisso external_fmt)
    list(APPEND CMAKE_INSTALL_RPATH ${FMT_LIBRARY_DIRS})
    target_link_libraries(libsisso PUBLIC ${FMT_LIBRARIES})
else()
    get_target_property(fmt_LOCATION fmt::fmt LOCATION)
    get_filename_component(fmt_LIBRARY_DIRS ${fmt_LOCATION} DIRECTORY)
    list(APPEND CMAKE_INSTALL_RPATH ${fmt_LIBRARY_DIRS})
    target_link_libraries(libsisso PUBLIC fmt::fmt)
endif()

if(BUILD_CALIPER)
    find_package(caliper)
    list(APPEND CMAKE_INSTALL_RPATH ${caliper_LIB_DIR})
    target_link_libraries(libsisso PUBLIC caliper)
endif()

list(APPEND CMAKE_INSTALL_RPATH ${CMAKE_INSTALL_PREFIX}/lib64/)

# Set the install RPATH for all targets
set_target_properties(libsisso PROPERTIES INSTALL_RPATH "${CMAKE_INSTALL_RPATH}")
if(BUILD_EXE)
    set_target_properties(sisso++ PROPERTIES INSTALL_RPATH "${CMAKE_INSTALL_RPATH}")
endif()
if(BUILD_PYTHON)
    set_target_properties(_sissopp PROPERTIES INSTALL_RPATH "${CMAKE_INSTALL_RPATH}")
endif()

if(BUILD_PYTHON AND (NOT BUILD_SHARED_LIBS))
    find_package(BZip2)
    target_link_libraries(libsisso PUBLIC ${BZIP2_LIBRARIES})
endif()
# Set up the necessary flags if one wants to test the coverage
if(CMAKE_BUILD_TYPE STREQUAL "Coverage")
    # Setup which targets will be used for the coverage tests
    SET(COV_DEPS "sisso++")

    if(BUILD_PYTHON)
        LIST(APPEND COV_DEPS "_sissopp")
    endif()

    if(BUILD_TESTS)
        LIST(APPEND COV_DEPS "sisso_test")
    endif()

    # Which coverage tester are we using?
    string(COMPARE EQUAL ${CMAKE_CXX_COMPILER_ID} "GNU" GNU_COMP)
    string(COMPARE EQUAL ${CMAKE_CXX_COMPILER_ID} "Intel" INTEL_COMP)

    if(GNU_COMP)
        SET(CMAKE_CXX_FLAGS "-g -O0 -fprofile-arcs -ftest-coverage")
        SET(CMAKE_C_FLAGS "-g -O0 -fprofile-arcs -ftest-coverage")

        set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)
        include(CodeCoverage)

        setup_target_for_coverage_lcov(
            NAME "coverage_html"
            EXECUTABLE ctest --output-on-failure
            DEPENDENCIES ${COV_DEPS}
            BASE_DIRECTORY "${CMAKE_SOURCE_DIR}/"
            EXCLUDE "CMakeCXXCompilerId.cpp" "/usr/*" "/opt/*" "*/external/*" "third_party/*/${COMPILER_TOOLSET}/*" "${MPI_CXX_INCLUDE_DIRS}/*" "${Boost_INCLUDE_DIRS}/*" "${CMAKE_INSTALL_PREFIX}/include/kokkos/" "src/utils/mkl_*" "tests/*" "*/boost/*" "*/_deps/*"
        )
        SET(GCOVR_ADDITIONAL_ARGS --exclude-unreachable-branches --print-summary)
        setup_target_for_coverage_gcovr_xml(
            NAME "coverage"
            EXECUTABLE ctest --output-on-failure
            DEPENDENCIES ${COV_DEPS}
            BASE_DIRECTORY "${CMAKE_SOURCE_DIR}/"
            EXCLUDE "${CMAKE_BINARY_DIR}/" "src/external/*" "/usr/*" "/opt/*" "third_party/*/${COMPILER_TOOLSET}/*" "${MPI_CXX_INCLUDE_DIRS}/*" "${Boost_INCLUDE_DIRS}/*" "${CMAKE_INSTALL_PREFIX}/include/kokkos/" "src/utils/mkl_*" "tests/*" "_deps/*"
        )
        setup_target_for_coverage_gcovr_html(
            NAME "coverage-gcov-html"
            EXECUTABLE ctest --output-on-failure
            DEPENDENCIES ${COV_DEPS}
            BASE_DIRECTORY "${CMAKE_SOURCE_DIR}/"
            EXCLUDE "${CMAKE_BINARY_DIR}/" "src/external/*" "/usr/*" "/opt/*" "third_party/*/${COMPILER_TOOLSET}/*" "${MPI_CXX_INCLUDE_DIRS}/*" "${Boost_INCLUDE_DIRS}/*" "${CMAKE_INSTALL_PREFIX}/include/kokkos/" "src/utils/mkl_*" "tests/*" "_deps/*"
        )
    else()
        message(STATUS "Unable to build coverage target for the current compiler ${CMAKE_CXX_COMPILER_ID}")
    endif()
endif() #CMAKE_BUILD_TYPE STREQUAL "Coverage"

if(BUILD_TESTS)
    include(TestFunctions)
    add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/tests/integration/)
endif()
