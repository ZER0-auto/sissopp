###############
# Basic Flags #
###############
set(CMAKE_CXX_COMPILER icpc CACHE STRING "")
set(CMAKE_C_COMPILER icc CACHE STRING "")
set(CMAKE_CXX_FLAGS "-xHost -qopt-zmm-usage=high" CACHE STRING "")
set(CMAKE_BUILD_TYPE "Release" CACHE STRING "")

#################
# Feature Flags #
#################
set(BUILD_PARAMS OFF CACHE BOOL "")
set(BUILD_PYTHON ON CACHE BOOL "")
set(EXTERNAL_BOOST OFF CACHE BOOL "")
