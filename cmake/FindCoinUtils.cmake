find_path(COIN_UTILS_INCLUDE_DIRS
    NAMES "coin/CoinMpsIO.hpp"
)

if(${BUILD_SHARED_LIBS})
    set(COIN_UTILS_LIB_NAME "libCoinUtils.so")
else()
    set(COIN_UTILS_LIB_NAME "libCoinUtils.la")
endif()

find_library(COIN_UTILS_LIBRARY
    ${COIN_UTILS_LIB_NAME}
    HINTS "/usr/local/lib"
    HINTS "${COIN_UTILS_LIB_HINT}"
)

get_filename_component(COIN_UTILS_LIBRARY_DIRS ${COIN_UTILS_LIBRARY} DIRECTORY)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CoinUtils DEFAULT_MSG COIN_UTILS_LIBRARY COIN_UTILS_INCLUDE_DIRS)

if(CoinUtils_FOUND)
    message(STATUS "Found CoinUtils in ${COIN_UTILS_INCLUDE_DIRS}")
    set(COIN_UTILS_INCLUDE_DIRS "${COIN_UTILS_INCLUDE_DIRS};${COIN_UTILS_INCLUDE_DIRS}/coin/")
    set(COIN_UTILS_LIBRARIES ${COIN_UTILS_LIBRARY})
else (CoinUtils_FOUND)
    message(STATUS "Cannot find CoinUtils, will try building it from github.")
endif(CoinUtils_FOUND)

mark_as_advanced(COIN_UTILS_LIBRARIES COIN_UTILS_LIBRARY_DIRS COIN_UTILS_INCLUDE_DIRS)
