function(sisso_add_test)
    if (BUILD_TESTING AND SISSO_ENABLE_TESTING)
        set(options)
        set(oneValueArgs NAME)
        set(multiValueArgs FILES LABELS PROCESSES)
        cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

        if (NOT ARG_NAME)
            message(FATAL_ERROR "No name given for test!")
        endif ()

        if (NOT ARG_FILES)
            message(FATAL_ERROR "No files given for test ${ARG_NAME}!")
        endif ()

        if (NOT ARG_PROCESSES)
            set(ARG_PROCESSES 1)
        endif ()

        foreach (NUM_PROCESSES ${ARG_PROCESSES})
            set(TARGET_NAME ${ARG_NAME}_mpi_${NUM_PROCESSES})
            add_executable(${TARGET_NAME})
            target_sources(${TARGET_NAME} PRIVATE ${ARG_FILES})
            target_link_libraries(${TARGET_NAME}
                    PRIVATE libsisso -Wl,--rpath=${GTEST_LIBRARY_DIRS} ${GTEST_BOTH_LIBRARIES} -Wl,--rpath=${LAPACK_DIR} ${LAPACK_LIBRARIES} ${MPI_LIBRARIES} -Wl,--rpath=${Boost_LIB_DIR} ${Boost_LIBRARIES} ${OPENMP_LIBRARIES}
                    )
            target_include_directories(${TARGET_NAME} SYSTEM PUBLIC ${GTEST_INCLUDE_DIRS})

            add_test(NAME ${TARGET_NAME} COMMAND ${MPIEXEC_EXECUTABLE} ${MPIEXEC_NUMPROC_FLAG} ${NUM_PROCESSES} --map-by socket:OVERSUBSCRIBE ${MPIEXEC_PREFLAGS} ./${TARGET_NAME} ${MPIEXEC_POSTFLAGS})

            set_tests_properties(${TARGET_NAME} PROPERTIES ENVIRONMENT "OMP_PROC_BIND=spread;OMP_PLACES=threads")
            if (NOT ARG_LABELS)
                set_tests_properties(${TARGET_NAME} PROPERTIES LABELS "${ARG_LABELS}")
            endif ()
            set_tests_properties(${TARGET_NAME} PROPERTIES PROCESSORS ${NUM_PROCESSES})
            set_tests_properties(${TARGET_NAME} PROPERTIES PROCESSOR_AFFINITY ON)

            install(TARGETS ${TARGET_NAME} DESTINATION "${CMAKE_INSTALL_PREFIX}/tests/integration/")
        endforeach ()
    endif ()
endfunction()
