function(transfer_doc_string PYTHON_BINDING_FILE PYTHON_BINDING_FILE_OUT)
    file(READ ${PYTHON_BINDING_FILE} BINDINGS)
    string(REGEX REPLACE "\n" ";" BINDINGS "${BINDINGS}")
    string(REGEX MATCHALL "DocString_[A-Za-z0-9_]+" DOCSTRING_KEYS ${BINDINGS})

    file(GLOB_RECURSE HEADER_FILES *.hpp)

    foreach(HEADER_FILE ${HEADER_FILES})
        # To find the line with the flag
        file(READ ${HEADER_FILE} CONTENTS)
        if(CONTENTS STREQUAL "")
            message(STATUS "Header file ${HEADER_FILE} is empty.")
        else()

            string(REGEX REPLACE "[ \t]*// DocString: " "// DocString: "  CONTENTS ${CONTENTS})
            string(REGEX REPLACE "[ \t]*/[*][*]" "/**"  CONTENTS ${CONTENTS})
            string(REGEX REPLACE "[ \t]* [*]" " *"  CONTENTS ${CONTENTS})
            string(REGEX REPLACE "[ \t]* [*]/" " [*]/"  CONTENTS ${CONTENTS})

            string(REGEX REPLACE "\n" ";" CONTENTS "${CONTENTS}")

            foreach(KEY ${DOCSTRING_KEYS})
                # Get key to search for inside the source file
                string(REGEX REPLACE "DocString\\_" "// DocString: " HEADER_KEY ${KEY})

                # Remove tabs before DoxyGen Comments

                list(FIND CONTENTS "${HEADER_KEY}" INDEX)
                string(COMPARE NOTEQUAL ${INDEX} -1 CONDITION)

                if(CONDITION)
                    unset(DOCSTRING)
                    unset(DOXY_COMMENTS)
                    # To extract doxygen comments
                    math(EXPR INDEX "${INDEX}+1")
                    list(GET CONTENTS ${INDEX} LINE)
                    if(${LINE} MATCHES "/[*][*]")
                        math(EXPR INDEX "${INDEX}+1")
                        list(GET CONTENTS ${INDEX} LINE)
                    endif()

                    while(${LINE} MATCHES "(.*)")
                        string(REGEX MATCH "@([a-z]+) (.*)" LINE "${LINE}")
                        set(DOXY_COMMENTS ${DOXY_COMMENTS} ${LINE})
                        math(EXPR INDEX "${INDEX}+1")
                        list(GET CONTENTS ${INDEX} LINE)
                    endwhile()

                    set(IN_PARAMS OFF)
                    # To convert doxygen comments into docstrings
                    foreach(LINE ${DOXY_COMMENTS})
                        string(REGEX REPLACE "@brief " "" LINE "${LINE}")
                        string(REGEX REPLACE "@details " "" LINE "${LINE}")

                        if("${LINE}" MATCHES "@param ([a-zA-Z0-9_]+)(\\([a-zA-Z0-9_.]+\\)) (.*)")
                            if(IN_PARAMS)
                                set(LINE "    ${CMAKE_MATCH_1} ${CMAKE_MATCH_2}: ${CMAKE_MATCH_3}")
                            else(NOT IN_PARAMS)
                                set(LINE "\\nArgs:\\n    ${CMAKE_MATCH_1} ${CMAKE_MATCH_2}: ${CMAKE_MATCH_3}")
                                set(IN_PARAMS ON)
                            endif()
                        endif()
                        if("${LINE}" MATCHES "@param ([a-zA-Z0-9_]+) (\\([a-zA-Z0-9_.]+\\)) (.*)")
                            if(IN_PARAMS)
                                set(LINE "    ${CMAKE_MATCH_1} ${CMAKE_MATCH_2}: ${CMAKE_MATCH_3}")
                            else(NOT IN_PARAMS)
                                set(LINE "\\nArgs:\\n    ${CMAKE_MATCH_1} ${CMAKE_MATCH_2}: ${CMAKE_MATCH_3}")
                                set(IN_PARAMS ON)
                            endif()
                        endif()
                        if("${LINE}" MATCHES "@param ([a-zA-Z0-9_]+) (.*)")
                            if(IN_PARAMS)
                                set(LINE "    ${CMAKE_MATCH_1}: ${CMAKE_MATCH_2}")
                            else(NOT IN_PARAMS)
                                set(LINE "\\nArgs:\\n    ${CMAKE_MATCH_1}: ${CMAKE_MATCH_2}")
                                set(IN_PARAMS ON)
                            endif()
                        endif()
                        if ("${LINE}" MATCHES "@return (.+)")
                            set(LINE "\\nReturns:\\n     ${CMAKE_MATCH_1}")
                        endif()
                        set(DOCSTRING ${DOCSTRING} ${LINE})
                    endforeach()
                    string(REPLACE ";" "\\n" DOCSTRING "${DOCSTRING}")
                    set(${KEY} ${DOCSTRING})
                endif()
            endforeach()
        endif()
    endforeach()
    # # To insert docstrings in cpp file
    # set(Docstring_${FUNCTION} ${DOCSTRING})
    configure_file(${PYTHON_BINDING_FILE} ${PYTHON_BINDING_FILE_OUT} @ONLY)
endfunction()
