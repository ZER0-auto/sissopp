#! /usr/bin/bash

export uname_out="$(uname -s)"
case "${uname_out}" in
    Linux*)     export machine=linux;;
    Darwin*)    export machine=darwin;;
    *)          export machine="UNKNOWN:${unameOut}";;
esac

if [[ $2 == "intel-llvm-${machine}" ]]; then
    export boost_toolset="intel-${machine}"
    export mpi_compiler="mpiicpc"
    sed -i 's/rm -f "$(<)" && LD_LIBRARY_PATH="$(RUN_PATH)" "$(CONFIG_COMMAND)" -x c++-header $(OPTIONS) $(USER_OPTIONS) -D$(DEFINES) -I"$(INCLUDES)" -c -pch-create "$(<)" "$(>)"/rm -f "$(<)" && LD_LIBRARY_PATH="$(RUN_PATH)" "$(CONFIG_COMMAND)" -x c++-header $(OPTIONS) $(USER_OPTIONS) -D$(DEFINES) -I"$(INCLUDES)" -c -Xclang -emit-pch -o "$(<)" "$(>)"/g' tools/build/src/tools/intel-linux.jam
elif [[ $2 == "intel-${machine}" ]]; then
    export boost_toolset="intel-${machine}"
    export mpi_compiler="mpiicpc"
else
    export boost_toolset=$2
    export mpi_compiler="mpicxx"
fi

if [ "$#" -eq 2 ]; then
    ./bootstrap.sh --with-toolset=${boost_toolset} --with-libraries=mpi,serialization,system,filesystem --prefix=$1
    echo "using mpi : ${mpi_compiler} ;" >> project-config.jam
elif [ "$#" -eq 3 ]; then
    ./bootstrap.sh --with-toolset=${boost_toolset} --with-libraries=mpi,serialization,system,filesystem --prefix=$1 --libdir=$3
    echo "using mpi : ${mpi_compiler} ;" >> project-config.jam
elif [ "$#" -eq 7 ]; then
    ./bootstrap.sh --with-toolset=${boost_toolset} --with-libraries=mpi,serialization,system,filesystem --prefix=$1 --libdir=$3
    echo "using mpi : ${mpi_compiler} ;" >> project-config.jam
    sed -i "s/--with-mpi --with-serialization --with-system --with-filesystem/--with-mpi --with-serialization --with-system --with-filesystem --with-python/g" project-config.jam
    echo -e "import python ;\nif ! [ python.configured ]\n{\n    using python : ${4} : ${5} : ${6} : ${7} ;\n}" >> project-config.jam
fi

if [[ $2 == "intel-${machine}" ]]; then
    export version=`icpc -dumpversion`
    sed -i "s/using intel-linux ;/using intel-linux : $version : icpc ;/g" project-config.jam
fi
