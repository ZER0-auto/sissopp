.. SISSO++ documentation master file, created by
   sphinx-quickstart on Fri Aug  6 17:42:26 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SISSO++
=======
.. toctree::
   :maxdepth: 2
   :caption: Contents:

This package provides a C++ implementation of SISSO with built in Python bindings for an efficient python interface.
Future work will expand the python interface to include more postporcessing analysis tools.

Table of Contents
^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2

   quick_start/QuickStart
   tutorial/tutorial
   cpp_api/cpp_api
   python_api/python_api
   development/develop
