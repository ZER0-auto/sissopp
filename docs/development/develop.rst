.. _devel:

Development
===========
.. toctree::
    :maxdepth: 2

    CONTRIBUTING
    license
    Credits
    References
