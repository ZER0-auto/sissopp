# Acknowledgements

`SISSO++` would not be possible without the following packages:

- [boost](https://www.boost.org/)
- [Coin-Clp](https://github.com/coin-or/Clp)
- [LIBSVM](https://www.csie.ntu.edu.tw/~cjlin/libsvm/)
- [googletest](https://github.com/google/googletest)
- [NLopt](http://github.com/stevengj/nlopt)

## How to cite these packages:

Please make sure to give credit to the right people when using `SISSO++`:
For classification problems cite:
- [How to cite Coin-Clp](https://zenodo.org/record/3748677#.YBuxDVmYVhE)
- [How to cite LIBSVM](https://www.csie.ntu.edu.tw/~cjlin/libsvm/faq.html#f203)

For use of the parameterized nodes please cite:
- [How to cite NLopt](https://nlopt.readthedocs.io/en/latest/Citing_NLopt/)
