Installation
---
The package uses a CMake build system, and compatible all versions of the C++ standard library after C++ 14.
You can access the code [here](http://gitlab.com/sissopp_developers/sissopp).
To download the code including the `pybind11` submodule (necessary for the python bindings) then you need to run:
```bash
git clone --recursive http://gitlab.com/sissopp_developers/sissopp
```
This will clone both the source code and `pybind11` dependency to your machine.

### Prerequisites
To install `SISSO++` the following packages are needed:

- CMake version 3.10 and up
- A C++ compiler (compatible with C++ 14 and later, e.g. gcc 5.0+ or icpc 17.0+)
- BLAS/LAPACK
- MPI

Additionally the following packages needed by SISSO++ will be installed (if they are not installed already/if they cannot be found in $PATH)

  - [Boost](https://www.boost.org) (mpi, serialization, system, and filesystem libraries)
  - [pybind11](https://pybind11.readthedocs.io/en/stable/index.html) (Provided as a git submodule)
  - [GTest](https://github.com/google/googletest)
  - [Coin-Clp](https://github.com/coin-or/Clp)
  - [NLopt](https://github.com/stevengj/nlopt)
  - [{fmt}](https://fmt.dev/latest/index.html) (Used for the C++ 20 [std::format](https://en.cppreference.com/w/cpp/utility/format/format) library)

pybind11 can be accessed via a git submodule, which is automatically cloned into a subdirectory if you used `git clone --recursive`. If you did not you can clone it via
```bash
git submodule init
git submodule update
```
The remaining packages can also be installed separately outside of the `SISSO++` build process by using the `build_third_party.bash` script. To use this run
```bash
./build_third_party.bash CXX=$CXX_COMPILER CC=$C_COMPILER CXXFLAGS=$CXX_FLAGS -j ${N_PROCS}
```
where CXX_COMPILER is the C++ compiler, C_COMPILER is the C compiler, CXX_FLAGS are the C++ compiler arguments and N_PROCS is the number of processes to build the libraries on.
The script will install the libraries in `third_party/LIB_NAME/$TOOLSET_NAME/$VERSION/`, where LIB_NAME is the name of the library, TOOLSET_NAME is the compiler identifier and VERSION is the version of the compiler. For the Boost libraries the LIB_NAME=`boost/1.68.0/`.

To build and use the optional python bindings the following are also needed:

- [Python 3.6 or greater](https://www.python.org/)
- [numpy](https://numpy.org/)
- [pandas](https://pandas.pydata.org/)
- [scipy](https://www.scipy.org/)
- [seaborn](https://seaborn.pydata.org/)
- [sklearn](https://scikit-learn.org/stable/index.html)
- [toml](https://pypi.org/project/toml/)

The setup of the python environment can be done using anaconda with

```bash
conda create -n sissopp_env python=3.9 numpy pandas scipy seaborn scikit-learn toml
```

### Installing `SISSO++`

`SISSO++` is installed using a cmake build system, with sample configuration files located in `cmake/toolchains/`
For example, here is `initial_config.cmake` file used to construct `SISSO++` and the python bindings using the gnu compiler.

```
###############
# Basic Flags #
###############
set(CMAKE_CXX_COMPILER g++ CACHE STRING "")
set(CMAKE_C_COMPILER gcc CACHE STRING "")
set(CMAKE_CXX_FLAGS "-O3 -march=native" CACHE STRING "")
set(CMAKE_C_FLAGS "-O3 -march=native" CACHE STRING "")

#################
# Feature Flags #
#################
set(BUILD_PYTHON ON CACHE BOOL "")
set(BUILD_PARAMS ON CACHE BOOL "")
```

Because we want to build with the python bindings in this example and assuming there is no preexisting python environment, we need to first create/activate it.
For this example we will use `conda`, but standard python installations or virtual environments are also possible.

```bash
conda create -n sissopp_env python=3.9 numpy pandas scipy seaborn scikit-learn toml
conda activate sissopp_env
```

Note if you are using a python environment with a local MKL installation then make sure the versions of all accessible MKL libraries are the same.

Now we can install `SISSO++` using `initial_config.cmake` and the following commands (this assumes gnu compiler and MKL are used, if you are using a different compiler/BLAS library change the flags to the relevant directories)

```bash
export MKLROOT=/path/to/mkl/
export BOOST_ROOT=/path/to/boost

cd ~/sissopp/
mkdir build/;
cd build/;

cmake -C initial_config.cmake ../
make
make install
```

Once all the commands are run `SISSO++` should be in the `~/SISSO++/main directory/bin/` directory.

#### Installing the Python Bindings Without Administrative Privileges

To install the python bindings on a machine where you do not have write privilege to the default python install directory (typical on most HPC systems), you must set the `PYTHON_INSTDIR` to a directory where you do have write access.
This can be done by modifying the `camke` command to:

```bash
cmake -C initial_config.cmake -DPYTHON_INSTDIR=/path/to/python/install/directory/ ../
```

A standard local python installation directory for pip and conda is `$HOME/.local/lib/python3.X/site-packages/` where X is the minor version of python.
It is important that if you do set this variable then that directory is also inside your `PYTHONPATH` envrionment variable. This can be updated with

```bash
export PYTHONPATH=$PYTHONPATH:/path/to/python/install/directory/
```

If you are using anaconda, then this can be avoided by creating a new conda environment as detailed above.

You will need to set this variable and recompile the code (remove all build files first) if you see this error

```bash

CMake Error at src/cmake_install.cmake:114 (file):
  file cannot create directory:
  ${PTYHON_BASE_DIR}/lib/python3.X/site-packages/sissopp.
  Maybe need administrative privileges.
Call Stack (most recent call first):
  cmake_install.cmake:42 (include)" 
```

#### Install the Binary Without the Python Bindings

To install only the `SISSO++` executable repeat the same commands as above but set `USE_PYTHON` in `initial_config.cmake` to `OFF`.

#### Testing the Compilation

To test the compilation of `SISSO++` run `make test` and you should get the following output or a subset of it depending on which parameters you are using
```
Test project /home/purcell/git/cpp_sisso/build
      Start  1: Classification
 1/17 Test  #1: Classification ...........................................   Passed    1.74 sec
      Start  2: Classification_Max_Correlation_NE_One
 2/17 Test  #2: Classification_Max_Correlation_NE_One ....................   Passed    0.55 sec
      Start  3: Classification_Generate_Project
 3/17 Test  #3: Classification_Generate_Project ..........................   Passed    0.55 sec
      Start  4: Classification_Max_Correlation_NE_One_Generate_Project
 4/17 Test  #4: Classification_Max_Correlation_NE_One_Generate_Project ...   Passed    0.54 sec
      Start  5: Regression
 5/17 Test  #5: Regression ...............................................   Passed    0.38 sec
      Start  6: Generate_Project
 6/17 Test  #6: Generate_Project .........................................   Passed    0.38 sec
      Start  7: Maximum_Correlation_NE_One
 7/17 Test  #7: Maximum_Correlation_NE_One ...............................   Passed    0.43 sec
      Start  8: Maximum_Correlation_NE_One_Generate_Project
 8/17 Test  #8: Maximum_Correlation_NE_One_Generate_Project ..............   Passed    0.39 sec
      Start  9: Log_Regression
 9/17 Test  #9: Log_Regression ...........................................   Passed    0.37 sec
      Start 10: Log_Regression_Max_Correlation_NE_One
10/17 Test #10: Log_Regression_Max_Correlation_NE_One ....................   Passed    0.37 sec
      Start 11: Log_Regression_Generate_Project
11/17 Test #11: Log_Regression_Generate_Project ..........................   Passed    0.39 sec
      Start 12: Log_Regression_Max_Correlation_NE_One_Generate_Project
12/17 Test #12: Log_Regression_Max_Correlation_NE_One_Generate_Project ...   Passed    0.40 sec
      Start 13: NL_Optimization
13/17 Test #13: NL_Optimization ..........................................   Passed    3.06 sec
      Start 14: NL_Optimization_Residuals
14/17 Test #14: NL_Optimization_Residuals ................................   Passed    9.66 sec
      Start 15: NL_Optimization_Residuals_Generate_Project
15/17 Test #15: NL_Optimization_Residuals_Generate_Project ...............   Passed    3.04 sec
      Start 16: Python_Bindings
16/17 Test #16: Python_Bindings ..........................................   Passed   12.49 sec
      Start 17: CPP_Unit_Tests
17/17 Test #17: CPP_Unit_Tests ...........................................   Passed   12.13 sec

100% tests passed, 0 tests failed out of 17

Total Test time (real) =  46.87 sec
```

Note the total test time may vary based on optimizations and computational resources used

#### Cleaning up Installation Files

To remove all previously built library and executable files run `make clean` in the build directory. If a compilation error occurs because of issues with one of the external libraries, then clean the build and recompile.

### Installing Only the Python Bindings with pip

If one only wants to use the python bindings without the executable, you can also install `SISSO++` using pip. To do this simply run

```bash
pip install .
```

inside the cloned SISSO repository. If you do not have administrative priveldges then you need to use

```bash
pip install --user .
```

This command will build all external libraries and `SISSO++` and store them inside a seperate `_sissopp` module that will be imported into `sissopp` automatically. To then test the code run
```bash
pytest test/pytest
```
And you should see the following output
```
========================================== test session starts ==========================================
platform linux -- Python 3.9.7, pytest-6.2.5, py-1.10.0, pluggy-1.0.0
rootdir: /home/purcell/git/sissopp
collected 83 items

tests/pytest/test_param.py .                                                                      [  1%]
tests/pytest/test_sisso.py .                                                                      [  2%]
tests/pytest/test_descriptor_identifier/test_class_model_from_file.py .                           [  3%]
tests/pytest/test_descriptor_identifier/test_class_model_retrain_svm.py .                         [  4%]
tests/pytest/test_descriptor_identifier/test_class_model_train_from_file.py .                     [  6%]
tests/pytest/test_descriptor_identifier/test_classifier.py .                                      [  7%]
tests/pytest/test_descriptor_identifier/test_log_reg_model_from_file.py .                         [  8%]
tests/pytest/test_descriptor_identifier/test_log_reg_train_model_from_file.py .                   [  9%]
tests/pytest/test_descriptor_identifier/test_log_regressor.py .                                   [ 10%]
tests/pytest/test_descriptor_identifier/test_reg_model_from_file.py .                             [ 12%]
tests/pytest/test_descriptor_identifier/test_reg_model_train_from_file.py .                       [ 13%]
tests/pytest/test_descriptor_identifier/test_regressor.py .                                       [ 14%]
tests/pytest/test_feature_creation/test_feat_generation/test_abs_diff_node.py .                   [ 15%]
tests/pytest/test_feature_creation/test_feat_generation/test_abs_node.py .                        [ 16%]
tests/pytest/test_feature_creation/test_feat_generation/test_add_node.py .                        [ 18%]
tests/pytest/test_feature_creation/test_feat_generation/test_cb_node.py .                         [ 19%]
tests/pytest/test_feature_creation/test_feat_generation/test_cbrt_node.py .                       [ 20%]
tests/pytest/test_feature_creation/test_feat_generation/test_cos_node.py .                        [ 21%]
tests/pytest/test_feature_creation/test_feat_generation/test_div_node.py .                        [ 22%]
tests/pytest/test_feature_creation/test_feat_generation/test_exp_node.py .                        [ 24%]
tests/pytest/test_feature_creation/test_feat_generation/test_feat_node.py .                       [ 25%]
tests/pytest/test_feature_creation/test_feat_generation/test_inv_node.py .                        [ 26%]
tests/pytest/test_feature_creation/test_feat_generation/test_log_node.py .                        [ 27%]
tests/pytest/test_feature_creation/test_feat_generation/test_model_node.py .                      [ 28%]
tests/pytest/test_feature_creation/test_feat_generation/test_mult_node.py .                       [ 30%]
tests/pytest/test_feature_creation/test_feat_generation/test_neg_exp_node.py .                    [ 31%]
tests/pytest/test_feature_creation/test_feat_generation/test_sin_node.py .                        [ 32%]
tests/pytest/test_feature_creation/test_feat_generation/test_six_pow_node.py .                    [ 33%]
tests/pytest/test_feature_creation/test_feat_generation/test_sq_node.py .                         [ 34%]
tests/pytest/test_feature_creation/test_feat_generation/test_sqrt_node.py .                       [ 36%]
tests/pytest/test_feature_creation/test_feat_generation/test_sub_node.py .                        [ 37%]
tests/pytest/test_feature_creation/test_feature_space/test_feature_space.py .                     [ 38%]
tests/pytest/test_feature_creation/test_feature_space/test_gen_feature_space_from_file.py .       [ 39%]
tests/pytest/test_feature_creation/test_feature_space/test_gen_feature_space_selected_from_file.py . [ 40%]
                                                                                                  [ 40%]
tests/pytest/test_feature_creation/test_feature_space/test_units.py .                             [ 42%]
tests/pytest/test_feature_creation/test_parameterize/test_lorentizan.py .                         [ 43%]
tests/pytest/test_feature_creation/test_parameterize/test_param_abs.py .                          [ 44%]
tests/pytest/test_feature_creation/test_parameterize/test_param_abs_diff.py .                     [ 45%]
tests/pytest/test_feature_creation/test_parameterize/test_param_add.py .                          [ 46%]
tests/pytest/test_feature_creation/test_parameterize/test_param_cb.py .                           [ 48%]
tests/pytest/test_feature_creation/test_parameterize/test_param_cbrt.py .                         [ 49%]
tests/pytest/test_feature_creation/test_parameterize/test_param_cos.py .                          [ 50%]
tests/pytest/test_feature_creation/test_parameterize/test_param_div.py .                          [ 51%]
tests/pytest/test_feature_creation/test_parameterize/test_param_exp.py .                          [ 53%]
tests/pytest/test_feature_creation/test_parameterize/test_param_inv.py .                          [ 54%]
tests/pytest/test_feature_creation/test_parameterize/test_param_log.py .                          [ 55%]
tests/pytest/test_feature_creation/test_parameterize/test_param_neg_exp.py .                      [ 56%]
tests/pytest/test_feature_creation/test_parameterize/test_param_sin.py .                          [ 57%]
tests/pytest/test_feature_creation/test_parameterize/test_param_six_pow.py .                      [ 59%]
tests/pytest/test_feature_creation/test_parameterize/test_param_sq.py .                           [ 60%]
tests/pytest/test_feature_creation/test_parameterize/test_param_sqrt.py .                         [ 61%]
tests/pytest/test_feature_creation/test_parameterize/test_param_sub.py .                          [ 62%]
tests/pytest/test_inputs/test_inputs.py .                                                         [ 63%]
tests/pytest/test_model_eval/test_model_node/test_binary_binary.py .                              [ 65%]
tests/pytest/test_model_eval/test_model_node/test_binary_unary.py .                               [ 66%]
tests/pytest/test_model_eval/test_model_node/test_op_model_nodes.py .                             [ 67%]
tests/pytest/test_model_eval/test_model_node/test_unary_binary.py .                               [ 68%]
tests/pytest/test_model_eval/test_model_node/test_unary_unary.py .                                [ 69%]
tests/pytest/test_model_eval/test_models/test_log_reg_model.py .                                  [ 71%]
tests/pytest/test_model_eval/test_models/test_reg_model.py .                                      [ 72%]
tests/pytest/test_model_eval/test_models/test_reg_model_param.py .                                [ 73%]
tests/pytest/test_model_eval/test_param_model_node/test_binary_binary_param.py .                  [ 74%]
tests/pytest/test_model_eval/test_param_model_node/test_binary_unary_param.py .                   [ 75%]
tests/pytest/test_model_eval/test_param_model_node/test_param_op_model_nodes.py .                 [ 77%]
tests/pytest/test_model_eval/test_param_model_node/test_unary_binary_param.py .                   [ 78%]
tests/pytest/test_model_eval/test_param_model_node/test_unary_unary_param.py .                    [ 79%]
tests/pytest/test_nl_opt/test_nl_opt_utils.py .                                                   [ 80%]
tests/pytest/test_sklearn/test_sisso_class_sklearn.py .....                                       [ 86%]
tests/pytest/test_sklearn/test_sisso_log_reg_sklearn.py .....                                     [ 92%]
tests/pytest/test_sklearn/test_sisso_reg_sklearn.py .....                                           [ 98%]
tests/pytest/test_sklearn/test_sklearn_interface.py .                                               [100%]

=========================================== 83 passed in 44.28s ===========================================

```
### Installation Settings for Basic Flags

Terms highlighted in red should always be set, and terms highlighted in blue should be set based on what you want to do.

#### <span style="color:red"> CMAKE_CXX_COMPILER </span>

The C++ compiler used to compile the code

#### CMAKE_C_COMPILER

The C compiler used to compile external dependencies (GTest and {fmt})

#### CMAKE_INSTALL_PREFIX

*Default the main SISSO++ directory*
The directory used to install the final binaries

#### <span style="color:red"> CMAKE_CXX_FLAGS </span>

Define the flags used by the compiler to build the C++ source files. It is recommended to use `-O2` or `-O3` level of optimizations, but it can be changed to match the compiler.

#### CMAKE_C_FLAGS

Define the flags used by the compiler to build the C source files (GTest and {fmt}). It is recommended to use `-O2` or `-O3` level of optimizations, but it can be changed to match the compiler.

#### <span style="color:blue"> CMAKE_INSTALL_PREFIX </span>

*Default: SISSO++ Source Directory*

Path to where the final library and executable files will be placed

### Installation Settings

#### <span style="color:blue"> BUILD_PARAMS </span>

*Default: OFF*

If `BUILD_PARAMS` is ON then build the operators with non-linearly optimized scale and shift parameters, and the relevant optimization files. With this flag on `NLopt`, [the parameterized operators](../cpp_api/param_node), and [the non-linear optimization](../cpp_api/nl_opt) classes will be built.

#### <span style="color:blue"> BUILD_PYTHON </span>

*Default: ON*

If `BUILD_PYTHON` is ON then build the python bindings

#### PYTHON_INSTDIR

*Default: The default python installation directory associated with the binary*

The base directory to install the python bindings to. If this is not the default value, then you must also add the directory to your `PYTHONPATH` environment variable.

#### BUILD_TESTS

*Default: OFF*

If `BULD_TESTS` is ON then build GTest based tests

#### EXTERNAL_BOOST

*Default: OFF*

If `EXTERNAL_BOOST` is ON then use the pre-built Boost Libraries currently in your path or in `$ENV{BOOST_ROOT}`
Here the `-O3` flag is for optimizations, it is recommended to stay as `-O3` or `-O2`, but it can be changed to match compiler requirements.

We recommend to use `-DEXTERNAL_BOOST=OFF` if you are building the python bindings with an anaconda environment or if you are using non-standard C++ or MPI libraries. If the boost libraries have already been built with the same C++ compiler, MPI library, and Python environment, then `EXTERNAL_BOOST=ON` can be used.

#### EXTERNAL_BUILD_N_PROCS

*Default: 1*

The number of processes passed to make when building external libraries.
