# Input Files

To see a sample of the input files look in `/path/to/sisso++/tests/exec_test`.
In this directory there are multiple subdirectories for different types of calculations, but the `default/` directory would be the most common application.

To use the code two files are necessary: `sisso.json` and `data.csv`.
`data.csv` stores all of the data for the calculation in a `csv` file.
The first row in the file corresponds to the feature meta data with the following format `expression (Unit)`.
For example if one of the primary features used in the set is the lattice constant of a material the header would be `lat_param (AA)`.
The first column of the file are sample labels for all of the other rows, and is used to set the sample ids in the output files.

The input parameters are stored in `sisso.json`, here is a list of all possible variables that can be set in `sisso.json`

### `data.csv`
The data file contains all relevant data and metadata to describe the individual features and samples.
The first row of the file corresponds to the features metadata and has the following format `expression (Unit)` or `expression`.
For the cases where no `(Unit)` is included in the header then the feature is considered to be dimensionless.
For example if one of the primary features used in the set is the lattice constant of a material the header would be `lat_param (AA)`, but the number of species in the material would be `n_species` because it is a dimensionless number.


The first column provide the labels for each sample in the data file, and is used to set the sample ids in the output files.
In the simplest case, this can be just a running index.
The data describing the property vector is defined in the column with an `expression` matching the `property_key` filed in the `sisso.json` file, and will not be included in the feature space.
Additionally, an optional `Task` column whose header matches the `task_key` field in the sisso.json file can also be included in the data file.
This column maps each sample to a respective task with a label defined in the task column.
Below in a minimal example of the data file used to learn a model for a materials volume.

```
material, Structure_Type, Volume (AA^3), lat_param (AA)
C, diamond, 45.64, 3.57
Si, diamond, 163.55, 5.47
Ge, diamond, 191.39, 5.76
Sn, diamond, 293.58, 6.65
Pb, diamond, 353.84, 7.07.757
LiF, rock_salt, 67.94, 4.08
NaF, rock_salt, 103.39, 4.69
KF, rock_salt, 159.00, 5.42
RbF, rock_salt, 189.01, 5.74
CsF, rock_salt, 228.33, 6.11
```

### `sisso.json`

All input parameters that can not be extracted from the data file are defined in the `sisso.json` file.

Here is a complete example of a `sisso.json` file where the property and task keys match those in the above data file example.

```json
{
    "data_file": "data.csv",
    "property_key": "Volume",
    "task_key": "Structure_Type",
    "opset": ["add", "sub", "mult", "div", "sq", "cb", "cbrt", "sqrt"],
    "param_opset": [],
    "calc_type": "regression",
    "desc_dim": 2,
    "n_sis_select": 5,
    "max_rung": 2,
    "max_leaves": 4,
    "n_residual": 1,
    "n_models_store": 1,
    "n_rung_store": 1,
    "n_rung_generate": 0,
    "min_abs_feat_val": 1e-5,
    "max_abs_feat_val": 1e8,
    "leave_out_inds": [0, 5],
    "leave_out_frac": 0.25,
    "fix_intercept": false,
    "max_feat_cross_correlation": 1.0,
    "nlopt_seed": 13,
    "global_param_opt": false,
    "reparam_residual": true
}
```

A description of all fields is listed below. Anything highlighted in red should be in all `sisso.json` files, while anything highlighted in blue highlighted terms should be defined depending on the circumstances. All terms listed in black can normally remain as their default values.

#### <span style="color:red">data_file</span>

*Default: "data.csv"*

The name of the csv file where the data is stored.

#### <span style="color:red">property_key</span>

*Default: "prop"*

The expression of the column where the property to be modeled is stored.

#### <span style="color:red">task_key</span>

*Default: "task"*

The expression of the column where the task identification is stored.

#### <span style="color:red">opset</span>

A list containing the set of all operators that will be used during the feature creation step of SISSO. (If empty use all available features)

#### <span style="color:blue">param_opset</span>

A list containing the set of all operators, for which the non-linear scale and bias terms will be optimized, that will be used during the feature creation step of SISSO. (If empty none of the available features are used)

#### <span style="color:red">calc_type</span>

*Default: "regression"*

The type of calculation to run either regression, log regression, or classification

#### <span style="color:red">desc_dim</span>

The maximum dimension of the model to be created (no default value)

#### <span style="color:red">n_sis_select</span>

The number of features that SIS selects over each iteration (no default value)

#### <span style="color:red">max_rung</span>

The maximum rung of the feature (height of the tallest possible binary expression tree - 1) (no default value)

#### max_leaves

*Default: 2^max_rung*

The maximum number of primary features with replacement allowed in a final feature

#### <span style="color:red">n_residual</span>

*Default: 1*

Number of residuals to used to select the next subset of materials in the iteration. (Affects SIS after the 1D model)

#### n_models_store

*Default: `n_residual`*

Number of models to output as file for each dimension

#### n_rung_store

*Default: `max_rung` - 1*

The number of rungs where all of the training/testing data of the materials are stored in memory.

#### <span style="color:blue">n_rung_generate</span>

*Default: 0*

The number of rungs to generate on the fly during each SIS step. Must be 1 or 0.

#### min_abs_feat_val

*Default: 1e-50*

Minimum absolute value allowed in the feature's training data

#### max_abs_feat_val

*Default: 1e50*

Maximum absolute value allowed in the feature's training data

#### leave_out_inds

The list of indexes from the data set to use as the test set. If empty and `leave_out_frac > 0` the selection will be random

#### <span style="color:blue">leave_out_frac</span>

*Default: 0.0*

Fraction (in decimal form) of the data to use as a test set. This is not used if `leave_out_inds` is set.

#### <span style="color:blue">fix_intercept</span>

*Default: false*

If true set the bias term for regression models to 0.0. For classification problems this must be

#### <span style="color:blue">max_feat_cross_correlation</span>

*Default: 1.0*

The maximum Pearson correlation allowed between selected features

#### nlopt_seed

*Default: 42*

The random seed used for seeding the pseudo-random number generator for NLopt

#### <span style="color:blue">global_param_opt</span>

*Default: false*

If true then attempt to globally optimize the non-linear scale/bias terms for the operators in `param_opset`

#### <span style="color:blue">reparam_residual</span>

*Default: false*

If true then reparameterize features based on the residuals

# Running the Code

## Perform the Calculation
Once the input files are made the code can be run using the following command

```
mpiexec -n 2 /path/to/sisso++/bin/sisso++ sisso.json
```

which will give the following output for the simple problem defined above

```text
time input_parsing: 0.000721931 s
time to generate feat sapce: 0.00288105 s
Projection time: 0.00304198 s
Time to get best features on rank : 1.09673e-05 s
Complete final combination/selection from all ranks: 0.00282502 s
Time for SIS: 0.00595999 s
Time for l0-norm: 0.00260496 s
Projection time: 0.000118971 s
Time to get best features on rank : 1.38283e-05 s
Complete final combination/selection from all ranks: 0.00240111 s
Time for SIS: 0.00276804 s
Time for l0-norm: 0.000256062 s
Train RMSE: 0.293788 AA^3; Test RMSE: 0.186616 AA^3
c0 + a0 * (lat_param^3)

Train RMSE: 0.0936332 AA^3; Test RMSE: 15.8298 AA^3
c0 + a0 * ((lat_param^3)^2) + a1 * (sqrt(lat_param)^3)

```
## Analyzing the Results

Once the calculations are done, two sets of output files are generated.
Two files that summarize the results from SIS in a computer and human readable manner are stored in: `feature_space/` and every model used as a residual for SIS is stored in `models/`.
The human readable file describing the selected feature space is `feature_space/SIS_summary.txt` which contains the projection score (The Pearson correlation to the target property or model residual).
```
# FEAT_ID     Score                   Feature Expression
0             0.99997909235669924     (lat_param^3)
1             0.999036700010245471    ((lat_param^2)^2)
2             0.998534266139345261    (lat_param^2)
3             0.996929900301868899    (sqrt(lat_param)^3)
4             0.994755117666830335    lat_param
#-----------------------------------------------------------------------
5             0.0318376000648976157   ((lat_param^3)^3)
6             0.00846237838476477863  ((lat_param^3)^2)
7             0.00742498801557322716  cbrt(cbrt(lat_param))
8             0.00715447033658055554  cbrt(sqrt(lat_param))
9             0.00675695980092700429  sqrt(sqrt(lat_param))
#---------------------------------------------------------------------
```
The computer readable file file is `feature_space/selected_features.txt` and contains a the list of selected features represented by an alphanumeric code where the integers are the index of the feature in the primary feature space and strings represent the operators.
The order of each term in these expressions is the same as the order it would appear using postfix (reverse polish) notation.
```
# FEAT_ID     Feature Postfix Expression (RPN)
0             0|cb
1             0|sq|sq
2             0|sq
3             0|sqrt|cb
4             0
#-----------------------------------------------------------------------
5             0|cb|cb
6             0|cb|sq
7             0|cbrt|cbrt
8             0|sqrt|cbrt
9             0|sqrt|sqrt
#-----------------------------------------------------------------------
```

The model output files are split into train/test files sorted by the dimensionality of the model and by the train RMSE.
The model with the lowest RMSE is stored in the lowest numbered file.
For example `train_dim_2_model_0.dat` will have the best 2D model, `train_dim_2_model_1.dat` would have the second best, etc., whereas `train_dim_1_model_0.dat` will have the best 1D model.
Each model file has a large header containing information about the features selected and model generated
```
# c0 + a0 * (lat_param^3)
# Property Label: $Volume$; Unit of the Property: AA^3
# RMSE: 0.293787533962641; Max AE: 0.56084644346538
# Coefficients
# Task       a0                      c0
#  diamond,  1.000735616997855e+00, -1.551085274074442e-01,
#  rock_salt,  9.998140372873336e-01,  6.405707194855371e-02,
# Feature Rung, Units, and Expressions
# 0;  1; AA^3;                                             0|cb; (lat_param^3); $\left(lat_{param}^3\right)$; (lat_param).^3; lat_param
# Number of Samples Per Task
# Task    , n_mats_train
#  diamond, 4
#  rock_salt, 4
```
The first section of the header summarizes the model by providing a string representation of the model, defines the property's label and unit, and summarizes the error of the model.
```
# c0 + a0 * (lat_param^3)
# Property Label: $Volume$; Unit of the Property: AA^3
# RMSE: 0.293787533962641; Max AE: 0.56084644346538
```
Next the linear coefficients (as shown in the first line) for each task is listed.
```
# Coefficients
# Task       a0                      c0
#  diamond,  1.000735616997855e+00, -1.551085274074442e-01,
#  rock_salt,  9.998140372873336e-01,  6.405707194855371e-02,
```
Then a description of each feature in the model is listed, including units and various expressions.
```
# Feature Rung, Units, and Expressions
# 0;  1; AA^3;                                             0|cb; (lat_param^3); $\left(lat_{param}^3\right)$; (lat_param).^3; lat_param
```
Finally information about the number of samples in each task is given
```
# Number of Samples Per Task
# Task    , n_mats_train
#  diamond, 4
#  rock_salt, 4
```

The header for the test data files contain the same information as the training file, with an additional line at the end to list all indexes included in the test set:
```
# Test Indexes: [ 0, 5 ]
```
These indexes can be used to reproduce the results by setting `leave_out_inds` to those listed on this line.

After this header in both file the following data is stored in the file:

```
# Sample ID , Property Value        ,  Property Value (EST) ,  Feature 0 Value
```
With this data, one can plot and analyzed the model, e.g., by using the python binding.



## Using the Python Library
To see how the python interface can be used refer to the [tutorials](../tutorial/2_python.md).
If you get an error about not being able to load MKL libraries, you may have to run `conda install numpy` to get proper linking.

