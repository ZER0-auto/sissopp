.. _api_classification:

Classification
==============
.. toctree::
    :maxdepth: 2

ConvexHull1D
------------
.. doxygenfile:: ConvexHull1D.hpp
   :project: SISSO++

LPWrapper
---------
.. doxygenfile:: LPWrapper.hpp
   :project: SISSO++

prop_sorted_d_mat
-----------------
.. doxygenfile:: prop_sorted_d_mat.hpp
   :project: SISSO++

SVMWrapper
----------
.. doxygenfile:: SVMWrapper.hpp
   :project: SISSO++

