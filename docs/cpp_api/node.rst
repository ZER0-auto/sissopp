.. _api_node:

Nodes
=====
.. toctree::
    :maxdepth: 2

Node
----
.. doxygenfile:: Node.hpp
   :project: SISSO++

FeatureNode
-----------
.. doxygenfile:: FeatureNode.hpp
   :project: SISSO++

ModelNode
---------
.. doxygenfile:: ModelNode.hpp
   :project: SISSO++


OperatorNode
------------
.. doxygenfile:: OperatorNode.hpp
   :project: SISSO++

AddNode
-------
.. doxygenfile:: add.hpp
   :project: SISSO++

SubNode
-------
.. doxygenfile:: subtract.hpp
   :project: SISSO++

AbsDiffNode
-----------
.. doxygenfile:: absolute_difference.hpp
   :project: SISSO++

MultNode
--------
.. doxygenfile:: multiply.hpp
   :project: SISSO++

DivNode
-------
.. doxygenfile:: divide.hpp
   :project: SISSO++

InvNode
-------
.. doxygenfile:: inverse.hpp
   :project: SISSO++

SqNode
------
.. doxygenfile:: square.hpp
   :project: SISSO++

CbNode
------
.. doxygenfile:: cube.hpp
   :project: SISSO++

SixPowNode
----------
.. doxygenfile:: sixth_power.hpp
   :project: SISSO++

SqrtNode
--------
.. doxygenfile:: square_root.hpp
   :project: SISSO++

CbrtNode
--------
.. doxygenfile:: cube_root.hpp
   :project: SISSO++

ExpNode
-------
.. doxygenfile:: exponential.hpp
   :project: SISSO++

NegExpNode
----------
.. doxygenfile:: negative_exponential.hpp
   :project: SISSO++

LogNode
-------
.. doxygenfile:: log.hpp
   :project: SISSO++

SinNode
-------
.. doxygenfile:: sin.hpp
   :project: SISSO++

CosNode
-------
.. doxygenfile:: cos.hpp
   :project: SISSO++

AbsNode
-------
.. doxygenfile:: absolute_value.hpp
   :project: SISSO++
