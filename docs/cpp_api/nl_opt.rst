.. _api_nl_opt:

Non-Linear Parameter Optimization
=================================
.. toctree::
    :maxdepth: 2

NLOptimizer
-----------
.. doxygenfile:: NLOptimizer.hpp
   :project: SISSO++

NLOptimizerRegression
---------------------
.. doxygenfile:: NLOptimizerRegression.hpp
   :project: SISSO++

NLOptimizerLogRegression
------------------------
.. doxygenfile:: NLOptimizerLogRegression.hpp
   :project: SISSO++

NLOptimizerClassification
-------------------------
.. doxygenfile:: NLOptimizerClassification.hpp
   :project: SISSO++
