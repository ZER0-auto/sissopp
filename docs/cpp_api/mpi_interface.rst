.. _api_mpi:

MPI
===
.. toctree::
    :maxdepth: 2

MPI_Interface
-------------
.. doxygenfile:: MPI_Interface.hpp
   :project: SISSO++

MPI_ops
-------
.. doxygenfile:: MPI_Ops.hpp
   :project: SISSO++
