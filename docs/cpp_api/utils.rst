.. _api_utils:

General Utilities
=================
.. toctree::
    :maxdepth: 2

enum
----
.. doxygenfile:: enum.hpp
   :project: SISSO++

String Utilities
----------------
.. doxygenfile:: string_utils.hpp
   :project: SISSO++

Vector Utilities
----------------
.. doxygenfile:: vector_utils.hpp
   :project: SISSO++

