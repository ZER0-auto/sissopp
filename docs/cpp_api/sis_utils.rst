.. _api_sis_utils:

Sure-Independence Screening Utilities
=====================================
.. toctree::
    :maxdepth: 2

Compare Features
----------------
.. doxygenfile:: compare_features.hpp
   :project: SISSO++


Projection Functions
--------------------
.. doxygenfile:: project.hpp
   :project: SISSO++
