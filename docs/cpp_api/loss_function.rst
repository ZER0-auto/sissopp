.. _api_loss:

Loss Functions
==============
.. toctree::
    :maxdepth: 2

LossFunction
------------
.. doxygenfile:: LossFunction.hpp
   :project: SISSO++

LossFunctionConvexHull
----------------------
.. doxygenfile:: LossFunctionConvexHull.hpp
   :project: SISSO++

LossFunctionPearsonRMSE
-----------------------
.. doxygenfile:: LossFunctionPearsonRMSE.hpp
   :project: SISSO++

LossFunctionLogPearsonRMSE
--------------------------
.. doxygenfile:: LossFunctionLogPearsonRMSE.hpp
   :project: SISSO++

Loss Function Utilities
-----------------------
.. doxygenfile:: loss_function/utils.hpp
   :project: SISSO++
