.. _py_api_solvers:

SISSOSolver
-----------

.. autoclass:: sissopp.SISSOSolver
    :special-members: __init__
    :members:
    :undoc-members:


SISSOClassifier
---------------

.. autoclass:: sissopp.SISSOClassifier
    :special-members: __init__
    :members:
    :undoc-members:


SISSORegressor
--------------

.. autoclass:: sissopp.SISSORegressor
    :special-members: __init__
    :members:
    :undoc-members:


SISSOLogRegressor
-----------------

.. autoclass:: sissopp.SISSOLogRegressor
    :special-members: __init__
    :members:
    :undoc-members:


