.. _py_api_post_parity

Parity Plots
------------

.. currentmodule:: sissopp.postprocess.plot.parity_plot

.. autofunction:: plot_model_parity_plot
