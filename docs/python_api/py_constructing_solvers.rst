.. _py_api_get_solvers:


Constructing Solvers
--------------------

.. currentmodule:: sissopp.py_interface.get_solver

.. autofunction:: get_fs_solver
