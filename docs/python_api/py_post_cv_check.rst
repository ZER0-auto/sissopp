.. _py_api_cv_conv


Cross-Validation Convergence
----------------------------

.. currentmodule:: sissopp.postprocess.check_cv_convergence

.. autofunction:: jackknife_cv_conv_est

.. currentmodule:: sissopp.postprocess.plot.cv_error_plot

.. autofunction:: plot_validation_rmse

.. autofunction:: plot_errors_dists

.. autofunction:: add_violin

.. autofunction:: add_boxplot

.. autofunction:: add_pointplot
