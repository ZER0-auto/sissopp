.. _py_api_post_an_fs


Feature Space Analysis
----------------------

.. currentmodule:: sissopp.postprocess.feature_space_analysis

.. autofunction:: get_prevelance_of_primary_features
