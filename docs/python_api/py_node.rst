.. _py_api_node:

Node
----
.. autoclass:: sissopp.Node
    :special-members: __init__
    :members:
    :undoc-members:


FeatureNode
-----------
.. autoclass:: sissopp.FeatureNode
    :special-members: __init__
    :members:
    :undoc-members:

ModelNode
---------
.. autoclass:: sissopp.ModelNode
    :special-members: __init__
    :members:
    :undoc-members:

OperatorNode1
-------------
.. autoclass:: sissopp.OperatorNode1
    :special-members: __init__
    :members:
    :undoc-members:

AddNode
-------
.. autoclass:: sissopp.AddNode
    :special-members: __init__
    :members:
    :undoc-members:

SubNode
-------
.. autoclass:: sissopp.SubNode
    :special-members: __init__
    :members:
    :undoc-members:

AbsDiffNode
-----------
.. autoclass:: sissopp.AbsDiffNode
    :special-members: __init__
    :members:
    :undoc-members:

MultNode
--------
.. autoclass:: sissopp.MultNode
    :special-members: __init__
    :members:
    :undoc-members:

DivNode
-------
.. autoclass:: sissopp.DivNode
    :special-members: __init__
    :members:
    :undoc-members:

InvNode
-------
.. autoclass:: sissopp.InvNode
    :special-members: __init__
    :members:
    :undoc-members:

SqNode
------
.. autoclass:: sissopp.SqNode
    :special-members: __init__
    :members:
    :undoc-members:

CbNode
------
.. autoclass:: sissopp.CbNode
    :special-members: __init__
    :members:
    :undoc-members:

SixPowNode
----------
.. autoclass:: sissopp.SixPowNode
    :special-members: __init__
    :members:
    :undoc-members:

SqrtNode
--------
.. autoclass:: sissopp.SqrtNode
    :special-members: __init__
    :members:
    :undoc-members:

CbrtNode
--------
.. autoclass:: sissopp.CbrtNode
    :special-members: __init__
    :members:
    :undoc-members:

ExpNode
-------
.. autoclass:: sissopp.ExpNode
    :special-members: __init__
    :members:
    :undoc-members:

NegExpNode
----------
.. autoclass:: sissopp.NegExpNode
    :special-members: __init__
    :members:
    :undoc-members:

LogNode
-------
.. autoclass:: sissopp.LogNode
    :special-members: __init__
    :members:
    :undoc-members:

SinNode
-------
.. autoclass:: sissopp.SinNode
    :special-members: __init__
    :members:
    :undoc-members:

CosNode
-------
.. autoclass:: sissopp.CosNode
    :special-members: __init__
    :members:
    :undoc-members:

AbsNode
-------
.. autoclass:: sissopp.AbsNode
    :special-members: __init__
    :members:
    :undoc-members:

AddParamNode
------------
.. autoclass:: sissopp.AddParamNode
    :special-members: __init__
    :members:
    :undoc-members:

SubParamNode
------------
.. autoclass:: sissopp.SubParamNode
    :special-members: __init__
    :members:
    :undoc-members:

AbsDiffParamNode
----------------
.. autoclass:: sissopp.AbsDiffParamNode
    :special-members: __init__
    :members:
    :undoc-members:

MultParamNode
-------------
.. autoclass:: sissopp.MultParamNode
    :special-members: __init__
    :members:
    :undoc-members:

DivParamNode
------------
.. autoclass:: sissopp.DivParamNode
    :special-members: __init__
    :members:
    :undoc-members:

InvParamNode
------------
.. autoclass:: sissopp.InvParamNode
    :special-members: __init__
    :members:
    :undoc-members:

SqParamNode
-----------
.. autoclass:: sissopp.SqParamNode
    :special-members: __init__
    :members:
    :undoc-members:

CbParamNode
-----------
.. autoclass:: sissopp.CbParamNode
    :special-members: __init__
    :members:
    :undoc-members:

SixPowParamNode
---------------
.. autoclass:: sissopp.SixPowParamNode
    :special-members: __init__
    :members:
    :undoc-members:

SqrtParamNode
-------------
.. autoclass:: sissopp.SqrtParamNode
    :special-members: __init__
    :members:
    :undoc-members:

CbrtParamNode
-------------
.. autoclass:: sissopp.CbrtParamNode
    :special-members: __init__
    :members:
    :undoc-members:

ExpParamNode
------------
.. autoclass:: sissopp.ExpParamNode
    :special-members: __init__
    :members:
    :undoc-members:

NegExpParamNode
---------------
.. autoclass:: sissopp.NegExpParamNode
    :special-members: __init__
    :members:
    :undoc-members:

LogParamNode
------------
.. autoclass:: sissopp.LogParamNode
    :special-members: __init__
    :members:
    :undoc-members:

SinParamNode
------------
.. autoclass:: sissopp.SinParamNode
    :special-members: __init__
    :members:
    :undoc-members:

CosParamNode
------------
.. autoclass:: sissopp.CosParamNode
    :special-members: __init__
    :members:
    :undoc-members:

AbsParamNode
------------
.. autoclass:: sissopp.AbsParamNode
    :special-members: __init__
    :members:
    :undoc-members:

