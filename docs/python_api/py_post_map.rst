.. _py_api_post_map


Plotting 2D Maps
----------------

.. currentmodule:: sissopp.postprocess.plot.maps

.. autofunction:: plot_2d_map
