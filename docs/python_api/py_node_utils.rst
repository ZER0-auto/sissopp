.. _py_api_node_utilities:

Node Utilities
==============
.. toctree::
    :maxdepth: 2

Node Python Utils
-----------------
.. autofunction:: sissopp.phi_selected_from_file

Node Data Storage
-----------------
.. autofunction:: sissopp.initialize_values_arr
.. autofunction:: sissopp.initialize_d_matrix_arr

Units
-----
.. autoclass:: sissopp.Unit
    :special-members: __init__
    :members:
    :undoc-members:
