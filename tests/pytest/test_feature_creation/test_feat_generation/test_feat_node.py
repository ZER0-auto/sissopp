# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp import (
    FeatureNode,
    Unit,
    initialize_values_arr,
)

import numpy as np
import pickle

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


def test_feat_node():
    task_sizes_train = [10]
    task_sizes_test = [10]

    initialize_values_arr(task_sizes_train, task_sizes_test, 3, 0)

    data_1 = np.random.random(task_sizes_train[0]) * 1e10 + 1e-10
    test_data_1 = np.random.random(task_sizes_test[0]) * 1e10 + 1e-10

    data_2 = np.random.random(task_sizes_train[0] + 1) * 2e4 - 1e4
    test_data_2 = np.random.random(task_sizes_test[0] + 1) * 2e4 - 1e4

    feat_1 = FeatureNode(0, "t_a", data_1, test_data_1, Unit("s"))
    feat_2 = FeatureNode(1, "v_a", data_1, test_data_1, Unit("s"))

    assert feat_1.n_leaves == 1

    try:
        feat_3 = FeatureNode(2, "t_b", data_2, test_data_2, Unit("s"))
        raise InvalidFeatureMade("FeatureNode created with wrong number of samples.")
    except RuntimeError:
        pass

    decomp = feat_1.primary_feat_decomp
    assert len(decomp.keys()) == 1
    assert decomp["t_a"] == 1

    try:
        feat_1.feat(2)
        raise ValueError("Accessing feature that should throw an error")
    except:
        pass
    assert feat_1.matlab_fxn_expr == "t_a"

    # Check pickling
    pickled = pickle.dumps(feat_1)
    feat_1_unpick = pickle.loads(pickled)
    assert np.all(feat_1_unpick.value == feat_1.value)
    assert np.all(feat_1_unpick.test_value == feat_1.test_value)
    assert feat_1_unpick.expr == feat_1.expr
    assert feat_1_unpick.unit == feat_1.unit
    assert feat_1_unpick.arr_ind == feat_1.arr_ind
    assert feat_1_unpick.feat_ind == feat_1.feat_ind


if __name__ == "__main__":
    test_feat_node()
