# Copyright 2022 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pandas as pd

from pathlib import Path

from sissopp.py_interface import create_inputs
from sissopp.sklearn import SISSORegressor, cross_validate_from_splitter

from sklearn.model_selection import KFold

import shutil

parent = Path(__file__).parents[1]


def test_sklearn_interface():
    df = pd.read_csv(str(parent / "data.csv"), index_col=0)
    model = SISSORegressor(
        prop_label="Prop",
        max_rung=2,
        n_sis_select=20,
        n_dim=2,
        n_residual=1,
        task_key="Task",
        workdir="sklearn_test",
    )
    cols = df.columns.tolist()
    del cols[1]
    X = df.loc[:, cols]
    y = df.loc[:, "Prop"].values
    k = 5
    kf = KFold(n_splits=k, random_state=13, shuffle=True)
    acc_score = cross_validate_from_splitter(X, y, model, kf)

    avg_acc_score = sum(acc_score) / k
    assert avg_acc_score < 1e-5
    shutil.rmtree("sklearn_test")


if __name__ == "__main__":
    test_sklearn_interface()
