# Copyright 2022 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import numpy as np
import pandas as pd

from sissopp.sklearn import SISSOClassifier
from sissopp import Unit, Inputs

np.random.seed(15)


def test_sklearn_sisso_classifier():
    def test_fxn(X):
        y = 1.0 + (X[:, 0] < 0.0)
        return y

    sisso = SISSOClassifier(
        prop_label="prop",
        prop_unit=Unit(),
        n_dim=1,
        max_rung=1,
        n_sis_select=5,
        n_residual=5,
        workdir="test_reg_sklearn",
        clean_workdir=True,
    )

    X = np.random.uniform(-100.0, 100.0, (100, 5))
    X = X + 2.0 * np.sign(X)
    y = test_fxn(X)

    X_test = np.random.uniform(-100.0, 100.0, (25, 5))
    X_test = X_test + 5.0 * np.sign(X_test)
    y_test = test_fxn(X_test)

    sisso.fit(X, y)
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X), y)[0, 1]) < 1e-5
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X_test), y_test)[0, 1]) < 1e-5


def test_sklearn_sisso_columns_index_classifier():
    def test_fxn(X):
        y = 1.0 + (X[:, 0] < 0.0)
        return y

    sisso = SISSOClassifier(
        prop_label="prop",
        prop_unit=Unit(),
        n_dim=1,
        max_rung=1,
        n_sis_select=5,
        n_residual=5,
        workdir="test_reg_sklearn",
        clean_workdir=True,
    )

    X = np.random.uniform(-100.0, 100.0, (100, 5))
    X = X + 2.0 * np.sign(X)
    y = test_fxn(X)

    X_test = np.random.uniform(-100.0, 100.0, (25, 5))
    X_test = X_test + 5.0 * np.sign(X_test)
    y_test = test_fxn(X_test)

    index = [f"{ii:03d}" for ii in range(100)]
    sisso.fit(X, y, columns=["a", "b", "c", "e", "f"], index=index)
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X), y)[0, 1]) < 1e-5
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X_test), y_test)[0, 1]) < 1e-5


def test_sklearn_sisso_df_classifier():
    def test_fxn(X):
        y = 1.0 + (X[:, 0] < 0.0)
        return y

    sisso = SISSOClassifier(
        prop_label="prop",
        prop_unit=Unit(),
        n_dim=1,
        max_rung=1,
        n_sis_select=5,
        n_residual=5,
        workdir="test_reg_sklearn",
        clean_workdir=True,
    )

    X = np.random.uniform(-100.0, 100.0, (100, 5))
    X = X + 2.0 * np.sign(X)
    y = test_fxn(X)

    X_test = np.random.uniform(-100.0, 100.0, (25, 5))
    X_test = X_test + 5.0 * np.sign(X_test)
    y_test = test_fxn(X_test)

    index = [f"{ii:03d}" for ii in range(100)]
    index_test = [f"{ii:03d}" for ii in range(25)]
    columns = ["a", "b", "c", "e", "f"]

    X = pd.DataFrame(data=X, index=index, columns=columns)
    X_test = pd.DataFrame(data=X_test, index=index_test, columns=columns)

    sisso.fit(X, y)
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X), y)[0, 1]) < 1e-5
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X_test), y_test)[0, 1]) < 1e-5


def test_sklearn_sisso_classifier_omp():
    def test_fxn(X):
        y = 1.0 + (X[:, 0] < 0.0)
        return y

    sisso = SISSOClassifier.OMP(
        prop_label="prop",
        prop_unit=Unit(),
        n_dim=1,
        workdir="test_reg_sklearn",
        clean_workdir=True,
    )

    X = np.random.uniform(-100.0, 100.0, (100, 5))
    X = X + 2.0 * np.sign(X)
    y = test_fxn(X)

    X_test = np.random.uniform(-100.0, 100.0, (25, 5))
    X_test = X_test + 5.0 * np.sign(X_test)
    y_test = test_fxn(X_test)

    index = [f"{ii:03d}" for ii in range(100)]
    index_test = [f"{ii:03d}" for ii in range(25)]
    columns = ["a", "b", "c", "e", "f"]

    X = pd.DataFrame(data=X, index=index, columns=columns)
    X_test = pd.DataFrame(data=X_test, index=index_test, columns=columns)

    sisso.fit(X, y)
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X), y)[0, 1]) < 1e-5
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X_test), y_test)[0, 1]) < 1e-5


def test_sklearn_sisso_classifier_inputs():
    def test_fxn(X):
        y = 1.0 + (X[:, 0] < 0.0)
        return y

    X = np.random.uniform(-100.0, 100.0, (100, 5))
    X = X + 2.0 * np.sign(X)
    y = test_fxn(X)

    X_test = np.random.uniform(-100.0, 100.0, (25, 5))
    X_test = X_test + 5.0 * np.sign(X_test)
    y_test = test_fxn(X_test)

    index = [f"{ii:03d}" for ii in range(100)]
    index_test = [f"{ii:03d}" for ii in range(25)]
    columns = ["a", "b", "c", "e", "f"]

    X = pd.DataFrame(data=X, index=index, columns=columns)
    X_test = pd.DataFrame(data=X_test, index=index_test, columns=columns)

    inputs = Inputs()
    inputs.task_key = ""
    inputs.prop_label = "prop"
    inputs.prop_unit = Unit()
    inputs.n_dim = 1
    inputs.max_rung = 1
    inputs.n_residual = 1
    inputs.allowed_ops = [
        "add",
        "sub",
        "mult",
        "div",
        "abs_diff",
        "inv",
        "abs",
        "cos",
        "sin",
        "exp",
        "neg_exp",
        "log",
        "sq",
        "sqrt",
        "cb",
        "cbrt",
        "six_pow",
    ]
    sisso = SISSOClassifier.from_inputs(
        inputs,
        workdir="test_reg_sklearn",
        clean_workdir=True,
    )

    sisso.fit(X, y)
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X), y)[0, 1]) < 1e-5
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X_test), y_test)[0, 1]) < 1e-5


if __name__ == "__main__":
    test_sklearn_sisso_classifier()
    test_sklearn_sisso_columns_index_classifier()
    test_sklearn_sisso_df_classifier()
    test_sklearn_sisso_classifier_omp()
    test_sklearn_sisso_classifier_inputs()
