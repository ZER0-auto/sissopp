# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import shutil
import numpy as np

np.random.seed(0)
from sissopp import (
    FeatureNode,
    FeatureSpace,
    Inputs,
    SISSOLogRegressor,
    Unit,
    phi_selected_from_file,
    initialize_values_arr,
)
from pathlib import Path

parent = Path(__file__).parent


def test_log_reg_restart():
    task_sizes_train = [90]
    task_sizes_test = [10]

    initialize_values_arr(task_sizes_train, task_sizes_test, 10, 2)

    inputs = Inputs()
    inputs.phi_0 = [
        FeatureNode(
            ff,
            f"feat_{ff}",
            np.random.random(task_sizes_train[0]) * 1e2,
            np.random.random(task_sizes_test[0]) * 1e2,
            Unit(),
        )
        for ff in range(10)
    ]

    inputs.task_names = ["task"]
    inputs.sample_ids_train = [
        str(ii)
        for ii in range(task_sizes_test[0], task_sizes_test[0] + task_sizes_train[0])
    ]
    inputs.sample_ids_test = [str(ii) for ii in range(task_sizes_test[0])]

    a0 = 0.95
    a1 = 1.01
    c0 = np.random.random() * 100.0
    inputs.prop_train = (
        c0 * np.power(inputs.phi_0[0].value, a0) * np.power(inputs.phi_0[2].value, a1)
    )
    inputs.prop_test = (
        c0
        * np.power(inputs.phi_0[0].test_value, a0)
        * np.power(inputs.phi_0[2].test_value, a1)
    )

    inputs.allowed_ops = ["add", "sub"]
    inputs.calc_type = "log_regression"
    inputs.max_rung = 1
    inputs.n_sis_select = 5
    inputs.n_dim = 1
    inputs.n_residual = 1
    inputs.n_models_store = 1
    inputs.task_sizes_train = task_sizes_train
    inputs.task_sizes_test = task_sizes_test
    inputs.leave_out_inds = list(range(task_sizes_test[0]))
    inputs.fix_intercept = False
    inputs.prop_label = "prop"
    inputs.prop_unit = Unit("m")
    inputs.phi_out_file = "feature_space/phi.txt"

    feat_space = FeatureSpace(inputs)
    sisso = SISSOLogRegressor(inputs, feat_space)
    sisso.fit()

    inputs.n_dim = 2
    feat_space_restart = FeatureSpace(
        inputs.phi_out_file,
        inputs.phi_0,
        inputs.prop_train,
        inputs.task_sizes_train,
        inputs.calc_type,
        inputs.n_sis_select,
        inputs.cross_cor_max,
        [],
        False,
        True,
    )
    phi_sel_prev = phi_selected_from_file(
        "feature_space/selected_features.txt", inputs.phi_0, []
    )
    feat_space_restart.phi_selected = phi_sel_prev
    sisso_restart = SISSOLogRegressor(inputs, feat_space_restart)
    sisso_restart.add_models(
        [["models/train_dim_1_model_0.dat"]], [["models/test_dim_1_model_0.dat"]]
    )
    sisso_restart.fit()

    shutil.rmtree("models/")
    shutil.rmtree("feature_space/")

    assert sisso_restart.models[1][0].rmse < 1e-7
    assert sisso_restart.models[1][0].test_rmse < 1e-7


if __name__ == "__main__":
    test_log_reg_restart()
