# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import shutil
import numpy as np

np.random.seed(0)
from sissopp import (
    Inputs,
    FeatureNode,
    FeatureSpace,
    Unit,
    initialize_values_arr,
    phi_selected_from_file,
    SISSOClassifier,
)
import matplotlib.pyplot as plt
import pickle
from pathlib import Path

parent = Path(__file__).parent


def test_class_restart():
    task_sizes_train = [80]
    task_sizes_test = [20]

    initialize_values_arr(task_sizes_train, task_sizes_test, 10, 2)

    inputs = Inputs()
    inputs.max_param_depth = 0
    inputs.task_key = "task"
    inputs.sample_ids_train = [str(ii) for ii in range(20, 100)]
    inputs.sample_ids_test = [str(ii) for ii in range(20)]

    train_data = np.random.random((10, task_sizes_train[0])) * 2.0 - 1.0
    test_data = np.random.random((10, task_sizes_test[0])) * 2.0 - 1.0

    train_data[0][:20] = np.random.random(20) * -1.0 - 1.0
    train_data[0][20:40] = np.random.random(20) + 1.0
    train_data[0][40:60] = np.random.random(20) * -1.0 - 1.0
    train_data[0][60:] = np.random.random(20) + 1.0

    train_data[1][:20] = np.random.random(20) * -1.0 - 1.0
    train_data[1][20:60] = np.random.random(40) + 1.0
    train_data[1][60:] = np.random.random(20) * -1.0 - 1.0

    test_data[0][:5] = np.random.random(5) * -1.0 - 1.0
    test_data[0][5:10] = np.random.random(5) + 1.0
    test_data[0][10:15] = np.random.random(5) * -1.0 - 1.0
    test_data[0][15:] = np.random.random(5) + 1.0

    test_data[1][:5] = np.random.random(5) * -1.0 - 1.0
    test_data[1][5:15] = np.random.random(10) + 1.0
    test_data[1][15:] = np.random.random(5) * -1.0 - 1.0

    inputs.prop_train = (np.sign(train_data[1] * train_data[0]) + 1) // 2
    inputs.prop_test = (np.sign(test_data[1] * test_data[0]) + 1) // 2

    train_data[0][0] = 0.01
    train_data[0][20] = -0.01
    train_data[0][40] = 0.01
    train_data[0][60] = -0.01

    train_data[1][0] = -0.01
    train_data[1][20] = 0.01
    train_data[1][40] = 0.01
    train_data[1][60] = -0.01

    train_data[2][0] = 10.0
    train_data[2][20] = -10.0
    train_data[2][40] = 10.0
    train_data[2][60] = -10.0

    train_data[3][0] = 10.0
    train_data[3][20] = -10.0
    train_data[3][40] = -10.0
    train_data[3][60] = 10.0

    test_data[2][0] = 10.0
    test_data[2][5] = -10.0
    test_data[2][10] = 10.0
    test_data[2][15] = -10.0

    test_data[3][0] = 10.0
    test_data[3][5] = -10.0
    test_data[3][10] = -10.0
    test_data[3][15] = 10.0

    inputs.phi_0 = [
        FeatureNode(
            ff,
            f"feat_{ff}",
            train_data[ff],
            test_data[ff],
            Unit(),
        )
        for ff in range(10)
    ]

    inputs.allowed_ops = ["add", "sub", "mult", "sq", "cb", "sqrt", "cbrt"]
    inputs.calc_type = "classification"
    inputs.max_rung = 1
    inputs.n_sis_select = 10
    inputs.n_dim = 1
    inputs.n_residual = 1
    inputs.n_models_store = 1
    inputs.task_names = ["all"]
    inputs.task_sizes_train = task_sizes_train
    inputs.task_sizes_test = task_sizes_test
    inputs.leave_out_inds = list(range(task_sizes_test[0]))
    inputs.prop_label = "Class"
    inputs.phi_out_file = "feature_space/phi.txt"

    feat_space = FeatureSpace(inputs)

    sisso = SISSOClassifier(inputs, feat_space)
    sisso.fit()

    inputs.n_dim = 2
    feat_space_restart = FeatureSpace(
        inputs.phi_out_file,
        inputs.phi_0,
        inputs.prop_train,
        inputs.task_sizes_train,
        inputs.calc_type,
        inputs.n_sis_select,
        inputs.cross_cor_max,
        [],
        False,
        True,
    )
    phi_sel_prev = phi_selected_from_file(
        "feature_space/selected_features.txt", inputs.phi_0, []
    )
    feat_space_restart.phi_selected = phi_sel_prev
    sisso_restart = SISSOClassifier(inputs, feat_space_restart)
    sisso_restart.add_models(
        [["models/train_dim_1_model_0.dat"]], [["models/test_dim_1_model_0.dat"]]
    )
    sisso_restart.fit()

    shutil.rmtree("models/")
    shutil.rmtree("feature_space/")

    assert sisso_restart.models[0][0].n_convex_overlap_train == 4
    assert sisso_restart.models[1][0].n_convex_overlap_train == 0

    assert sisso_restart.models[0][0].n_convex_overlap_test == 0
    assert sisso_restart.models[1][0].n_convex_overlap_test == 0


if __name__ == "__main__":
    test_class_restart()
