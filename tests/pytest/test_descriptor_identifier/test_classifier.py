# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import shutil
import numpy as np

np.random.seed(0)
from sissopp import (
    Inputs,
    FeatureNode,
    FeatureSpace,
    Unit,
    initialize_values_arr,
    SISSOClassifier,
)
import matplotlib.pyplot as plt
import pickle

np.random.seed(13)


def test_sisso_classifier():
    task_sizes_train = [80]
    task_sizes_test = [20]

    initialize_values_arr(task_sizes_train, task_sizes_test, 10, 2)

    inputs = Inputs()
    inputs.task_key = "task"
    inputs.sample_ids_train = [str(ii) for ii in range(20, 100)]
    inputs.sample_ids_test = [str(ii) for ii in range(20)]

    train_data = np.random.random((10, task_sizes_train[0])) * 2.0 - 1.0
    test_data = np.random.random((10, task_sizes_test[0])) * 2.0 - 1.0

    train_data[0][:20] = np.random.random(20) * -1.0 - 1.0
    train_data[0][20:40] = np.random.random(20) + 1.0
    train_data[0][40:60] = np.random.random(20) * -1.0 - 1.0
    train_data[0][60:] = np.random.random(20) + 1.0

    train_data[1][:20] = np.random.random(20) * -1.0 - 1.0
    train_data[1][20:60] = np.random.random(40) + 1.0
    train_data[1][60:] = np.random.random(20) * -1.0 - 1.0

    test_data[0][:5] = np.random.random(5) * -1.0 - 1.0
    test_data[0][5:10] = np.random.random(5) + 1.0
    test_data[0][10:15] = np.random.random(5) * -1.0 - 1.0
    test_data[0][15:] = np.random.random(5) + 1.0

    test_data[1][:5] = np.random.random(5) * -1.0 - 1.0
    test_data[1][5:15] = np.random.random(10) + 1.0
    test_data[1][15:] = np.random.random(5) * -1.0 - 1.0

    inputs.prop_train = (np.sign(train_data[1] * train_data[0]) + 1) // 2
    inputs.prop_test = (np.sign(test_data[1] * test_data[0]) + 1) // 2

    train_data[0][0] = 0.01
    train_data[0][20] = -0.01
    train_data[0][40] = 0.01
    train_data[0][60] = -0.01

    train_data[1][0] = -0.01
    train_data[1][20] = 0.01
    train_data[1][40] = 0.01
    train_data[1][60] = -0.01

    train_data[2][0] = 10.0
    train_data[2][20] = -10.0
    train_data[2][40] = 10.0
    train_data[2][60] = -10.0

    train_data[3][0] = 10.0
    train_data[3][20] = -10.0
    train_data[3][40] = -10.0
    train_data[3][60] = 10.0

    test_data[2][0] = 10.0
    test_data[2][5] = -10.0
    test_data[2][10] = 10.0
    test_data[2][15] = -10.0

    test_data[3][0] = 10.0
    test_data[3][5] = -10.0
    test_data[3][10] = -10.0
    test_data[3][15] = 10.0

    inputs.phi_0 = [
        FeatureNode(
            ff,
            f"feat_{ff}",
            train_data[ff],
            test_data[ff],
            Unit(),
        )
        for ff in range(10)
    ]

    inputs.allowed_ops = ["add", "sub", "mult", "sq", "cb", "sqrt", "cbrt"]
    inputs.calc_type = "classification"
    inputs.max_rung = 1
    inputs.n_sis_select = 10
    inputs.n_dim = 2
    inputs.n_residual = 1
    inputs.n_models_store = 1
    inputs.task_names = ["all"]
    inputs.task_sizes_train = task_sizes_train
    inputs.task_sizes_test = task_sizes_test
    inputs.leave_out_inds = list(range(task_sizes_test[0]))
    inputs.prop_label = "Class"

    feat_space = FeatureSpace(inputs)

    sisso = SISSOClassifier(inputs, feat_space)
    sisso.fit()

    shutil.rmtree("models/")
    shutil.rmtree("feature_space/")

    assert sisso.models[0][0].n_convex_overlap_train == 4
    assert sisso.models[1][0].n_convex_overlap_train == 0

    assert sisso.models[0][0].n_convex_overlap_test == 0
    assert sisso.models[1][0].n_convex_overlap_test == 0

    assert np.all(sisso.prop_train == inputs.prop_train)
    assert np.all(sisso.prop_test == inputs.prop_test)

    assert np.all(sisso.task_sizes_train == inputs.task_sizes_train)
    assert np.all(sisso.task_sizes_test == inputs.task_sizes_test)

    # Check Pickeling
    pickled = pickle.dumps(sisso)
    sisso_unpick = pickle.loads(pickled)

    assert np.all(sisso.sample_ids_train == sisso_unpick.sample_ids_train)
    assert np.all(sisso.sample_ids_test == sisso_unpick.sample_ids_test)
    assert np.all(sisso.task_names == sisso_unpick.task_names)
    assert np.all(sisso.task_sizes_train == sisso_unpick.task_sizes_train)
    assert np.all(sisso.task_sizes_test == sisso_unpick.task_sizes_test)
    assert np.all(sisso.leave_out_inds == sisso_unpick.leave_out_inds)
    assert np.all(sisso.prop_train == sisso_unpick.prop_train)
    assert np.all(sisso.prop_test == sisso_unpick.prop_test)
    assert np.all(
        [
            f1.expr == f2.expr
            for f1, f2 in zip(sisso.feat_space.phi, sisso_unpick.feat_space.phi)
        ]
    )
    assert sisso.prop_unit == sisso_unpick.prop_unit
    assert sisso.prop_label == sisso_unpick.prop_label
    assert sisso.n_dim == sisso_unpick.n_dim
    assert sisso.n_residual == sisso_unpick.n_residual
    assert sisso.n_models_store == sisso_unpick.n_models_store
    assert sisso.fix_intercept == sisso_unpick.fix_intercept
    assert np.all(
        [str(m1) == str(m2) for m1, m2 in zip(sisso.models, sisso_unpick.models)]
    )


if __name__ == "__main__":
    test_sisso_classifier()
