# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp.postprocess.load_models import load_model
from pathlib import Path

import numpy as np

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


parent = Path(__file__).parent


def test_log_reg_model_from_file():
    model = load_model(
        str(parent / "model_files/train_log_regressor.dat"),
    )

    assert np.all(np.abs(model.fit - model.prop_train) < 1e-7)
    assert np.all(np.abs(model.train_error) < 1e-7)

    assert model.task_sizes_train == [95]
    assert model.task_sizes_test == [0]
    assert model.leave_out_inds == []

    assert model.feats[0].postfix_expr == "1|0|add"
    assert model.feats[1].postfix_expr == "3|1|abd"

    actual_coefs = [
        [1.20, -1.95],
    ]

    assert np.all(
        [
            abs(coef - actual) < 1e-8
            for coef, actual in zip(model.coefs[0], actual_coefs[0])
        ]
    )

    assert model.rmse < 1e-7
    assert model.max_ae < 1e-7
    assert model.mae < 1e-7
    assert model.mape < 1e-7
    assert model.percentile_25_ae < 1e-7
    assert model.percentile_50_ae < 1e-7
    assert model.percentile_75_ae < 1e-7
    assert model.percentile_95_ae < 1e-7
    assert (
        model.latex_str
        == "$\\left(\\left(B + A\\right)\\right)^{a_0}\\left(\\left(\\left|D - B\\right|\\right)\\right)^{a_1}$"
    )


if __name__ == "__main__":
    test_log_reg_model_from_file()
