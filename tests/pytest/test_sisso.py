# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import shutil
from pathlib import Path
from sissopp.py_interface import get_fs_solver, create_inputs

parent = Path(__file__).parent


def test_sisso():
    inputs = create_inputs(
        df=str(parent / "data.csv"),
        prop_key="Prop",
        allowed_ops="all",
        allowed_param_ops=[],
        cols="all",
        max_rung=2,
        n_sis_select=20,
        n_dim=2,
        n_residual=1,
        task_key="Task",
        leave_out_frac=0.1,
        leave_out_inds=list(range(6)) + list(range(60, 64)),
    )
    feat_space, sisso = get_fs_solver(inputs, allow_overwrite=True)
    sisso.fit()
    shutil.rmtree("models/")
    shutil.rmtree("feature_space/")

    assert sisso.models[1][0].rmse < 1e-7
    assert sisso.models[1][0].test_rmse < 1e-7


if __name__ == "__main__":
    test_sisso()
