# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp import ModelRegressor
from pathlib import Path

import numpy as np
import shutil

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


parent = Path(__file__).parent


def test_model_reg_eval():
    model = ModelRegressor(
        str(parent / "model_files/param_train.dat"),
        str(parent / "model_files/param_test.dat"),
    )
    data = np.random.uniform(0.001, 2.0, size=(90, 4))

    eval_dat = model.coefs[0][-1] + model.coefs[0][0] * (
        np.log(2.259 * data[:, 1] + 1.3) + np.exp(data[:, 3])
    )

    data_dict = {"B": data[0, 1], "D": data[0, 3]}
    assert abs(model.eval(data_dict) - eval_dat[0]) < 1e-5
    assert abs(model.eval(data[0, :].flatten().tolist()) - eval_dat[0]) < 1e-5
    assert abs(model.eval(data[0, :].flatten()) - eval_dat[0]) < 1e-5

    data_dict = {"A": data[:, 0], "B": data[:, 1], "D": data[:, 3]}
    assert np.linalg.norm(model.eval_many(data_dict) - eval_dat) < 1e-5
    assert np.linalg.norm(model.eval_many(data) - eval_dat) < 1e-5

    task_ids = []
    eval_dat = model.coefs[0][-1] + model.coefs[0][0] * (
        np.log(2.259 * data[:, 1] + 1.3) + np.exp(data[:, 3])
    )

    model.prediction_to_file(
        "model_predict/param_reg.dat",
        eval_dat,
        data,
        np.arange(1000, 1090).astype(str),
        np.array(task_ids, dtype=str),
        np.arange(90).astype(np.int32),
    )
    model_predict = ModelRegressor(
        str(parent / "model_files/param_train.dat"),
        str("model_predict/param_reg.dat"),
    )

    assert model_predict.test_rmse < 1e-5

    model.prediction_to_file(
        "model_predict/param_reg_dict.dat",
        eval_dat,
        data_dict,
        np.arange(1000, 1090).astype(str),
        np.array(task_ids, dtype=str),
        np.arange(90).astype(np.int32),
    )
    model_predict = ModelRegressor(
        str(parent / "model_files/param_train.dat"),
        str("model_predict/param_reg_dict.dat"),
    )

    assert model_predict.test_rmse < 1e-5
    shutil.rmtree("model_predict/")


if __name__ == "__main__":
    test_model_reg_eval()
