# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp import (
    FeatureNode,
    InvNode,
    SqNode,
    ModelNode,
    Unit,
    initialize_values_arr,
)

import numpy as np

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


def test_un_un_model_eval():
    task_sizes_train = [90]
    task_sizes_test = [10]
    initialize_values_arr(task_sizes_train, task_sizes_test, 1, 2)

    data = np.random.uniform(-10.0, 10.0, size=(task_sizes_train[0], 1))
    test_data = np.random.uniform(-10.0, 10.0, size=(task_sizes_test[0], 1))

    feat_1 = FeatureNode(0, "t_a", data[:, 0], test_data[:, 0], Unit())

    node_1 = SqNode(feat_1, 2, 1e-50, 1e50)
    node_2 = InvNode(node_1, 3, 1e-50, 1e50)
    model_node = ModelNode(node_2)

    data_dict = {"t_a": data[0, 0]}
    assert abs(model_node.eval(data[0, :]) - model_node.value[0]) < 1e-5
    assert abs(model_node.eval(data[0, :].tolist()) - model_node.value[0]) < 1e-5
    assert abs(model_node.eval(data_dict) - model_node.value[0]) < 1e-5

    data_dict = {"t_a": data[:, 0]}
    assert np.linalg.norm(model_node.eval_many(data_dict) - model_node.value) < 1e-5
    assert np.linalg.norm(model_node.eval_many(data) - model_node.value) < 1e-5


if __name__ == "__main__":
    test_un_un_model_eval()
