// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <gtest/gtest.h>

#include <cstdint>
#include <descriptor_identifier/solver/KSmallest.hpp>

TEST(KSmallest, ksmallest)
{
    KSmallest<int64_t> ksmallest(10);
    EXPECT_EQ(ksmallest.data().size(), 0);

    for (int64_t val = 100; val > 0; --val) ksmallest.insert(val);

    EXPECT_EQ(ksmallest.data().size(), 10);
    for (auto& val : ksmallest.data())
    {
        EXPECT_LE(val, 10);
    }
}

TEST(KSmallest, merge)
{
    KSmallest<int64_t> ksmallest0(10);
    KSmallest<int64_t> ksmallest1(10);
    for (int64_t val = 100; val > 0; val -= 2)
    {
        ksmallest0.insert(val);
        ksmallest1.insert(val - 1);
    }

    EXPECT_EQ(ksmallest0.data().size(), 10);
    EXPECT_EQ(ksmallest1.data().size(), 10);

    ksmallest0.merge(ksmallest1);

    EXPECT_EQ(ksmallest0.data().size(), 10);
    EXPECT_EQ(ksmallest1.data().size(), 10);

    for (auto& val : ksmallest0.data())
    {
        EXPECT_LE(val, 10);
    }
}
