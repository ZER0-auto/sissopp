// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include "utils/split_range.hpp"

namespace
{
TEST(split_range, exact)
{
    auto sub_range0 = split_range({0, 4}, 0, 4);
    auto sub_range1 = split_range({0, 4}, 1, 4);
    auto sub_range2 = split_range({0, 4}, 2, 4);
    auto sub_range3 = split_range({0, 4}, 3, 4);

    EXPECT_EQ(sub_range0.first, 0);
    EXPECT_EQ(sub_range0.second, 1);

    EXPECT_EQ(sub_range1.first, 1);
    EXPECT_EQ(sub_range1.second, 2);

    EXPECT_EQ(sub_range2.first, 2);
    EXPECT_EQ(sub_range2.second, 3);

    EXPECT_EQ(sub_range3.first, 3);
    EXPECT_EQ(sub_range3.second, 4);
}

TEST(split_range, small_overhead)
{
    auto sub_range0 = split_range({0, 5}, 0, 4);
    auto sub_range1 = split_range({0, 5}, 1, 4);
    auto sub_range2 = split_range({0, 5}, 2, 4);
    auto sub_range3 = split_range({0, 5}, 3, 4);

    EXPECT_EQ(sub_range0.first, 0);
    EXPECT_EQ(sub_range0.second, 1);

    EXPECT_EQ(sub_range1.first, 1);
    EXPECT_EQ(sub_range1.second, 3);

    EXPECT_EQ(sub_range2.first, 3);
    EXPECT_EQ(sub_range2.second, 4);

    EXPECT_EQ(sub_range3.first, 4);
    EXPECT_EQ(sub_range3.second, 5);
}

TEST(split_range, large_overhead)
{
    auto sub_range0 = split_range({0, 7}, 0, 4);
    auto sub_range1 = split_range({0, 7}, 1, 4);
    auto sub_range2 = split_range({0, 7}, 2, 4);
    auto sub_range3 = split_range({0, 7}, 3, 4);

    EXPECT_EQ(sub_range0.first, 0);
    EXPECT_EQ(sub_range0.second, 2);

    EXPECT_EQ(sub_range1.first, 2);
    EXPECT_EQ(sub_range1.second, 4);

    EXPECT_EQ(sub_range2.first, 4);
    EXPECT_EQ(sub_range2.second, 5);

    EXPECT_EQ(sub_range3.first, 5);
    EXPECT_EQ(sub_range3.second, 7);
}
}  // namespace
