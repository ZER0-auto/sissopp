// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include <fstream>
#include <random>

#include "loss_function/RMSECPU.hpp"
#include "loss_function/RMSEGPU.hpp"

template <typename DEVICE>
class ScoreModelsFixture : public ::testing::Test
{
protected:
    using Device = DEVICE;

    constexpr static int64_t NUM_SAMPLES = 100;
    constexpr static int64_t NUM_FEATURES = 8;
    constexpr static int64_t MODEL_SIZE = 3;
    const std::vector<int> TASK_SIZES = {33, 33, 34};

    void SetUp() override
    {
        std::default_random_engine engine(42);
        std::uniform_real_distribution<double> dist;
        auto rng = [&]() { return dist(engine); };

        descriptor_matrix = typename Device::Matrix("descriptor_matrix", NUM_SAMPLES, NUM_FEATURES);
        properties_vector = typename Device::Vector("properties_vector", NUM_SAMPLES);
        for (auto x = 0; x < descriptor_matrix.extent(0); ++x)
        {
            for (auto y = 0; y < descriptor_matrix.extent(1); ++y)
            {
                descriptor_matrix(x, y) = rng();
            }
            properties_vector(x) = rng();
        }

        models = typename Device::ModelBatch(
            "model_batch", MODEL_SIZE, NUM_FEATURES * NUM_FEATURES * NUM_FEATURES);
        num_models = 0;
        for (auto first = 0; first < NUM_FEATURES; ++first)
        {
            for (auto second = first + 1; second < NUM_FEATURES; ++second)
            {
                for (auto third = second + 1; third < NUM_FEATURES; ++third)
                {
                    models(0, num_models) = first;
                    models(1, num_models) = second;
                    models(2, num_models) = third;
                    ++num_models;
                }
            }
        }
    }

    void TearDown() override {}

    typename Device::Matrix descriptor_matrix;
    typename Device::Vector properties_vector;
    typename Device::ModelBatch models;
    int64_t num_models;
};

using ScoreModelsCPU = ScoreModelsFixture<RMSECPU<CPUDevice, double>>;
using ScoreModelsGPU = ScoreModelsFixture<RMSEGPU<GPUDevice, double>>;

namespace regression
{
const std::vector<double> no_fix_intercept = {
    0.2968744455356558, 0.2878518560536579, 0.2955384692126172, 0.2959776010086253,
    0.2956894447810746, 0.2963510026169813, 0.2895441861273321, 0.3004252893607828,
    0.3005018349686654, 0.3009881771202938, 0.3003390574052615, 0.2885951603182214,
    0.2889766187824115, 0.2897355776157761, 0.2894467346862571, 0.2997568820732883,
    0.3001079068938308, 0.2998598892477587, 0.3001685395396687, 0.299709158283059,
    0.3001580940543015, 0.2879130824479613, 0.2958858410131079, 0.2963969808537125,
    0.2960146611642083, 0.2964292718019065, 0.2865904924290874, 0.2872629390540306,
    0.2878194400075799, 0.2878927028126146, 0.2951062545986028, 0.2943258373650841,
    0.2955188757250257, 0.2949230354247273, 0.2957142772719838, 0.2952009638756525,
    0.2886646374451922, 0.2891044591763595, 0.2897766764833637, 0.2893622597233588,
    0.3000418988429711, 0.3003338658296203, 0.2999696377219384, 0.3004274105144624,
    0.2998790828467645, 0.3002395086360939, 0.2881821873592164, 0.2887824566639858,
    0.2886364825236563, 0.2891781991660882, 0.2888928918891508, 0.2896293617808378,
    0.2996134547201668, 0.2993910066312495, 0.2996472500737609, 0.2995163232279329};
const std::vector<double> fix_intercept = {
    0.3460041824890566, 0.3131903536666566, 0.339932002787957,  0.3500890873944335,
    0.3437741944347687, 0.3412565277322214, 0.3112699298592246, 0.3296075416690615,
    0.3430190135918369, 0.3401273758197574, 0.3361389140850586, 0.3031319350357681,
    0.3123794564860755, 0.30747168390228,   0.3087279576130374, 0.3320190196444651,
    0.3314684100206392, 0.328038701767308,  0.3411491701611014, 0.3358070772607402,
    0.3341833634421915, 0.3159338285283406, 0.3420727692661342, 0.3555964638875786,
    0.3503581208734256, 0.3492143295785894, 0.3094684447228683, 0.3174536571724146,
    0.3104645684520942, 0.3145266527091654, 0.3459102570830948, 0.3447667288808178,
    0.3430576562016373, 0.3517619609334126, 0.3489925133928538, 0.3455641679560943,
    0.3062504350834494, 0.3182099423363848, 0.3136519244351541, 0.315474269777596,
    0.3372002653497139, 0.3378076529274206, 0.3348738113349444, 0.3524422264534931,
    0.3470925901761601, 0.3465813306835424, 0.3071447425265076, 0.3048475275677348,
    0.3062887032798106, 0.3135021509884591, 0.3147862244889199, 0.3110504443101813,
    0.3389831592089723, 0.3347053296668385, 0.3364263135617229, 0.3442835488398415};
const std::vector<double> fix_intercept_tasks = {
    0.3313198800550097, 0.3068436157489616, 0.3340754408952179, 0.3354946274579571,
    0.3280074482212511, 0.3316481028218237, 0.2970950552362218, 0.3171537926114773,
    0.3265182766689346, 0.323348985421953,  0.3273050925843154, 0.2986307556938774,
    0.2918275186023856, 0.2944398896987408, 0.3031945910323388, 0.3213213950023401,
    0.3210609929328891, 0.3193594653073253, 0.3261903215887724, 0.3237907291501853,
    0.3218348257539986, 0.3068889357952388, 0.335435225222797,  0.3390398996036507,
    0.3399498460429002, 0.34197713794663,   0.3086746711121537, 0.302698823914645,
    0.3040985239228855, 0.3120553101803623, 0.3363997663640801, 0.3400842347402104,
    0.3398555728369767, 0.3369361170832806, 0.3355304357630571, 0.3382984270895198,
    0.2973895424854922, 0.3074767429638004, 0.3057519639462253, 0.3107435148252229,
    0.3257493773731492, 0.3304591934968924, 0.330073678520475,  0.3461305375794607,
    0.3404348878961479, 0.3423750577945272, 0.2922506959694038, 0.2998152751025932,
    0.303243264603388,  0.3007652590269507, 0.3026521763447077, 0.3066031134001296,
    0.3282549331846071, 0.3241054002763115, 0.3308995839011797, 0.3387398348851756};
}  // namespace regression

TEST_F(ScoreModelsCPU, NoFixIntercept)
{
    ScoreModelsCPU::Device loss(19,
                                descriptor_matrix,
                                properties_vector,
                                {NUM_SAMPLES},
                                false,
                                MODEL_SIZE);
    auto res = loss(models, num_models);

    for (auto model_idx = 0; model_idx < num_models; ++model_idx)
    {
        EXPECT_FLOAT_EQ(res(model_idx), regression::no_fix_intercept[model_idx]);
    }
}

TEST_F(ScoreModelsCPU, FixIntercept)
{
    ScoreModelsCPU::Device loss(19,
                                descriptor_matrix,
                                properties_vector,
                                {NUM_SAMPLES},
                                true,
                                MODEL_SIZE);
    auto res = loss(models, num_models);

    for (auto model_idx = 0; model_idx < num_models; ++model_idx)
    {
        EXPECT_FLOAT_EQ(res(model_idx), regression::fix_intercept[model_idx]);
    }
}

TEST_F(ScoreModelsCPU, FixInterceptTasks)
{
    ScoreModelsCPU::Device loss(19,
                                descriptor_matrix,
                                properties_vector,
                                TASK_SIZES,
                                true,
                                MODEL_SIZE);
    auto res = loss(models, num_models);

    for (auto model_idx = 0; model_idx < num_models; ++model_idx)
    {
        EXPECT_FLOAT_EQ(res(model_idx), regression::fix_intercept_tasks[model_idx]);
    }
}

TEST_F(ScoreModelsGPU, NoFixIntercept)
{
    ScoreModelsGPU::Device loss(19,
                                descriptor_matrix,
                                properties_vector,
                                {NUM_SAMPLES},
                                false,
                                MODEL_SIZE);
    auto res = loss(models, num_models);

    for (auto model_idx = 0; model_idx < num_models; ++model_idx)
    {
        EXPECT_FLOAT_EQ(res(model_idx), regression::no_fix_intercept[model_idx]);
    }
}

TEST_F(ScoreModelsGPU, FixIntercept)
{
    ScoreModelsGPU::Device loss(19,
                                descriptor_matrix,
                                properties_vector,
                                {NUM_SAMPLES},
                                true,
                                MODEL_SIZE);
    auto res = loss(models, num_models);

    for (auto model_idx = 0; model_idx < num_models; ++model_idx)
    {
        EXPECT_FLOAT_EQ(res(model_idx), regression::fix_intercept[model_idx]);
    }
}

TEST_F(ScoreModelsGPU, FixInterceptTasks)
{
    ScoreModelsGPU::Device loss(19,
                                descriptor_matrix,
                                properties_vector,
                                TASK_SIZES,
                                true,
                                MODEL_SIZE);
    auto res = loss(models, num_models);

    for (auto model_idx = 0; model_idx < num_models; ++model_idx)
    {
        EXPECT_FLOAT_EQ(res(model_idx), regression::fix_intercept_tasks[model_idx]);
    }
}
