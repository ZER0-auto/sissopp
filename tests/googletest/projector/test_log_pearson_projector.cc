// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <gtest/gtest.h>

#include <random>

#include "projector/ProjectorLogPearson.hpp"
#include "mpi_interface/MPI_Interface.hpp"

namespace
{
class ProjectorLogPearsonTests : public ::testing::Test
{
protected:
    void SetUp() override
    {
        mpi_setup::init_mpi_env();
        _task_sizes = {80};

        node_value_arrs::initialize_values_arr(_task_sizes, {0}, 2, 2, false);
        node_value_arrs::initialize_d_matrix_arr();
        node_value_arrs::resize_d_matrix_arr(2);

        std::vector<double> value_1(_task_sizes[0], 0.0);
        std::vector<double> value_2(_task_sizes[0], 0.0);

        std::vector<double> test_value_1;
        std::vector<double> test_value_2;

        std::default_random_engine generator(0);
        std::uniform_real_distribution<double> distribution_feats(0.01, 100.0);
        std::uniform_real_distribution<double> distribution_params(0.9, 1.1);

        for (int ii = 0; ii < _task_sizes[0]; ++ii)
        {
            value_1[ii] = distribution_feats(generator);
            value_2[ii] = distribution_feats(generator);
        }

        _log_feat.resize(_task_sizes[0]);

        _phi.push_back(std::make_shared<FeatureNode>(0, "A", value_1, test_value_1, Unit("m")));
        _phi.push_back(std::make_shared<FeatureNode>(1, "B", value_2, test_value_2, Unit("m")));

        _model_phi.push_back(std::make_shared<ModelNode>(_phi[0]));
        _model_phi.push_back(std::make_shared<ModelNode>(_phi[1]));

        std::copy_n(value_1.data(), _task_sizes[0], node_value_arrs::get_d_matrix_ptr(0));
        std::copy_n(value_2.data(), _task_sizes[0], node_value_arrs::get_d_matrix_ptr(1));

        double a0 = distribution_params(generator);
        double a1 = distribution_params(generator);
        double c0 = distribution_params(generator);

        _prop.resize(_task_sizes[0], 0.0);
        std::transform(value_1.begin(),
                       value_1.end(),
                       value_2.begin(),
                       _prop.begin(),
                       [=](double v0, double v1) { return c0 * std::pow(v0, a0) * std::pow(v1, a1); });

        _log_prop.resize(_task_sizes[0]);

        std::transform(_prop.begin(),
                      _prop.end(),
                      _log_prop.begin(),
                      [](double p) {return std::log(p);});
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    std::vector<node_ptr> _phi;
    std::vector<model_node_ptr> _model_phi;

    std::vector<double> _prop;

    std::vector<double> _log_feat;
    std::vector<double> _log_prop;

    std::vector<int> _task_sizes;
};

TEST_F(ProjectorLogPearsonTests, Default)
{
    ProjectorLogPearson projector(_log_prop, _task_sizes);

    EXPECT_LT(std::abs(projector.project(_phi[0]) + util_funcs::log_r2(_phi[0]->value_ptr(),
                                                                       _log_prop.data(),
                                                                       _log_feat.data(),
                                                                       _task_sizes[0])),
              1e-10);
    EXPECT_LT(std::abs(projector.project(_phi[1]) + util_funcs::log_r2(_phi[1]->value_ptr(),
                                                                       _log_prop.data(),
                                                                       _log_feat.data(),
                                                                       _task_sizes[0])),
              1e-10);
    EXPECT_EQ(projector.type(), PROJECT_TYPE::LOG_PEARSON);
}

TEST_F(ProjectorLogPearsonTests, Copy)
{
    ProjectorLogPearson projector(_log_prop, _task_sizes);

    ProjectorLogPearson projector_copy(std::make_shared<ProjectorLogPearson>(projector));

    EXPECT_LT(std::abs(projector_copy.project(_phi[0]) + util_funcs::log_r2(_phi[0]->value_ptr(),
                                                                            _log_prop.data(),
                                                                            _log_feat.data(),
                                                                            _task_sizes[0])),
              1e-10);
    EXPECT_LT(std::abs(projector_copy.project(_phi[1]) + util_funcs::log_r2(_phi[1]->value_ptr(),
                                                                            _log_prop.data(),
                                                                            _log_feat.data(),
                                                                            _task_sizes[0])),
              1e-10);

    EXPECT_EQ(projector_copy.type(), PROJECT_TYPE::LOG_PEARSON);
}
}  // namespace
