// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <gtest/gtest.h>
#include <math.h>

#include "feature_creation/node/FeatureNode.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/cos/cos.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sin/sin.hpp"
#include "feature_creation/node/value_storage/nodes_value_containers.hpp"
#include "mpi_interface/MPI_Interface.hpp"

namespace
{
class CosNodeTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
#ifdef PARAMETERIZE
        node_value_arrs::initialize_values_arr({4}, {1}, 4, 2, true);
        nlopt_wrapper::MAX_PARAM_DEPTH = 1;
#else
        node_value_arrs::initialize_values_arr({4}, {1}, 4, 2, false);
#endif

        std::vector<double> value_1 = {0.0, 2.0, 3.0, 4.0};
        std::vector<double> test_value_1 = {0.0};

        std::vector<double> value_2 = {0.0, 2.0 * M_PI, 4.0 * M_PI, 6.0 * M_PI};
        std::vector<double> test_value_2 = {5.0};

        _feat_1 = std::make_shared<FeatureNode>(0, "A", value_1, test_value_1, Unit());
        _feat_2 = std::make_shared<FeatureNode>(1, "B", value_1, test_value_1, Unit("m"));
        _feat_3 = std::make_shared<FeatureNode>(2, "C", value_2, test_value_2, Unit());

        _phi = {_feat_1, _feat_2, _feat_3};
        _phi.push_back(std::make_shared<SinNode>(_feat_1, 3, 1e-50, 1e50));
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    node_ptr _feat_1;
    node_ptr _feat_2;
    node_ptr _feat_3;

    node_ptr _node_test;

    std::vector<node_ptr> _phi;
};

TEST_F(CosNodeTest, GeneratorTest)
{
    unsigned long int feat_ind = _phi.size();

    generateCosNode(_phi, _phi[2], feat_ind, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 4) << " (Creation of CosNode led to a feature with a constant value)";

    generateCosNode(_phi, _phi[1], feat_ind, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 4) << "(CosNode created with a feature that is not unitless)";

    generateCosNode(_phi, _phi[0], feat_ind, 1e-50, 1e-40);
    EXPECT_EQ(_phi.size(), 4) << " (CosNode created with an absolute value above the upper bound)";

    generateCosNode(_phi, _phi[0], feat_ind, 1e3, 1e50);
    EXPECT_EQ(_phi.size(), 4) << " (CosNode created with an absolute value below the lower bound)";

    generateCosNode(_phi, _phi[3], feat_ind, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 4) << " (CosNode created from another a SinNode)";

    generateCosNode(_phi, _phi[0], feat_ind, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 5) << " (Failure to create a valid feature)";

    generateCosNode(_phi, _phi[4], feat_ind, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 5) << " (CosNode created from another CosNode)";
}

TEST_F(CosNodeTest, ConstructorTest)
{
    unsigned long int feat_ind = _phi.size();

    try
    {
        _node_test = std::make_shared<CosNode>(_phi[2], feat_ind, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (Creation of CosNode led to a feature with a constant value)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<CosNode>(_phi[1], feat_ind, 1e-50, 1e50);
        EXPECT_TRUE(false) << "(CosNode created with a feature that is not unitless)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<CosNode>(_phi[0], feat_ind, 1e-50, 1e-40);
        EXPECT_TRUE(false) << " (CosNode created with an absolute value above the upper bound)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<CosNode>(_phi[0], feat_ind, 1e3, 1e50);
        EXPECT_TRUE(false) << " (CosNode created with an absolute value below the lower bound)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<CosNode>(_phi[3], feat_ind, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (CosNode created from another a SinNode)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<CosNode>(_phi[0], feat_ind, 1e-50, 1e50);
        ++feat_ind;
    }
    catch (const InvalidFeatureException& e)
    {
        ASSERT_TRUE(false) << " (Failure to create a valid feature)";
    }

    try
    {
        _node_test = std::make_shared<CosNode>(_node_test, feat_ind, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (CosNode created from another CosNode)";
    }
    catch (const InvalidFeatureException& e)
    {
    }
}

TEST_F(CosNodeTest, HardCopyTestTest)
{
    _node_test = std::make_shared<CosNode>(_phi[0], 5, 1e-50, 1e50);

    node_ptr copy_test = _node_test->hard_copy();

    EXPECT_EQ(copy_test->rung(), 1);

    EXPECT_EQ(copy_test->value_ptr()[0], 1.0);
    EXPECT_EQ(copy_test->test_value_ptr()[0], 1.0);

    EXPECT_EQ(copy_test->value()[0], 1.0);
    EXPECT_EQ(copy_test->test_value()[0], 1.0);

    EXPECT_STREQ(copy_test->unit().toString().c_str(), "Unitless");

    EXPECT_STREQ(copy_test->expr().c_str(), "cos(A)");
    EXPECT_STREQ(copy_test->postfix_expr().c_str(), "0|cos");
}

TEST_F(CosNodeTest, AttributesTest)
{
    if (mpi_setup::comm->rank() == 0)
    {
        _node_test = std::make_shared<CosNode>(_phi[0], 5, 1e-50, 1e50);
    }
    mpi::broadcast(*mpi_setup::comm, _node_test, 0);

    EXPECT_EQ(_node_test->rung(), 1);

    EXPECT_EQ(_node_test->value_ptr()[0], 1.0);
    EXPECT_EQ(_node_test->test_value_ptr()[0], 1.0);

    EXPECT_EQ(_node_test->value()[0], 1.0);
    EXPECT_EQ(_node_test->test_value()[0], 1.0);

    EXPECT_STREQ(_node_test->unit().toString().c_str(), "Unitless");

    EXPECT_STREQ(_node_test->expr().c_str(), "cos(A)");
    EXPECT_STREQ(_node_test->postfix_expr().c_str(), "0|cos");

#ifdef PARAMETERIZE
    EXPECT_THROW(_node_test->param_pointer(), std::logic_error);
    EXPECT_EQ(_node_test->n_params(), 0);

    nlopt_wrapper::MAX_PARAM_DEPTH = 0;
    EXPECT_EQ(_node_test->n_params_possible(), 0);
    nlopt_wrapper::MAX_PARAM_DEPTH = 1;
    EXPECT_EQ(_node_test->n_params_possible(), 2);

    EXPECT_EQ(_node_test->parameters().size(), 0);

    std::vector<double> params = {M_PI / 2.0, 0.0};
    std::vector<double> dfdp(4, 0.0);
    std::vector<double> grad(2, 0.0);
    EXPECT_THROW(_node_test->gradient(grad.data(), dfdp.data()), std::logic_error);

    _node_test->set_parameters({}, true);
    _node_test->set_parameters(nullptr);
    _node_test->param_derivative(params.data(), dfdp.data());

    EXPECT_LT(std::abs(dfdp[0] - 0.0), 1e-10);
    EXPECT_LT(std::abs(dfdp[1] - 0.0), 1e-10);

    EXPECT_LT(std::abs(dfdp[2] - 1.0), 1e-10);
    EXPECT_LT(std::abs(dfdp[3] - 0.0), 1e-10);
#endif
}
}  // namespace
