// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <gtest/gtest.h>

#include <boost/filesystem.hpp>

#include "feature_creation/node/utils.hpp"
#include "mpi_interface/MPI_Interface.hpp"

namespace
{
class FeatCreationUtilsTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        node_value_arrs::initialize_values_arr({4}, {1}, 3, 2, false);

        std::vector<double> value_1 = {-1.0, -2.0, -3.0, -4.0};
        std::vector<double> test_value_1 = {50.0};

        std::vector<double> value_2 = {1.0, 2.0, 3.0, 4.0};
        std::vector<double> test_value_2 = {5.0};

        std::vector<double> value_3 = {1.0, -1.0, 1.0, -1.0};
        std::vector<double> test_value_3 = {1.0};

        node_ptr feat_1 = std::make_shared<FeatureNode>(0, "A", value_1, test_value_1, Unit());
        node_ptr feat_2 = std::make_shared<FeatureNode>(1, "B", value_2, test_value_2, Unit());
        node_ptr feat_3 = std::make_shared<FeatureNode>(2, "C", value_3, test_value_3, Unit());

        _phi0 = {feat_1, feat_2, feat_3};
        _feat_ind = 3;
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    std::vector<node_ptr> _phi0;
    unsigned long int _feat_ind;
};

TEST_F(FeatCreationUtilsTest, TestPostfix2Node)
{
    std::vector<int> excluded_inds;

    EXPECT_THROW(str2node::postfix2node("0|asdf", _phi0, _feat_ind, excluded_inds),
                 std::logic_error);
    EXPECT_THROW(str2node::postfix2node("1|0|sq", _phi0, _feat_ind, excluded_inds),
                 std::logic_error);
    EXPECT_THROW(str2node::postfix2node("3|sq", _phi0, _feat_ind, excluded_inds), std::logic_error);

    excluded_inds = {1};
    EXPECT_THROW(str2node::postfix2node("1|sq", _phi0, _feat_ind, excluded_inds),
                 InvalidFeatureException);
    excluded_inds.clear();

    node_ptr test = str2node::postfix2node("0|2|div|exp|1|add", _phi0, _feat_ind, excluded_inds);
    EXPECT_EQ(test->type(), NODE_TYPE::ADD);
    EXPECT_EQ(test->rung(), 3);
    EXPECT_LT(abs(test->value()[1] - (std::exp(2.0) + 2.0)), 1e-10);
    EXPECT_STREQ(test->expr().c_str(), "(exp((A / C)) + B)");
}

TEST_F(FeatCreationUtilsTest, TestPhiSelFromFile)
{
    std::vector<int> excluded_inds;
    if (mpi_setup::comm->rank() == 0)
    {
        std::ofstream out_file_stream = std::ofstream();
        out_file_stream.open("phi_sel.txt");
        out_file_stream << std::setw(14) << std::left << "# FEAT_ID"
                        << "Feature Postfix Expression (RPN)" << std::endl;
        out_file_stream << std::setw(14) << std::left << 0 << "0|2|div|exp|1|add" << std::endl;
        out_file_stream << std::setw(14) << std::left << 1 << "0|2|div|nexp|1|add" << std::endl;
        out_file_stream << std::setw(14) << std::left << 2 << "0|2|div|sq|1|add" << std::endl;
        out_file_stream << std::setw(14) << std::left << 3 << "0|2|div|cb|1|add" << std::endl;
        out_file_stream << std::setw(14) << std::left << 4 << "0|2|div|sp|1|add" << std::endl;
        out_file_stream << "#";
        for (int dd = 0; dd < 71; ++dd)
        {
            out_file_stream << "-";
        }
        out_file_stream << std::endl;
        out_file_stream << std::setw(14) << std::left << 5 << "0|2|div|exp|1|sub" << std::endl;
        out_file_stream << std::setw(14) << std::left << 6 << "0|2|div|nexp|1|sub" << std::endl;
        out_file_stream << std::setw(14) << std::left << 7 << "0|2|div|sq|1|sub" << std::endl;
        out_file_stream << std::setw(14) << std::left << 8 << "0|2|div|cb|1|sub" << std::endl;
        out_file_stream << std::setw(14) << std::left << 9 << "0|2|div|sp|1|sub" << std::endl;
        out_file_stream << "#";
        for (int dd = 0; dd < 71; ++dd)
        {
            out_file_stream << "-";
        }
        out_file_stream << std::endl;
        out_file_stream.close();
    }
    mpi_setup::comm->barrier();

    std::vector<node_ptr> phi_sel = str2node::phi_selected_from_file(
        "phi_sel.txt", _phi0, excluded_inds);

    ASSERT_EQ(phi_sel.size(), 10);
    EXPECT_EQ(phi_sel[0]->type(), NODE_TYPE::ADD);
    EXPECT_EQ(phi_sel[0]->rung(), 3);
    EXPECT_LT(abs(phi_sel[0]->value()[1] - (std::exp(2.0) + 2.0)), 1e-10);
    EXPECT_STREQ(phi_sel[0]->expr().c_str(), "(exp((A / C)) + B)");

    EXPECT_EQ(phi_sel[5]->type(), NODE_TYPE::SUB);
    EXPECT_EQ(phi_sel[5]->rung(), 3);
    EXPECT_LT(abs(phi_sel[5]->value()[1] - (std::exp(2.0) - 2.0)), 1e-10);
    EXPECT_STREQ(phi_sel[5]->expr().c_str(), "(exp((A / C)) - B)");

    mpi_setup::comm->barrier();
    if (mpi_setup::comm->rank() == 0)
    {
        boost::filesystem::remove("phi_sel.txt");
    }
}

TEST_F(FeatCreationUtilsTest, TestPhiFromFile)
{
    std::vector<int> excluded_inds;
    if (mpi_setup::comm->rank() == 0)
    {
        std::ofstream out_file_stream = std::ofstream();
        out_file_stream.open("phi.txt");
        out_file_stream << "0|2|div|exp|1|add" << std::endl;
        out_file_stream << "0|2|div|nexp|1|add" << std::endl;
        out_file_stream << "0|2|div|sq|1|add" << std::endl;
        out_file_stream << "0|2|div|cb|1|add" << std::endl;
        out_file_stream << "0|2|div|sp|1|add" << std::endl;
        out_file_stream << "0|2|div|exp|1|sub" << std::endl;
        out_file_stream << "0|2|div|nexp|1|sub" << std::endl;
        out_file_stream << "0|2|div|sq|1|sub" << std::endl;
        out_file_stream << "0|2|div|cb|1|sub" << std::endl;
        out_file_stream << "0|2|div|sp|1|sub" << std::endl;
        out_file_stream << "#";
        for (int dd = 0; dd < 71; ++dd)
        {
            out_file_stream << "-";
        }
        out_file_stream << std::endl;
        out_file_stream.close();
    }
    mpi_setup::comm->barrier();

    EXPECT_THROW(str2node::phi_from_file("not_phi_file.txt", _phi0, excluded_inds),
                 std::logic_error);
    std::vector<node_ptr> phi = str2node::phi_from_file("phi.txt", _phi0, excluded_inds);

    ASSERT_EQ(phi.size(), 10);
    EXPECT_EQ(phi[0]->type(), NODE_TYPE::ADD);
    EXPECT_EQ(phi[0]->rung(), 3);
    EXPECT_LT(abs(phi[0]->value()[1] - (std::exp(2.0) + 2.0)), 1e-10);
    EXPECT_STREQ(phi[0]->expr().c_str(), "(exp((A / C)) + B)");

    EXPECT_EQ(phi[5]->type(), NODE_TYPE::SUB);
    EXPECT_EQ(phi[5]->rung(), 3);
    EXPECT_LT(abs(phi[5]->value()[1] - (std::exp(2.0) - 2.0)), 1e-10);
    EXPECT_STREQ(phi[5]->expr().c_str(), "(exp((A / C)) - B)");

    mpi_setup::comm->barrier();
    if (mpi_setup::comm->rank() == 0)
    {
        boost::filesystem::remove("phi.txt");
    }
}

TEST_F(FeatCreationUtilsTest, TestType2Str)
{
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::FEAT).c_str(), "feature");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::MODEL_FEATURE).c_str(),
                 "model");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::ADD).c_str(), "add");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::SUB).c_str(), "sub");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::ABS_DIFF).c_str(), "abs_diff");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::MULT).c_str(), "mult");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::DIV).c_str(), "div");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::EXP).c_str(), "exp");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::NEG_EXP).c_str(), "neg_exp");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::INV).c_str(), "inv");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::SQ).c_str(), "sq");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::CB).c_str(), "cb");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::SIX_POW).c_str(), "six_pow");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::SQRT).c_str(), "sqrt");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::CBRT).c_str(), "cbrt");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::LOG).c_str(), "log");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::ABS).c_str(), "abs");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::SIN).c_str(), "sin");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::COS).c_str(), "cos");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_ADD).c_str(), "p:add");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_SUB).c_str(), "p:sub");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_ABS_DIFF).c_str(),
                 "p:abs_diff");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_MULT).c_str(), "p:mult");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_DIV).c_str(), "p:div");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_EXP).c_str(), "p:exp");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_NEG_EXP).c_str(),
                 "p:neg_exp");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_INV).c_str(), "p:inv");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_SQ).c_str(), "p:sq");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_CB).c_str(), "p:cb");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_SIX_POW).c_str(),
                 "p:six_pow");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_SQRT).c_str(), "p:sqrt");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_CBRT).c_str(), "p:cbrt");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_LOG).c_str(), "p:log");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_ABS).c_str(), "p:abs");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_SIN).c_str(), "p:sin");
    EXPECT_STREQ(node_identifier::feature_type_to_string(NODE_TYPE::PARAM_COS).c_str(), "p:cos");

    EXPECT_THROW(node_identifier::feature_type_to_string(NODE_TYPE::MAX), std::logic_error);
}
}  // namespace
