import numpy as np

def get_data(train=True):
    typ = "train" if train else "test"
    ref_data = np.genfromtxt(f"ref_models/{typ}_dim_2_model_0.dat", delimiter=",", usecols=(0,1))
    test_data = np.genfromtxt(f"models/{typ}_dim_2_model_0.dat", delimiter=",", usecols=(0,1))
    
    return ref_data, test_data


assert np.allclose(*get_data(True))
assert np.allclose(*get_data(False))

