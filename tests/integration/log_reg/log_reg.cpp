// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#include <gtest/gtest.h>

#include <Kokkos_Core.hpp>
#include <descriptor_identifier/solver/SISSOLogRegressor.hpp>
#include <feature_creation/feature_space/FeatureSpace.hpp>
#include <inputs/InputParser.hpp>

void log_reg_test()
{
    allowed_op_maps::set_node_maps();
    std::vector<std::string> filepath = str_utils::split_string_trim(__FILE__, "/");
    std::string json_fn = str_utils::join("/", filepath.data(), filepath.size() - 1) + "/sisso.json";
    InputParser inputs(json_fn);

    auto feat_space = std::make_shared<FeatureSpace>(inputs);
    node_value_arrs::initialize_d_matrix_arr();

    SISSOLogRegressor sisso(inputs, feat_space);
    sisso.fit();

    if (mpi_setup::comm->rank() == 0)
    {
        EXPECT_LT(sisso.models().back()[0].rmse(), 1e-5);

        double coef_sum = 0;
        double coef = 0;
        double* val_ptr;

        coef = sisso.models()[0][0].coefs()[0][0];
        val_ptr = sisso.models()[0][0].feats()[0]->value_ptr();
        coef_sum = std::accumulate(val_ptr, val_ptr + 95, 0.0, [coef](double sum, double val){return sum + std::pow(val, coef); });
        EXPECT_FLOAT_EQ(coef_sum, 2.58598898170897717);

        coef = sisso.models()[1][0].coefs()[0][0];
        val_ptr = sisso.models()[1][0].feats()[0]->value_ptr();
        coef_sum = std::accumulate(val_ptr, val_ptr + 95, 0.0, [coef](double sum, double val){return sum + std::pow(val, coef); });
        EXPECT_FLOAT_EQ(coef_sum, 168134.66634773055557162);

        coef = sisso.models()[1][0].coefs()[0][1];
        val_ptr = sisso.models()[1][0].feats()[1]->value_ptr();
        coef_sum = std::accumulate(val_ptr, val_ptr + 95, 0.0, [coef](double sum, double val){return sum + std::pow(val, coef); });
        EXPECT_FLOAT_EQ(coef_sum, 2.58106927105938633);

    }
}

TEST(log_reg, run)
{
    log_reg_test();
}

int main(int argc, char** argv)
{
    int result = EXIT_SUCCESS;

    ::testing::InitGoogleTest(&argc, argv);
    mpi_setup::init_mpi_env();

    {
        Kokkos::ScopeGuard kokkos(argc, argv);
        result = RUN_ALL_TESTS();
    }

    mpi_setup::finalize_mpi_env();

    return result;
}
