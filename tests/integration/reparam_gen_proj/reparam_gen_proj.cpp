// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#include <gtest/gtest.h>

#include <Kokkos_Core.hpp>
#include <descriptor_identifier/solver/SISSORegressor.hpp>
#include <feature_creation/feature_space/FeatureSpace.hpp>
#include <inputs/InputParser.hpp>

void reparam_gen_proj_test()
{
    allowed_op_maps::set_node_maps();
    allowed_op_maps::set_param_node_maps();
    std::vector<std::string> filepath = str_utils::split_string_trim(__FILE__, "/");
    std::string json_fn = str_utils::join("/", filepath.data(), filepath.size() - 1) + "/sisso.json";
    InputParser inputs(json_fn);

    auto feat_space = std::make_shared<FeatureSpace>(inputs);
    node_value_arrs::initialize_d_matrix_arr();

    SISSORegressor sisso(inputs, feat_space);
    sisso.fit();

    if (mpi_setup::comm->rank() == 0)
    {
        EXPECT_LT(sisso.models().back()[0].rmse(), 1e-3);
    }
}

TEST(reparam_gen_proj, run)
{
    reparam_gen_proj_test();
}

int main(int argc, char** argv)
{
    int result = EXIT_SUCCESS;

    ::testing::InitGoogleTest(&argc, argv);
    mpi_setup::init_mpi_env();

    {
        Kokkos::ScopeGuard kokkos(argc, argv);
        result = RUN_ALL_TESTS();
    }

    mpi_setup::finalize_mpi_env();

    return result;
}
