// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#include <gtest/gtest.h>

#include <Kokkos_Core.hpp>
#include <descriptor_identifier/solver/SISSORegressor.hpp>
#include <feature_creation/feature_space/FeatureSpace.hpp>
#include <inputs/InputParser.hpp>

void default_test()
{
    allowed_op_maps::set_node_maps();
    std::vector<std::string> filepath = str_utils::split_string_trim(__FILE__, "/");
    std::string json_fn = str_utils::join("/", filepath.data(), filepath.size() - 1) + "/sisso.json";
    InputParser inputs(json_fn);

    auto feat_space = std::make_shared<FeatureSpace>(inputs);
    node_value_arrs::initialize_d_matrix_arr();

    SISSORegressor sisso(inputs, feat_space);
    sisso.fit();

    if (mpi_setup::comm->rank() == 0)
    {
        EXPECT_LT(sisso.models().back()[0].rmse(), 1e-5);

        double coef_sum = 0;
        std::vector<double> feat_sum(1);

        feat_sum[0] = std::accumulate(sisso.models()[0][0].feats()[0]->value_ptr(), sisso.models()[0][0].feats()[0]->value_ptr() + 95, 0.0);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[0][0];
        EXPECT_FLOAT_EQ(coef_sum, 8.93825470532816499e+07);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[1][0];
        EXPECT_FLOAT_EQ(coef_sum, -1.54670576562153518e+08);

        feat_sum.resize(2);

        feat_sum[0] = std::accumulate(sisso.models()[1][0].feats()[0]->value_ptr(), sisso.models()[1][0].feats()[0]->value_ptr() + 95, 0.0);
        feat_sum[1] = std::accumulate(sisso.models()[1][0].feats()[1]->value_ptr(), sisso.models()[1][0].feats()[1]->value_ptr() + 95, 0.0);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[0][0];
        EXPECT_FLOAT_EQ(coef_sum, -2.32274099083877028e-02);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[0][1];
        EXPECT_FLOAT_EQ(coef_sum, 8.93825471104539633e+07);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[1][0];
        EXPECT_FLOAT_EQ(coef_sum, 3.19159177009055328e-01);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[1][1];
        EXPECT_FLOAT_EQ(coef_sum, -1.54670576840085834e+08);

    }
}

TEST(default_sisso, run)
{
    default_test();
}

int main(int argc, char** argv)
{
    int result = EXIT_SUCCESS;

    ::testing::InitGoogleTest(&argc, argv);
    mpi_setup::init_mpi_env();

    {
        Kokkos::ScopeGuard kokkos(argc, argv);
        result = RUN_ALL_TESTS();
    }

    mpi_setup::finalize_mpi_env();

    return result;
}
