// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#include <gtest/gtest.h>

#include <Kokkos_Core.hpp>
#include <descriptor_identifier/solver/SISSOClassifier.hpp>
#include <feature_creation/feature_space/FeatureSpace.hpp>
#include <inputs/InputParser.hpp>

void classification_test()
{
    allowed_op_maps::set_node_maps();
    std::vector<std::string> filepath = str_utils::split_string_trim(__FILE__, "/");
    std::string json_fn = str_utils::join("/", filepath.data(), filepath.size() - 1) + "/sisso.json";
    InputParser inputs(json_fn);

    auto feat_space = std::make_shared<FeatureSpace>(inputs);
    node_value_arrs::initialize_d_matrix_arr();

    SISSOClassifier sisso(inputs, feat_space);
    sisso.fit();

    if (mpi_setup::comm->rank() == 0)
    {
        EXPECT_FLOAT_EQ(sisso.models()[1][0].percent_train_error(), 0.0);

        double coef_sum = 0;
        std::vector<double> feat_sum(1);

        feat_sum[0] = std::accumulate(sisso.models()[0][0].feats()[0]->value_ptr(), sisso.models()[0][0].feats()[0]->value_ptr() + 80, 0.0);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[0][0];
        EXPECT_FLOAT_EQ(coef_sum, -8.39729503864873834e+01);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[1][0];
        EXPECT_FLOAT_EQ(coef_sum, 7.43408541728945212e-04);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[2][0];
        EXPECT_FLOAT_EQ(coef_sum, -9.73161837444518909e+01);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[3][0];
        EXPECT_FLOAT_EQ(coef_sum, 1.16916811919392273e+02);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[4][0];
        EXPECT_FLOAT_EQ(coef_sum, 1.41283213771036955e+01);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[5][0];
        EXPECT_FLOAT_EQ(coef_sum, -1.09110358761459935e+02);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[6][0];
        EXPECT_FLOAT_EQ(coef_sum, 6.96533406874194441e+01);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[7][0];
        EXPECT_FLOAT_EQ(coef_sum, -4.30543455735077529e+01);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[8][0];
        EXPECT_FLOAT_EQ(coef_sum, -1.19103915021333535e+02);

        feat_sum.resize(2);

        feat_sum[0] = std::accumulate(sisso.models()[1][0].feats()[0]->value_ptr(), sisso.models()[1][0].feats()[0]->value_ptr() + 80, 0.0);
        feat_sum[1] = std::accumulate(sisso.models()[1][0].feats()[1]->value_ptr(), sisso.models()[1][0].feats()[1]->value_ptr() + 80, 0.0);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[0][0];
        EXPECT_FLOAT_EQ(coef_sum, -8.35764523195718425e+01);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[0][1];
        EXPECT_FLOAT_EQ(coef_sum, -1.31524726454364260e+00);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[1][0];
        EXPECT_FLOAT_EQ(coef_sum, -1.10784687124948071e+01);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[1][1];
        EXPECT_FLOAT_EQ(coef_sum, 2.12592189309846660e+01);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[2][0];
        EXPECT_FLOAT_EQ(coef_sum, -2.47065037476154217e+01);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[2][1];
        EXPECT_FLOAT_EQ(coef_sum, 1.48562070932243735e+01);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[3][0];
        EXPECT_FLOAT_EQ(coef_sum, 1.65717669002687664e+01);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[3][1];
        EXPECT_FLOAT_EQ(coef_sum, 1.60647370233281173e+01);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[4][0];
        EXPECT_FLOAT_EQ(coef_sum, 1.70745222837566257e+00);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[4][1];
        EXPECT_FLOAT_EQ(coef_sum, 2.00277666539520212e+01);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[5][0];
        EXPECT_FLOAT_EQ(coef_sum, -1.14720520144187390e+02);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[5][1];
        EXPECT_FLOAT_EQ(coef_sum, -3.96834983478395609e+00);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[6][0];
        EXPECT_FLOAT_EQ(coef_sum, 1.63605850656474772e+01);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[6][1];
        EXPECT_FLOAT_EQ(coef_sum, 1.16513323199600372e+01);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[7][0];
        EXPECT_FLOAT_EQ(coef_sum, 3.51743737952060398e+00);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[7][1];
        EXPECT_FLOAT_EQ(coef_sum, 1.22150366931407053e+01);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[8][0];
        EXPECT_FLOAT_EQ(coef_sum, -8.07683283742011326e+01);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[8][1];
        EXPECT_FLOAT_EQ(coef_sum, 2.35066220459856297e+01);

    }
}

TEST(classification, run)
{
    classification_test();
}

int main(int argc, char** argv)
{
    int result = EXIT_SUCCESS;

    ::testing::InitGoogleTest(&argc, argv);
    mpi_setup::init_mpi_env();

    {
        Kokkos::ScopeGuard kokkos(argc, argv);
        result = RUN_ALL_TESTS();
    }

    mpi_setup::finalize_mpi_env();

    return result;
}
