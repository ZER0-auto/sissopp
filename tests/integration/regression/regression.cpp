// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#include <gtest/gtest.h>

#include <Kokkos_Core.hpp>
#include "utils/KokkosDefines.hpp"
#include <descriptor_identifier/solver/SISSORegressor.hpp>
#include <feature_creation/feature_space/FeatureSpace.hpp>
#include <inputs/InputParser.hpp>

void regression_test(const std::string& config_file)
{
    allowed_op_maps::set_node_maps();
    std::vector<std::string> filepath = str_utils::split_string_trim(__FILE__, "/");
    std::string json_fn = str_utils::join("/", filepath.data(), filepath.size() - 1) + "/" + config_file;
    InputParser inputs(json_fn);

    auto feat_space = std::make_shared<FeatureSpace>(inputs);
    node_value_arrs::initialize_d_matrix_arr();

    SISSORegressor sisso(inputs, feat_space);
    sisso.fit();

    if (mpi_setup::comm->rank() == 0)
    {
        EXPECT_FLOAT_EQ(sisso.models()[0][0].rmse(), 0.46386456);
        EXPECT_FLOAT_EQ(sisso.models()[1][0].rmse(), 0.34579778);

        double coef_sum = 0;
        std::vector<double> feat_sum(1);

        feat_sum[0] = std::accumulate(sisso.models()[0][0].feats()[0]->value_ptr(), sisso.models()[0][0].feats()[0]->value_ptr() + 100, 0.0);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[0][0];
        EXPECT_FLOAT_EQ(coef_sum, 1.46471842163576099e+02);

        feat_sum.resize(2);

        feat_sum[0] = std::accumulate(sisso.models()[1][0].feats()[0]->value_ptr(), sisso.models()[1][0].feats()[0]->value_ptr() + 100, 0.0);
        feat_sum[1] = std::accumulate(sisso.models()[1][0].feats()[1]->value_ptr(), sisso.models()[1][0].feats()[1]->value_ptr() + 100, 0.0);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[0][0];
        EXPECT_FLOAT_EQ(coef_sum, -7.08804545442836087e+01);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[0][1];
        EXPECT_FLOAT_EQ(coef_sum, 7.10296491879078928e+01);

    }
}

TEST(regression, cpu)
{
    regression_test("regression.json");
}

#ifdef SISSO_ENABLE_GPU
TEST(regression, gpu)
{
    regression_test("regression_gpu.json");
}
#endif

int main(int argc, char** argv)
{
    int result = EXIT_SUCCESS;

    ::testing::InitGoogleTest(&argc, argv);
    mpi_setup::init_mpi_env();

    {
        Kokkos::ScopeGuard kokkos(argc, argv);
        result = RUN_ALL_TESTS();
    }

    mpi_setup::finalize_mpi_env();

    return result;
}
