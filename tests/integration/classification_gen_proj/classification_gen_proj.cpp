// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#include <gtest/gtest.h>

#include <Kokkos_Core.hpp>
#include <descriptor_identifier/solver/SISSOClassifier.hpp>
#include <feature_creation/feature_space/FeatureSpace.hpp>
#include <inputs/InputParser.hpp>

void classification_gen_proj_test()
{
    allowed_op_maps::set_node_maps();
    std::vector<std::string> filepath = str_utils::split_string_trim(__FILE__, "/");
    std::string json_fn = str_utils::join("/", filepath.data(), filepath.size() - 1) + "/sisso.json";
    InputParser inputs(json_fn);

    auto feat_space = std::make_shared<FeatureSpace>(inputs);
    node_value_arrs::initialize_d_matrix_arr();

    SISSOClassifier sisso(inputs, feat_space);
    sisso.fit();

    if (mpi_setup::comm->rank() == 0)
    {
        EXPECT_FLOAT_EQ(sisso.models()[1][0].percent_train_error(), 0.0);

        double coef_sum = 0;
        std::vector<double> feat_sum(1);

        feat_sum[0] = std::accumulate(sisso.models()[0][0].feats()[0]->value_ptr(), sisso.models()[0][0].feats()[0]->value_ptr() + 100, 0.0);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[0][0];
        EXPECT_FLOAT_EQ(coef_sum, -9.35054868176788290e+01);

        // coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[1][0];
        // EXPECT_FLOAT_EQ(coef_sum, 7.30717676695470902e-04);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[2][0];
        EXPECT_FLOAT_EQ(coef_sum, -1.08363432443216041e+02);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[3][0];
        EXPECT_FLOAT_EQ(coef_sum, 1.30189106913325304e+02);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[4][0];
        EXPECT_FLOAT_EQ(coef_sum, -1.66311954147994250e+01);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[5][0];
        EXPECT_FLOAT_EQ(coef_sum, -1.21496471969664583e+02);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[6][0];
        EXPECT_FLOAT_EQ(coef_sum, 8.60049363573037482e+01);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[7][0];
        EXPECT_FLOAT_EQ(coef_sum, -2.56496693073776747e+01);

        coef_sum = feat_sum[0] * sisso.models()[0][0].coefs()[8][0];
        EXPECT_FLOAT_EQ(coef_sum, -1.25095793198179720e+02);

        feat_sum.resize(2);

        feat_sum[0] = std::accumulate(sisso.models()[1][0].feats()[0]->value_ptr(), sisso.models()[1][0].feats()[0]->value_ptr() + 100, 0.0);
        feat_sum[1] = std::accumulate(sisso.models()[1][0].feats()[1]->value_ptr(), sisso.models()[1][0].feats()[1]->value_ptr() + 100, 0.0);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[0][0];
        EXPECT_FLOAT_EQ(coef_sum, -9.30639786344059274e+01);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[0][1];
        EXPECT_FLOAT_EQ(coef_sum, 4.75568016299808383e-01);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[1][0];
        EXPECT_FLOAT_EQ(coef_sum, -1.23360868635496139e+01);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[1][1];
        EXPECT_FLOAT_EQ(coef_sum, -7.68692309624357240e+00);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[2][0];
        EXPECT_FLOAT_EQ(coef_sum, -2.75111646053981502e+01);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[2][1];
        EXPECT_FLOAT_EQ(coef_sum, -5.37171764391791839e+00);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[3][0];
        EXPECT_FLOAT_EQ(coef_sum, 1.84529794928829283e+01);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[3][1];
        EXPECT_FLOAT_EQ(coef_sum, -5.80869873256348956e+00);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[4][0];
        EXPECT_FLOAT_EQ(coef_sum, 1.90128072310637952e+00);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[4][1];
        EXPECT_FLOAT_EQ(coef_sum, -7.24165372952909081e+00);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[5][0];
        EXPECT_FLOAT_EQ(coef_sum, -1.27747558307967125e+02);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[5][1];
        EXPECT_FLOAT_EQ(coef_sum, 1.43443874342606148e+00);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[6][0];
        EXPECT_FLOAT_EQ(coef_sum, 1.44068023943373440e+01);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[6][1];
        EXPECT_FLOAT_EQ(coef_sum, -4.52300677029126597e+00);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[7][0];
        EXPECT_FLOAT_EQ(coef_sum, 3.91655642854970987e+00);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[7][1];
        EXPECT_FLOAT_EQ(coef_sum, -4.41676919155879322e+00);

        coef_sum = feat_sum[0] * sisso.models()[1][0].coefs()[8][0];
        EXPECT_FLOAT_EQ(coef_sum, -8.38845802769984630e+01);

        coef_sum = feat_sum[1] * sisso.models()[1][0].coefs()[8][1];
        EXPECT_FLOAT_EQ(coef_sum, -9.70453135866783789e+00);
    }
}

TEST(classification_gen_proj, run)
{
    classification_gen_proj_test();
}

int main(int argc, char** argv)
{
    int result = EXIT_SUCCESS;

    ::testing::InitGoogleTest(&argc, argv);
    mpi_setup::init_mpi_env();

    {
        Kokkos::ScopeGuard kokkos(argc, argv);
        result = RUN_ALL_TESTS();
    }

    mpi_setup::finalize_mpi_env();

    return result;
}
