from sissopp import ModelRegressor
from pathlib import Path

import numpy as np

model = ModelRegressor(
    str("models/train_dim_2_model_0.dat"), str("models/test_dim_2_model_0.dat")
)
assert model.rmse < 1e-4
assert model.test_rmse < 1e-4
