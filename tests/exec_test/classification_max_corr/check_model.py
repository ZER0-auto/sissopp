from sissopp import ModelClassifier
from pathlib import Path

import numpy as np

model = ModelClassifier(
    str("models/train_dim_2_model_0.dat"), str("models/test_dim_2_model_0.dat")
)
assert model.percent_error < 1e-7
assert model.percent_test_error < 1e-7
