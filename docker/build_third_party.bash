#! /bin/bash

export BASE_DIR=`dirname "${BASH_SOURCE[0]}"`
export BASE_DIR=`pwd`

export THIRD_PARTY_DIREC=$BASE_DIR/third_party/
if [ ! -d $THIRD_PARTY_DIREC ]; then
    mkdir $THIRD_PARTY_DIREC
fi
cd $THIRD_PARTY_DIREC

# This is taken from a configure script
# Avoid depending upon Character Ranges.
as_cr_letters='abcdefghijklmnopqrstuvwxyz'
as_cr_LETTERS='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
as_cr_Letters=$as_cr_letters$as_cr_LETTERS
as_cr_digits='0123456789'
as_cr_alnum=$as_cr_Letters$as_cr_digits


for opt
do
    unset optarg
    case $opt in
    *=?*) optarg=`expr "X$opt" : '[^=]*=\(.*\)'` ;;
    *=)   optarg= ;;
    -j*)  N_PROCS=`expr substr "$opt" 3 100` ;;
    *)    optarg=yes ;;
    esac
    envvar=`expr "x$opt" : 'x\([^=]*\)='`
    # Reject names that are not valid shell variable names.
    if [ ! -z $envvar ]; then
        eval $envvar=\$optarg
        export $envvar
    fi
done

if [ -z $N_PROCS ]; then
    N_PROCS=1
fi

echo "Building third party libraries with $N_PROCS processors"
# End of portion taken from a configure script

if [ -z $CC ]; then
    echo "CC is not set, defaulting to gcc."
    export CC=gcc
fi

if [ -z $CXX ]; then
    echo "CXX is not set, defaulting to g++."
    export CXX=g++
fi

export uname_out="$(uname -s)"
case "${uname_out}" in
    Linux*)     export machine=linux;;
    Darwin*)    export machine=darwin;;
    *)          export machine="UNKNOWN:${unameOut}";;
esac

case $CXX in
    "clang++")
        export toolset="clang";
        export boost_toolset="clang";
        export version=`clang++ --version >&1 | head -n 1 | awk '{print $3}'`
        export shared=""
    ;;
    "g++")
        export toolset="gcc";
        export boost_toolset="gcc";
        export version=`g++ --version >&1 | head -n 1 | awk '{print $NF}'`
        export shared="--shared"
    ;;
    "icpc")
        export toolset="intel-${machine}"
        export boost_toolset="intel-linux"
        export version=`icpc -dumpversion`
        IFS='.'
        read -ra ver_arr <<< $version
        unset IFS
        if [ ${#ver_arr[@]} == 1 ]; then
            export version="$version.0.0"
        elif [ ${#ver_arr[@]} == 2 ]; then
            export version="$version.0"
        fi
        export build_date=`icpc --version >&1 | head -n 1 | awk '{print $NF}'`
        export version="$version.$build_date"
        export shared="--shared"
    ;;
    "icpx")
        export toolset="intel-llvm-${machine}"
        export boost_toolset="intel-${machine}"
        export version=`icpx --version >&1 | head -n 1 | awk '{print $5}'`
        if [[ $version == "Pro" ]]; then
            export version=`icpx --version >&1 | head -n 1 | awk '{print $6}'`
        fi
        IFS='.'
        read -ra ver_arr <<< $version
        unset IFS
        if [ ${#ver_arr[@]} == 1 ]; then
            export version="$version.0.0"
        elif [ ${#ver_arr[@]} == 2 ]; then
            export version="$version.0"
        fi
        export shared=""
    ;;
    "")         ;;
    *)          echo "invalid compiler: $CXX" 1>&2 ;;
esac

echo "Building third party libraries with $CXX $version (toolset=$boost_toolset)"

# Build Boost
if [ ! -d boost/ ]; then
    mkdir boost/
fi
cd boost/
if [ ! -f boost_1_79_0.tar.bz2 ]; then
    wget -O boost_1_79_0.tar.bz2 https://boostorg.jfrog.io/artifactory/main/release/1.79.0/source/boost_1_79_0.tar.bz2
fi

if [ ! -d boost_1_79_0 ]; then
    export BOOST_SHA256=`sha256sum boost_1_79_0.tar.bz2 | awk '{print($1)}'`
    if [ $BOOST_SHA256 == "475d589d51a7f8b3ba2ba4eda022b170e562ca3b760ee922c146b6c65856ef39" ];  then
        tar xjf boost_1_79_0.tar.bz2
    else
        echo "Boost tarball file has the wrong hash, exiting script."
        exit 1
    fi
fi
cd boost_1_79_0/
if [[ $CXX == "icpx"  ]];then
    sed -i 's/rm -f "$(<)" && LD_LIBRARY_PATH="$(RUN_PATH)" "$(CONFIG_COMMAND)" -x c++-header $(OPTIONS) $(USER_OPTIONS) -D$(DEFINES) -I"$(INCLUDES)" -c -pch-create "$(<)" "$(>)"/rm -f "$(<)" && LD_LIBRARY_PATH="$(RUN_PATH)" "$(CONFIG_COMMAND)" -x c++-header $(OPTIONS) $(USER_OPTIONS) -D$(DEFINES) -I"$(INCLUDES)" -c -Xclang -emit-pch -o "$(<)" "$(>)"/g' tools/build/src/tools/intel-linux.jam

fi
export BOOST_INSTALL_DIR=$THIRD_PARTY_DIREC/boost/1.79.0/$toolset/$version/
export BOOST_LIBRARY_DIR=$THIRD_PARTY_DIREC/boost/1.79.0/$toolset/$version/lib/

export uname_out="$(uname -s)"
case "${uname_out}" in
    Linux*)     export machine=linux;;
    Darwin*)    export machine=darwin;;
    *)          export machine="UNKNOWN:${unameOut}";;
esac


if [[ ${toolset} == "intel-llvm-${machine}" ]]; then
    export boost_toolset_config="intel-${machine}"
    export mpi_compiler="mpiicpc"
    sed -i 's/rm -f "$(<)" && LD_LIBRARY_PATH="$(RUN_PATH)" "$(CONFIG_COMMAND)" -x c++-header $(OPTIONS) $(USER_OPTIONS) -D$(DEFINES) -I"$(INCLUDES)" -c -pch-create "$(<)" "$(>)"/rm -f "$(<)" && LD_LIBRARY_PATH="$(RUN_PATH)" "$(CONFIG_COMMAND)" -x c++-header $(OPTIONS) $(USER_OPTIONS) -D$(DEFINES) -I"$(INCLUDES)" -c -Xclang -emit-pch -o "$(<)" "$(>)"/g' tools/build/src/tools/intel-linux.jam
elif [[ ${boost_toolset} == "intel-${machine}" ]]; then
    export boost_toolset_config="intel-${machine}"
    export mpi_compiler="mpiicpc"
else
    export boost_toolset_config=${boost_toolset}
    export mpi_compiler="mpicxx"
fi


./bootstrap.sh --with-toolset=${boost_toolset_config} --with-libraries=mpi,serialization,system,filesystem --prefix=${BOOST_INSTALL_DIR} --libdir=${BOOST_LIBRARY_DIR}
echo "using mpi : ${mpi_compiler} ;" >> project-config.jam

if [[ ${boost_toolset} == "intel-${machine}" ]]; then
    export boost_version=`icpc -dumpversion`
    sed -i "s/using intel-linux ;/using intel-linux : $boost_version : icpc ;/g" project-config.jam
fi

# Build shared libraries
./b2 -a -j$N_PROCS toolset=$boost_toolset
./b2 -j$N_PROCS install toolset=$boost_toolset
cd ../
cd ../
pwd

# Build CoinUtils
if [ ! -d  coin-or/ ]; then
    mkdir coin-or/
fi
cd coin-or/
export COIN_OR_INSTALL_DIR=`pwd`/$toolset/$version/

if [ ! -d CoinUtils/ ]; then
    git clone https://github.com/coin-or/CoinUtils.git
fi
cd CoinUtils/
git checkout releases/2.11.4
if [ ! -d  build_${toolset}_${version}/ ]; then
    mkdir build_${toolset}_${version}/
fi
cd build_${toolset}_${version}/;
../configure -C --prefix=$COIN_OR_INSTALL_DIR --libdir=$COIN_OR_INSTALL_DIR/lib/ ADD_CXXFLAGS="-std=c++14 -fPIC $shared" --enable-shared --enable-static;
make -j$N_PROCS
make install
cd ../;
cd ../;
pwd

# Build Coin-Clp
if [ ! -d Clp/ ]; then
    git clone https://github.com/coin-or/Clp.git
fi
cd Clp/;
git checkout releases/1.17.6

if [ ! -d  build_${toolset}_${version}/ ]; then
    mkdir build_${toolset}_${version}/
fi
cd build_${toolset}_${version}/;
../configure -C --prefix=$COIN_OR_INSTALL_DIR --libdir=$COIN_OR_INSTALL_DIR/lib/ --with-coinutils-incdir=$COIN_OR_INSTALL_DIR/include/coin/ --with-coinutils-lib=$COIN_OR_INSTALL_DIR/lib/libCoinUtils.so ADD_CXXFLAGS="-std=c++14 -fPIC $shared" --enable-shared --enable-static;
make -j$N_PROCS
make install
cd ../;
cd ../;
cd ../

# Build NLOpt
if [ ! -d  nlopt/ ]; then
    mkdir nlopt/
fi
cd nlopt/
export NLOPT_INSTALL_DIR=`pwd`/$toolset/$version/

if [ ! -d nlopt/ ]; then
    git clone https://github.com/stevengj/nlopt.git
fi
cd nlopt/
git checkout v2.6.2
if [ ! -d  build_${toolset}_${version}/ ]; then
    mkdir build_${toolset}_${version}/
fi
cd build_${toolset}_${version}/
cmake -DCMAKE_INSTALL_PREFIX=${NLOPT_INSTALL_DIR} -DBUILD_SHARED_LIBS=ON ../
make -j$N_PROCS
make install
cd ../
cd ../
cd ../

# Build gtest
if [ ! -d  gtest/ ]; then
    mkdir gtest/
fi
cd gtest/
export GTEST_INSTALL_DIR=`pwd`/$toolset/$version/

if [ ! -d googletest/ ]; then
    git clone https://github.com/google/googletest.git
fi
cd googletest
git checkout release-1.11.0
if [ ! -d  build_${toolset}_${version}/ ]; then
    mkdir build_${toolset}_${version}/
fi
cd build_${toolset}_${version}/
cmake -DCMAKE_INSTALL_PREFIX=${GTEST_INSTALL_DIR} -DBUILD_SHARED_LIBS=ON ../
make -j$N_PROCS
make install
cd ../
cd ../
cd ../

# Build FMT
if [ ! -d  fmt ]; then
    mkdir fmt
fi
cd fmt
export FMT_INSTALL_DIR=`pwd`/$toolset/$version/

if [ ! -d fmt/ ]; then
    git clone https://github.com/fmtlib/fmt.git
fi
cd fmt/
git checkout 7.1.2
if [ ! -d  build_${toolset}_${version}/ ]; then
    mkdir build_${toolset}_${version}/
fi
cd build_${toolset}_${version}/
cmake -DCMAKE_INSTALL_PREFIX=${FMT_INSTALL_DIR} -DBUILD_SHARED_LIBS=ON -DFMT_DOC=OFF -DFMT_TEST=OFF -DFMT_OS=OFF ../
make -j$N_PROCS
make install
cd ../
cd ../
cd ../

# Build Caliper
if [ ! -d Caliper ]; then
    mkdir Caliper/
fi

cd Caliper/
export CALIPER_INSTALL_DIR=`pwd`/$toolset/$version/

if [ ! -d Caliper ]; then
    git clone https://github.com/LLNL/Caliper.git
fi
cd Caliper/

if [ ! -d build_${toolset}_${version}/ ]; then
    mkdir build_${toolset}_${version}/
fi
cd build_${toolset}_${version}/
if ! command -v vtune &> /dev/null; then
    cmake -DCMAKE_INSTALL_PREFIX=${CALIPER_INSTALL_DIR} -DBUILD_SHARED_LIBS=ON -DWITH_MPI=ON -DWITH_OMPT=OFF -DWITH_VTUNE=OFF ../
else
    cmake -DCMAKE_INSTALL_PREFIX=${CALIPER_INSTALL_DIR} -DBUILD_SHARED_LIBS=ON -DWITH_MPI=ON -DWITH_OMPT=ON -DWITH_VTUNE=ON ../
fi
make -j$N_PROCS
make install
cd ../
cd ../
cd ../

cd ../
