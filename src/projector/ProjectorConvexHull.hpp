// Copyright 2023 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file loss_function/Projector.hpp
 *  @brief Defines a base class used to calculate the projection score and l0-regularization objective function
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef PROJECTOR_CONVEX_HULL
#define PROJECTOR_CONVEX_HULL

#include "projector/Projector.hpp"

// DocString: cls_projector_pearson
/**
 * @brief The projection function used during SIS
 *
 */
class ProjectorConvexHull: public Projector
{
    std::vector<ConvexHull1D> _convex_hull; //!< Vector to the convex hull objects for projection

public:

    /**
     * @brief Constructor
     *
     * @param prop The value of the property to evaluate the loss function against for the training set
     * @param task_sizes_train Number of training samples per task
     */
    ProjectorConvexHull(std::vector<double> prop, std::vector<int> task_sizes_train);

    /**
     * @brief Copy constructor
     *
     * @param o Pointer to the projector to be copied
     */
    ProjectorConvexHull(std::shared_ptr<Projector> o);

    /**
     * @brief Calculate the projection score of a feature
     *
     * @param feat Feature to calculate the projection score of
     * @return The projection score for the feature
     */
    double project(const node_ptr& feat);

    // DocString: projector_type
    /**
     * @brief The type of the projector
     */
    inline PROJECT_TYPE type() const {return PROJECT_TYPE::CONVEX_HULL;}

    void create_convex_hull();
};

#endif
