// Copyright 2023 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file utils/project.cpp
 *  @brief Implements a set of functions to project features onto a second vector
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "projector/utils.hpp"

std::shared_ptr<Projector> projector_util::copy(std::shared_ptr<Projector> proj)
{
    if(proj->type() == PROJECT_TYPE::PEARSON)
    {
        return std::make_shared<ProjectorPearson>(proj);
    }
    else if(proj->type() == PROJECT_TYPE::LOG_PEARSON)
    {
        return std::make_shared<ProjectorLogPearson>(proj);
    }
    else if(proj->type() == PROJECT_TYPE::CONVEX_HULL)
    {
        return std::make_shared<ProjectorConvexHull>(proj);
    }
    throw std::logic_error("Invalid projection pointer passed to copy.");
    return nullptr;
}

std::shared_ptr<Projector> projector_util::get_projector(const std::string project_type,
                                                         const std::vector<double> prop,
                                                         const std::vector<int> task_sizes)
{
    if ((project_type == "regression") || (project_type == "regression_gpu"))
    {
        return std::make_shared<ProjectorPearson>(prop, task_sizes);
    }
    else if (project_type == "log_regression")
    {
        return std::make_shared<ProjectorLogPearson>(prop, task_sizes);
    }
    else if (project_type == "classification")
    {
        return std::make_shared<ProjectorConvexHull>(prop, task_sizes);
    }
    throw std::logic_error("Invalid projection type passed to get_projector.");
    return nullptr;
}

std::shared_ptr<Projector> projector_util::get_projector(const std::string project_type,
                                                         const std::vector<std::shared_ptr<Model>>& models)
{
    std::vector<int> task_sizes = models[0]->task_sizes_train();
    int n_samp = std::accumulate(task_sizes.begin(), task_sizes.end(), 0);

    std::vector<double> prop(n_samp * models.size(), 0.0);
    for (int mm = 0; mm < models.size(); ++mm)
    {
        models[mm]->copy_error(&prop[mm * n_samp]);
    }

    return get_projector(project_type, prop, task_sizes);
}

void project_funcs::project(std::shared_ptr<Projector> projector,
                            const std::vector<node_ptr>& feats,
                            double* scores)
{
    std::fill_n(scores, feats.size(), std::numeric_limits<double>::max());
#pragma omp parallel
    {
        std::shared_ptr<Projector> projector_copy;
#pragma omp critical
        {
            projector_copy = projector_util::copy(projector);
        }
#pragma omp barrier
        int start = (omp_get_thread_num() * (feats.size() / omp_get_num_threads()) +
                     std::min(omp_get_thread_num(),
                              static_cast<int>(feats.size()) % omp_get_num_threads()));

        int end = ((omp_get_thread_num() + 1) * (feats.size() / omp_get_num_threads()) +
                   std::min(omp_get_thread_num() + 1,
                            static_cast<int>(feats.size()) % omp_get_num_threads()));

        std::transform(feats.begin() + start,
                       feats.begin() + end,
                       scores + start,
                       [&projector_copy](node_ptr feat) { return projector_copy->project(feat); });

#pragma omp barrier
    }
}

void project_funcs::project_no_omp(std::shared_ptr<Projector> projector,
                                   const std::vector<node_ptr>& feats,
                                   double* scores)
{
    std::fill_n(scores, feats.size(), std::numeric_limits<double>::max());
    std::transform(feats.begin(),
                   feats.end(),
                   scores,
                   [&projector](node_ptr feat) { return projector->project(feat); });
}
