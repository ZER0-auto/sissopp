// Copyright 2023 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file projector/Projector.cpp
 *  @brief Implements a base class used to calculate the projection score for a feature
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "projector/Projector.hpp"


Projector::Projector(std::vector<double> prop,
                     std::vector<int> task_sizes)
    : _prop(prop),
      _scores(prop.size() / std::accumulate(task_sizes.begin(), task_sizes.end(), 0), 0.0),
      _task_sizes(task_sizes),
      _n_samp(std::accumulate(task_sizes.begin(), task_sizes.end(), 0)),
      _n_task(task_sizes.size()),
      _n_project_prop(prop.size() / std::accumulate(task_sizes.begin(), task_sizes.end(), 0))
{
    if (_prop.size() != _n_project_prop * _n_samp)
    {
        throw std::logic_error("The size of the property vector is not an integer multiple of the number of samples.");
    }
}


Projector::Projector(std::shared_ptr<Projector> o)
    : _prop(o->prop()),
      _scores(o->scores()),
      _task_sizes(o->task_sizes()),
      _n_samp(o->n_samp()),
      _n_task(o->n_task()),
      _n_project_prop(o->n_project_prop())
{}
