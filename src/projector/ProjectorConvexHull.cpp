#include "projector/ProjectorConvexHull.hpp"

ProjectorConvexHull::ProjectorConvexHull(std::vector<double> prop, std::vector<int> task_sizes_train)
    : Projector(prop, task_sizes_train)
{
    create_convex_hull();
}

ProjectorConvexHull::ProjectorConvexHull(const std::shared_ptr<Projector> o)
    : Projector(o)
{
    create_convex_hull();
}

double ProjectorConvexHull::project(const node_ptr& feat)
{
    double* val_ptr = feat->value_ptr(-1, true);
    std::transform(_convex_hull.begin(),
                   _convex_hull.end(),
                   _scores.begin(),
                   [&val_ptr](auto conv_hull){return conv_hull.overlap_1d(val_ptr);});

    return *std::min_element(_scores.begin(), _scores.end());
}

void ProjectorConvexHull::create_convex_hull()
{
    _convex_hull.resize(_n_project_prop);
    for(int pp = 0; pp < _n_project_prop; ++pp)
    {
        _convex_hull[pp].initialize_prop(_task_sizes,
                                         _prop.data() + _n_samp * pp);
    }
}
