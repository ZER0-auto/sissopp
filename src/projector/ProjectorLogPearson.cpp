#include "projector/ProjectorLogPearson.hpp"

ProjectorLogPearson::ProjectorLogPearson(std::vector<double> prop, std::vector<int> task_sizes_train) :
    ProjectorPearson(prop, task_sizes_train),
    _log_feat(_n_samp, 0.0)
{}

ProjectorLogPearson::ProjectorLogPearson(std::shared_ptr<Projector> o) :
    ProjectorPearson(o),
    _log_feat(_n_samp, 0.0)
{}

double ProjectorLogPearson::project(const node_ptr& feat)
{
    std::fill_n(_scores.begin(), _n_project_prop, 0.0);

    double* val_ptr = feat->value_ptr(-1, true);

    if (*std::min_element(val_ptr, val_ptr + _n_samp) <= 0.0)
    {
        return 0.0;
    }

    std::transform(val_ptr,
                   val_ptr + _n_samp,
                   _log_feat.data(),
                   [](double val) { return std::log(val); });

    return calc_max_pearson(_log_feat.data());;
}
