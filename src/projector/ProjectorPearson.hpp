// Copyright 2023 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file loss_function/Projector.hpp
 *  @brief Defines a base class used to calculate the projection score and l0-regularization objective function
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef PROJECTOR_PEARSON
#define PROJECTOR_PEARSON

#include "projector/Projector.hpp"

// DocString: cls_projector_pearson
/**
 * @brief The projection function used during SIS
 *
 */
class ProjectorPearson : public Projector
{
private:
    std::vector<double> _prop_std;
    std::vector<double> _c;

public:

    /**
     * @brief Constructor
     *
     * @param prop The value of the property to evaluate the loss function against for the training set
     * @param task_sizes_train Number of training samples per task
     */
    ProjectorPearson(std::vector<double> prop, std::vector<int> task_sizes_train);

    /**
     * @brief Copy constructor
     *
     * @param o Pointer to the projector to be copied
     */
    ProjectorPearson(std::shared_ptr<Projector> o);

    /**
     * @brief Calculate the projection score of a feature
     *
     * @param feat Feature to calculate the projection score of
     * @return The projection score for the feature
     */
    double project(const node_ptr& feat);

    // DocString: projector_type
    /**
     * @brief The type of the projector
     */
    inline PROJECT_TYPE type() const {return PROJECT_TYPE::PEARSON;}

    void standardize_prop(const double* prop, double* std_prop);

    double calc_max_pearson(double* feat_val_ptr);

};

#endif
