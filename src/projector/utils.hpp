// Copyright 2023 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file utils/project.hpp
 *  @brief Defines a set of functions to project features onto a second vector
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef UTILS_PROJECT
#define UTILS_PROJECT

#include <limits>

#include "projector/ProjectorConvexHull.hpp"
#include "projector/ProjectorLogPearson.hpp"

#ifdef PARAMETERIZE
#include "nl_opt/utils.hpp"
#endif


namespace projector_util
{
/**
 * @brief Copy a projector
 *
 * @param proj The Projector to copy
 */
std::shared_ptr<Projector> copy(std::shared_ptr<Projector> proj);

/**
 * @brief Get the projector for the problem
 *
 * @param project_type The type of projector to create
 * @param prop The property
 * @param task_sizes_train The number of samples per task
 *
 * @return [description]
 */
std::shared_ptr<Projector> get_projector(const std::string project_type,
                                         const std::vector<double> prop,
                                         const std::vector<int> task_sizes);

/**
 * @brief Get the projector for the problem
 *
 * @param project_type The type of projector to create
 * @param models The last set of models created
 *
 * @return [description]
 */
std::shared_ptr<Projector> get_projector(const std::string project_type,
                                         const std::vector<std::shared_ptr<Model>>& models);
}

namespace project_funcs
{
/**
* @brief Calculate the projection score of a set of features
*
* @param projector The Projector used for the projection
* @param feats The set of features to calculate
* @param scores A pointer to the head of the vector to output the scores to
*/
void project(std::shared_ptr<Projector> projector,
             const std::vector<node_ptr>& feats,
             double* scores);

/**
* @brief Calculate the projection score of a set of features without using OpenMP
*
* @param projector The Projector used for the projection
* @param feats The set of features to calculate
* @param scores A pointer to the head of the vector to output the scores to
*/
void project_no_omp(std::shared_ptr<Projector> projector,
                    const std::vector<node_ptr>& feats,
                    double* scores);
}  // namespace project_funcs

#endif

