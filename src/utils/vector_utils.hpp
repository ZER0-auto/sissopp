// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file utils/vector_utils.hpp
 *  @brief Defines and implements set of functions to manipulate vectors
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef VECTOR_UTILS
#define VECTOR_UTILS

#include <algorithm>
#include <vector>

namespace vector_utils
{
/**
 * @brief Return a vector of all unique elements of in_vec
 *
 * @param in_vec The input vector
 * @tparam T The type of elements of T
 * @return The vector of unique elements of in_vec
 */
template <typename T>
std::vector<T> unique(const std::vector<T> in_vec)
{
    std::vector<T> out_vec;
    for (auto& el : in_vec)
    {
        if (std::find(out_vec.begin(), out_vec.end(), el) == out_vec.end())
        {
            out_vec.push_back(el);
        }
    }

    return out_vec;
}

/**
 * @brief Return a vector of all unique elements of in_vec
 *
 * @param in_vec Pointer to the input vector
 * @param sz The size of the vector
 * @tparam T The type of elements of T
 * @return The vector of unique elements of in_vec
 */
template <typename T>
std::vector<T> unique(const T* in_vec, int sz)
{
    std::vector<T> out_vec;
    for (int ii = 0; ii < sz; ++ii)
    {
        if (std::find(out_vec.begin(), out_vec.end(), in_vec[ii]) == out_vec.end())
        {
            out_vec.push_back(in_vec[ii]);
        }
    }
    return out_vec;
}
}  // namespace vector_utils

#endif
