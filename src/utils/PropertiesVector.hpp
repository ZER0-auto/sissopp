// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include <Kokkos_Core.hpp>

#include "feature_creation/node/value_storage/nodes_value_containers.hpp"

/**
 * Holds the properties vector in a Kokkos::View.
 *
 * @tparam MemorySpace memory space to allocate the matrix in
 * @tparam Precision floating point precision to use
 */
template <typename MemorySpace, typename Precision = double>
class PropertiesVector
{
    static_assert(Kokkos::is_memory_space<MemorySpace>::value,
                  "MemorySpace is not a valid Kokkos memory space!");
    static_assert(std::is_floating_point<Precision>::value,
                  "Precision needs to be a floating point data type!");

public:
    using Vector = typename Kokkos::View<Precision*, Kokkos::LayoutLeft, MemorySpace>;

    PropertiesVector(const int64_t num_samples)
        : vector_(Kokkos::ViewAllocateWithoutInitializing("properties_vector"), num_samples)
    {
    }

    PropertiesVector(const std::vector<double>& properties) : PropertiesVector(properties.size())
    {
        auto h_vector = Kokkos::create_mirror_view(vector_);
        for (int material_idx = 0; material_idx < properties.size(); ++material_idx)
        {
            h_vector(material_idx) = properties[material_idx];
        }
        Kokkos::deep_copy(vector_, h_vector);
    }

    Vector getVector() const { return vector_; }

private:
    Vector vector_;
};
