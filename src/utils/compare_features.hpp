// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file utils/compare_features.hpp
 *  @brief Defines a set of functions to compare features to see if they are too correlated to other selected features to be selected here
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef UTILS_FEAT_COMP
#define UTILS_FEAT_COMP

#include <tuple>

#include "feature_creation/node/Node.hpp"
#include "openmp_reduction/ScoreObjectPair.hpp"

namespace comp_feats
{
// clang-format off
    extern std::vector<double> DGEMV_OUT; //!< Function used to store the output of DGEMV
    extern std::vector<double> RANK; //!< Global variable used to store the rank variables for Spearman correlation
    extern std::vector<int> INDEX; //!< Global variable used to store the sorting indexes for Spearman correlation
// clang-format on

/**
     * @brief Set the is_valid and is_valid_feat_list functions for SIS
     *
     * @param project_type The type of LossFunction to use when projecting the features onto a property
     * @param max_corr Maximum cross-correlation used for selecting features
     * @param n_samp Number of samples in the training set
     * @param is_valid Function used to determine of a feature is too correlated to previously selected features
     * @param is_valid_feat_list Function used to determine of a feature is too correlated to previously selected features within a given list
     */
void set_is_valid_fxn(
    const std::string project_type,
    const double max_corr,
    const int n_samp,
    std::function<bool(const double*,
                       const int,
                       const double,
                       const std::vector<double>&,
                       const std::vector<int>&,
                       const double,
                       const int,
                       const int)>& is_valid,
    std::function<int(
        const double*, const int, const double, const std::vector<node_sc_pair>&, const double)>&
        is_valid_feat_list);

/**
     * @brief Reset the RANK and INDEX vectors to be size 0
     */
void reset_vectors();

/**
     * @brief Checks the feature to see if it is still valid against previously selected features (using the Pearson correlation coefficient with a maximum cross-correlation of 1.0)
     *
     * @param val_ptr Pointer to value array of the current feature
     * @param n_samp Number of samples in the training set
     * @param cross_cor_max Maximum cross-correlation used for selecting features
     * @param scores_sel The projection scores for the selected feature
     * @param sorted_score_inds The indexes of the sorted scores
     * @param cur_score The score of the current candidate feature
     * @param end_sel The index of the feature to stop checking
     * @param start_sel The index of the feature to start checking
     *
     * @return True if the feature is still valid
     */
bool valid_feature_against_selected_pearson_max_corr_1(const double* val_ptr,
                                                       const int n_samp,
                                                       const double cross_cor_max,
                                                       const std::vector<double>& scores_sel,
                                                       const std::vector<int>& sorted_score_inds,
                                                       const double cur_score,
                                                       const int end_sel,
                                                       const int start_sel = 0);

/**
     * @brief Checks the feature to see if it is still valid against previously selected features (using the Pearson correlation coefficient with a maximum cross-correlation of 1.0)
     *
     * @param val_ptr Pointer to value array of the current feature
     * @param n_samp Number of samples in the training set
     * @param cross_cor_max Maximum cross-correlation used for selecting features
     * @param selected The list of previously selected features
     * @param scores_sel The projection scores for the selected feature
     * @param cur_score The score of the current candidate feature
     *
     * @return 1 if the feature is valid, 0 if the feature is invalid, -1 if the feature is invalid but has a higher score than the conflicting features
     */
int valid_feature_against_selected_pearson_max_corr_1_feat_list(
    const double* val_ptr,
    const int n_samp,
    const double cross_cor_max,
    const std::vector<node_sc_pair>& scores_phi_sel,
    const double cur_score);

/**
     * @brief Checks the feature to see if it is still valid against previously selected features (using the Pearson correlation coefficient with a maximum cross-correlation of 1.0)
     *
     * @param val_ptr Pointer to value array of the current feature
     * @param n_samp Number of samples in the training set
     * @param cross_cor_max Maximum cross-correlation used for selecting features
     * @param out_vec The output vector for the MPI reduce all operator
     * @param cur_score The score of the current candidate feature
     *
     * @return True if the feature is still valid
     */
bool valid_feature_against_selected_pearson_max_corr_1_mpi_op(
    const double* val_ptr,
    const int n_samp,
    const double cross_cor_max,
    const std::vector<node_sc_pair>& out_vec,
    const double cur_score);

/**
     * @brief Checks the feature to see if it is still valid against previously selected features (using the Pearson correlation coefficient)
     *
     * @param val_ptr Pointer to value array of the current feature
     * @param n_samp Number of samples in the training set
     * @param cross_cor_max Maximum cross-correlation used for selecting features
     * @param scores_sel The projection scores for the selected feature
     * @param sorted_score_inds The indexes of the sorted scores
     * @param cur_score The score of the current candidate feature
     * @param end_sel The index of the feature to stop checking
     * @param start_sel The index of the feature to start checking
     *
     * @return True if the feature is still valid
     */
bool valid_feature_against_selected_pearson(const double* val_ptr,
                                            const int n_samp,
                                            const double cross_cor_max,
                                            const std::vector<double>& scores_sel,
                                            const std::vector<int>& sorted_score_inds,
                                            const double cur_score,
                                            const int end_sel,
                                            const int start_sel = 0);

/**
     * @brief Checks the feature to see if it is still valid against previously selected features (using the Pearson correlation coefficient)
     *
     * @param val_ptr Pointer to value array of the current feature
     * @param n_samp Number of samples in the training set
     * @param cross_cor_max Maximum cross-correlation used for selecting features
     * @param selected The list of previously selected features
     * @param scores_sel The projection scores for the selected feature
     * @param cur_score The score of the current candidate feature
     *
     * @return 1 if the feature is valid, -1 if the feature is invalid, 0 if the feature is invalid but has a higher score than the conflicting features
     */
int valid_feature_against_selected_pearson_feat_list(
    const double* val_ptr,
    const int n_samp,
    const double cross_cor_max,
    const std::vector<node_sc_pair>& scores_phi_sel,
    const double cur_score);

/**
     * @brief Checks the feature to see if it is still valid against previously selected features (using the Pearson correlation coefficient)
     *
     * @param val_ptr Pointer to value array of the current feature
     * @param n_samp Number of samples in the training set
     * @param cross_cor_max Maximum cross-correlation used for selecting features
     * @param out_vec The output vector for the MPI reduce all operator
     * @param cur_score The score of the current candidate feature
     *
     * @return True if the feature is still valid
     */
bool valid_feature_against_selected_pearson_mpi_op(const double* val_ptr,
                                                   const int n_samp,
                                                   const double cross_cor_max,
                                                   const std::vector<node_sc_pair>& out_vec,
                                                   const double cur_score);

/**
     * @brief Checks the feature to see if it is still valid against previously selected features (using the rank correlation coefficient with a maximum cross-correlation of 1.0)
     *
     * @param val_ptr Pointer to value array of the current feature
     * @param n_samp Number of samples in the training set
     * @param cross_cor_max Maximum cross-correlation used for selecting features
     * @param scores_sel The projection scores for the selected feature
     * @param sorted_score_inds The indexes of the sorted scores
     * @param cur_score The score of the current candidate feature
     * @param end_sel The index of the feature to stop checking
     * @param start_sel The index of the feature to start checking
     *
     * @return True if the feature is still valid
     */
bool valid_feature_against_selected_spearman_max_corr_1(const double* val_ptr,
                                                        const int n_samp,
                                                        const double cross_cor_max,
                                                        const std::vector<double>& scores_sel,
                                                        const std::vector<int>& sorted_score_inds,
                                                        const double cur_score,
                                                        const int end_sel,
                                                        const int start_sel = 0);

/**
     * @brief Checks the feature to see if it is still valid against previously selected features (using the rank correlation coefficient with a maximum cross-correlation of 1.0)
     *
     * @param val_ptr Pointer to value array of the current feature
     * @param n_samp Number of samples in the training set
     * @param cross_cor_max Maximum cross-correlation used for selecting features
     * @param selected The list of previously selected features
     * @param scores_sel The projection scores for the selected feature
     * @param cur_score The score of the current candidate feature
     *
     * @return 1 if the feature is valid, -1 if the feature is invalid, 0 if the feature is invalid but has a higher score than the conflicting features
     */
int valid_feature_against_selected_spearman_max_corr_1_feat_list(
    const double* val_ptr,
    const int n_samp,
    const double cross_cor_max,
    const std::vector<node_sc_pair>& scores_phi_sel,
    const double cur_score);

/**
     * @brief Checks the feature to see if it is still valid against previously selected features (using the rank correlation coefficient with a maximum cross-correlation of 1.0)
     *
     * @param val_ptr Pointer to value array of the current feature
     * @param n_samp Number of samples in the training set
     * @param cross_cor_max Maximum cross-correlation used for selecting features
     * @param out_vec The output vector for the MPI reduce all operator
     * @param cur_score The score of the current candidate feature
     *
     * @return True if the feature is still valid
     */
bool valid_feature_against_selected_spearman_max_corr_1_mpi_op(
    const double* val_ptr,
    const int n_samp,
    const double cross_cor_max,
    const std::vector<node_sc_pair>& out_vec,
    const double cur_score);

/**
     * @brief Checks the feature to see if it is still valid against previously selected features (using the rank correlation coefficient)
     *
     * @param val_ptr Pointer to value array of the current feature
     * @param n_samp Number of samples in the training set
     * @param cross_cor_max Maximum cross-correlation used for selecting features
     * @param scores_sel The projection scores for the selected feature
     * @param sorted_score_inds The indexes of the sorted scores
     * @param cur_score The score of the current candidate feature
     * @param end_sel The index of the feature to stop checking
     * @param start_sel The index of the feature to start checking
     *
     * @return True if the feature is still valid
     */
bool valid_feature_against_selected_spearman(const double* val_ptr,
                                             const int n_samp,
                                             const double cross_cor_max,
                                             const std::vector<double>& scores_sel,
                                             const std::vector<int>& sorted_score_inds,
                                             const double cur_score,
                                             const int end_sel,
                                             const int start_sel = 0);

/**
     * @brief Checks the feature to see if it is still valid against previously selected features (using the rank correlation coefficient)
     *
     * @param val_ptr Pointer to value array of the current feature
     * @param n_samp Number of samples in the training set
     * @param cross_cor_max Maximum cross-correlation used for selecting features
     * @param selected The list of previously selected features
     * @param scores_sel The projection scores for the selected feature
     * @param cur_score The score of the current candidate feature
     *
     * @return 1 if the feature is valid, -1 if the feature is invalid, 0 if the feature is invalid but has a higher score than the conflicting features
     */
int valid_feature_against_selected_spearman_feat_list(
    const double* val_ptr,
    const int n_samp,
    const double cross_cor_max,
    const std::vector<node_sc_pair>& scores_phi_sel,
    const double cur_score);

/**
     * @brief Checks the feature to see if it is still valid against previously selected features (using the rank correlation coefficient)
     *
     * @param val_ptr Pointer to value array of the current feature
     * @param n_samp Number of samples in the training set
     * @param cross_cor_max Maximum cross-correlation used for selecting features
     * @param out_vec The output vector for the MPI reduce all operator
     * @param cur_score The score of the current candidate feature
     *
     * @return True if the feature is still valid
     */
bool valid_feature_against_selected_spearman_mpi_op(const double* val_ptr,
                                                    const int n_samp,
                                                    const double cross_cor_max,
                                                    const std::vector<node_sc_pair>& out_vec,
                                                    const double cur_score);
}  // namespace comp_feats

#endif
