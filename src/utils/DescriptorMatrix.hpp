// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include <Kokkos_Core.hpp>

#include "feature_creation/node/value_storage/nodes_value_containers.hpp"

/**
 * Holds the descriptor matrix in a Kokkos::View.
 * The data layout is always Kokkos::LayoutLeft!
 *
 * @tparam MemorySpace memory space to allocate the matrix in
 * @tparam Precision floating point precision to use
 */
template <typename MemorySpace, typename Precision = double>
class DescriptorMatrix
{
    static_assert(Kokkos::is_memory_space<MemorySpace>::value,
                  "MemorySpace is not a valid Kokkos memory space!");
    static_assert(std::is_floating_point<Precision>::value,
                  "Precision needs to be a floating point data type!");

public:
    using Matrix = typename Kokkos::View<Precision**, Kokkos::LayoutLeft, MemorySpace>;

    /**
     * Initialize empty descriptor matrix with dimensions num_features x num_samples.
     */
    DescriptorMatrix(const int64_t num_samples, const int64_t num_features)
        : matrix_(Kokkos::ViewAllocateWithoutInitializing("descriptor_matrix"),
                  num_samples,
                  num_features)
    {
    }

    /**
     * Initialize the matrix from node_value_arrs::D_MATRIX.
     */
    DescriptorMatrix() : DescriptorMatrix(node_value_arrs::N_SAMPLES, node_value_arrs::N_SELECTED)
    {
        auto h_matrix = Kokkos::create_mirror_view(matrix_);
        for (int feature_idx = 0; feature_idx < node_value_arrs::N_SELECTED; ++feature_idx)
        {
            for (int sample_idx = 0; sample_idx < node_value_arrs::N_SAMPLES; ++sample_idx)
            {
                h_matrix(sample_idx, feature_idx) = node_value_arrs::D_MATRIX
                    [feature_idx * node_value_arrs::N_SAMPLES + sample_idx];
            }
        }
        Kokkos::deep_copy(matrix_, h_matrix);
    }

    Matrix getMatrix() const { return matrix_; }

private:
    Matrix matrix_;
};
