// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#include "split_range.hpp"

std::pair<int, int> split_range(const std::pair<int, int>& range,
                                const int comm_rank,
                                const int comm_size)
{
    CHECK_LESS_EQUAL(range.first, range.second, "not a valid range");
    CHECK_GREATER_EQUAL(comm_rank, 0, "negative mpi rank encountered");
    CHECK_GREATER_EQUAL(comm_size, 1, "communication needs at least one rank");
    CHECK_LESS(comm_rank, comm_size, "mpi rank outside of comm size");

    const auto num_elements = range.second - range.first;
    const auto increment = num_elements / static_cast<double>(comm_size);
    auto result = std::pair<int, int>(range.first + std::lround(increment * comm_rank),
                                      range.first + std::lround(increment * (comm_rank + 1)));

    CHECK_GREATER_EQUAL(result.first, range.first, "result not within range");
    CHECK_LESS_EQUAL(result.second, range.second, "result not within range");
    CHECK_LESS_EQUAL(result.first, result.second, "not a valid range");

    return result;
}