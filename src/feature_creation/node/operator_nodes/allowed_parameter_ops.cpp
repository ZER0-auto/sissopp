// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/allowed_parameter_ops.cpp
 *  @brief Implements the map that converts std::string descriptions of the operators into their generator functions for parameterized operations
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "feature_creation/node/operator_nodes/allowed_ops.hpp"

std::map<std::string, un_param_op_node_gen> allowed_op_maps::UNARY_PARAM_OPERATOR_MAP;
std::map<std::string, bin_param_op_node_gen> allowed_op_maps::BINARY_PARAM_OPERATOR_MAP;

void allowed_op_maps::set_param_node_maps()
{
    allowed_op_maps::BINARY_PARAM_OPERATOR_MAP["add"] = generateAddParamNode;
    allowed_op_maps::BINARY_PARAM_OPERATOR_MAP["sub"] = generateSubParamNode;
    allowed_op_maps::BINARY_PARAM_OPERATOR_MAP["abs_diff"] = generateAbsDiffParamNode;
    allowed_op_maps::BINARY_PARAM_OPERATOR_MAP["mult"] = generateMultParamNode;
    allowed_op_maps::BINARY_PARAM_OPERATOR_MAP["div"] = generateDivParamNode;

    allowed_op_maps::UNARY_PARAM_OPERATOR_MAP["exp"] = generateExpParamNode;
    allowed_op_maps::UNARY_PARAM_OPERATOR_MAP["neg_exp"] = generateNegExpParamNode;
    allowed_op_maps::UNARY_PARAM_OPERATOR_MAP["inv"] = generateInvParamNode;
    allowed_op_maps::UNARY_PARAM_OPERATOR_MAP["sq"] = generateSqParamNode;
    allowed_op_maps::UNARY_PARAM_OPERATOR_MAP["cb"] = generateCbParamNode;
    allowed_op_maps::UNARY_PARAM_OPERATOR_MAP["six_pow"] = generateSixPowParamNode;
    allowed_op_maps::UNARY_PARAM_OPERATOR_MAP["sqrt"] = generateSqrtParamNode;
    allowed_op_maps::UNARY_PARAM_OPERATOR_MAP["cbrt"] = generateCbrtParamNode;
    allowed_op_maps::UNARY_PARAM_OPERATOR_MAP["log"] = generateLogParamNode;
    allowed_op_maps::UNARY_PARAM_OPERATOR_MAP["abs"] = generateAbsParamNode;
    allowed_op_maps::UNARY_PARAM_OPERATOR_MAP["sin"] = generateSinParamNode;
    allowed_op_maps::UNARY_PARAM_OPERATOR_MAP["cos"] = generateCosParamNode;
}
