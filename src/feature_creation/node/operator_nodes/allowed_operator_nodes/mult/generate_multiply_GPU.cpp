// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/allowed_operator_nodes/generate_multiply_GPU.cpp
 *  @brief Defines a class for the multiplication operator
 *
 *  @author Yi Yao
 *  @bug No known bugs.
 *
 *  This class represents the binary operator -> A * B
 */


#include "feature_creation/node/operator_nodes/allowed_operator_nodes/mult/generate_multiply_GPU.hpp"


void generateFeatListPairMult(std::vector<size_t>& new_feat_1_list,
                              std::vector<size_t>& new_feat_2_list,
                              const std::vector<std::vector<node_ptr>::iterator>& feat_it_list_1,
                              const std::vector<node_ptr>& feat_list_0,
                              const int max_leaves)
{
    // convert the div_mult_leaves from map<string,double> to map<size_t,double> for 
    // efficient comparison.
    std::vector<std::map<std::string, double>> div_mult_leaves_list_0_str;
    std::vector<std::map<size_t, double>> div_mult_leaves_list_0;
    std::vector<std::vector<size_t>> div_mult_leaves_list_0_idx;
    std::vector<std::vector<double>> div_mult_leaves_list_0_val;
    std::vector<double> expected_abs_tot_list_0;
    std::map<std::string,size_t> unique_features;
    std::vector<NODE_TYPE> node_type_list_0;

    std::vector<int> n_leaves_0;

    for (auto it_feat_2 = feat_list_0.begin(); it_feat_2 != feat_list_0.end(); it_feat_2++) {
        const node_ptr& feat_2 = *it_feat_2;
        node_type_list_0.push_back(feat_2->type());
        n_leaves_0.push_back(feat_2->n_leaves());
        double expected_abs_tot = 0.0;
        std::map<std::string, double> div_mult_leaves;
        feat_2->update_div_mult_leaves(div_mult_leaves, 1.0, expected_abs_tot);
        div_mult_leaves_list_0_str.push_back(div_mult_leaves);
        expected_abs_tot_list_0.push_back(expected_abs_tot);
        for (auto pair: div_mult_leaves) {
            if (unique_features.find(pair.first) == unique_features.end()) {
              unique_features[pair.first] = unique_features.size();
            }
        }
    }

    for (auto it_feat_2 = feat_list_0.begin(); it_feat_2 != feat_list_0.end(); it_feat_2++) {
        std::map<size_t, double> div_mult_leaves;
        for (auto pair: div_mult_leaves_list_0_str[it_feat_2-feat_list_0.begin()]) {
            div_mult_leaves[unique_features[pair.first]] = pair.second;
        }
        std::vector<size_t> div_mult_leaves_idx;
        std::vector<double> div_mult_leaves_val;
        for (auto pair: div_mult_leaves) {
            div_mult_leaves_idx.push_back(pair.first);
            div_mult_leaves_val.push_back(pair.second);
        }
        div_mult_leaves_list_0.push_back(div_mult_leaves);
        div_mult_leaves_list_0_idx.push_back(div_mult_leaves_idx);
        div_mult_leaves_list_0_val.push_back(div_mult_leaves_val);
    }

    #pragma omp parallel 
    {
    std::vector<size_t> new_feat_1_list_local;
    std::vector<size_t> new_feat_2_list_local;
    std::vector<double> div_mult_leaves_val_combine(unique_features.size(), 0.0);
    #pragma omp for schedule(monotonic : dynamic)
    for(auto it_it_feat_1 = feat_it_list_1.begin(); it_it_feat_1 != feat_it_list_1.end(); ++it_it_feat_1) {
      auto it_feat_1 = *it_it_feat_1;
      size_t idx_1 = it_feat_1-feat_list_0.begin();
      for (auto it_feat_2 = feat_list_0.begin(); it_feat_2 < it_feat_1; ++it_feat_2) {
        size_t idx_2 = it_feat_2-feat_list_0.begin();
        // If either feature is an inverse or division operation this feature will be a repeat
        if ((n_leaves_0[idx_1] + n_leaves_0[idx_2] > max_leaves) ||
            ( node_type_list_0[idx_1] == NODE_TYPE::INV)        || (node_type_list_0[idx_2] == NODE_TYPE::INV) ||
            ((node_type_list_0[idx_1] == NODE_TYPE::DIV)        && (node_type_list_0[idx_2] == NODE_TYPE::DIV)) ||
            ( node_type_list_0[idx_1] == NODE_TYPE::PARAM_INV)  || (node_type_list_0[idx_2] == NODE_TYPE::PARAM_INV) ||
            ( node_type_list_0[idx_1] == NODE_TYPE::PARAM_DIV)  || (node_type_list_0[idx_2] == NODE_TYPE::PARAM_DIV) ||
            ( node_type_list_0[idx_1] == NODE_TYPE::PARAM_MULT) || (node_type_list_0[idx_2] == NODE_TYPE::PARAM_MULT))
        {
            continue;
        }

        // Check if the feature would simplify to a less complicated one
        std::vector<size_t>& div_mult_leaves_idx_1 = div_mult_leaves_list_0_idx[idx_1];
        std::vector<size_t>& div_mult_leaves_idx_2 = div_mult_leaves_list_0_idx[idx_2];
        int size_1 = div_mult_leaves_idx_1.size();
        int size_2 = div_mult_leaves_idx_2.size();
        int i1 = 0;
        int i2 = 0;
        int i_combine = 0;
        std::vector<double>& div_mult_leaves_val_1 = div_mult_leaves_list_0_val[idx_1];
        std::vector<double>& div_mult_leaves_val_2 = div_mult_leaves_list_0_val[idx_2];
        while(i1 < size_1 && i2 < size_2) {
            if (div_mult_leaves_idx_1[i1] == div_mult_leaves_idx_2[i2]) {
               div_mult_leaves_val_combine[i_combine++] = 
                   div_mult_leaves_val_1[i1++] +
                   div_mult_leaves_val_2[i2++];
            } else if (div_mult_leaves_idx_1[i1] < div_mult_leaves_idx_2[i2]) {
               div_mult_leaves_val_combine[i_combine++] = 
                   div_mult_leaves_val_1[i1++];
            } else {
               div_mult_leaves_val_combine[i_combine++] = 
                   div_mult_leaves_val_2[i2++];
            }
        }
        while(i1 < size_1) {
            div_mult_leaves_val_combine[i_combine++] =
                div_mult_leaves_val_1[i1++];
        }
        while(i2 < size_2) {
            div_mult_leaves_val_combine[i_combine++] =
                div_mult_leaves_val_2[i2++];
        }

        double expected_abs_tot = 
            expected_abs_tot_list_0[idx_1] +
            expected_abs_tot_list_0[idx_2];

        double leaves_v_expected = std::accumulate(
            div_mult_leaves_val_combine.begin(),
            div_mult_leaves_val_combine.begin() + i_combine,
            -1.0 * expected_abs_tot,
            [](double tot, auto el) { return tot + std::abs(el); });
        if ((i_combine < 2) || (std::abs(leaves_v_expected) > 1e-12))
        {
            continue;
        }

        // Check if feature is a constant multiple of a previous feature
        double div_mult_tot_first = std::abs(div_mult_leaves_val_combine.front());
        if ((std::abs(div_mult_tot_first) - 1.0 > 1e-12) &&
            std::all_of(div_mult_leaves_val_combine.begin(), div_mult_leaves_val_combine.begin() + i_combine, [&div_mult_tot_first](auto el) {
                return std::abs(el) == div_mult_tot_first;
            }))
        {
            continue;
        }
        new_feat_1_list_local.push_back(idx_1);
        new_feat_2_list_local.push_back(idx_2);
      }
    }
    #pragma omp critical
    {
        new_feat_1_list.insert(new_feat_1_list.end(), new_feat_1_list_local.begin(), new_feat_1_list_local.end());
        new_feat_2_list.insert(new_feat_2_list.end(), new_feat_2_list_local.begin(), new_feat_2_list_local.end());
    }

    }
}
