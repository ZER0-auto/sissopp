// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/allowed_operator_nodes/sub/generate_subtract_GPU.hpp
 *  @brief Kokkos GPU implementation of Generation the subtract nodes.
 *
 *  @author Yi Yao
 *  @bug No known bugs.
 *
 */

#ifndef GENERATE_SUB_NODE_GPU
#define GENERATE_SUB_NODE_GPU

#ifdef PROFILE_W_CALIPER
#include <caliper/cali.h>
#endif

#include <Kokkos_Core.hpp>
#include <Kokkos_StdAlgorithms.hpp>

#include <set>

#include "feature_creation/node/operator_nodes/OperatorNode.hpp"
#include "feature_creation/node/value_storage/nodes_value_containers.hpp"

#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sub/subtract.hpp"
#include "feature_creation/node/operator_nodes/utils_GPU_team_thread.hpp"



// template<typename PRECISION>
// void generateSubNodeBatched(std::vector<node_ptr>& feat_list,
//                             const std::vector<std::vector<node_ptr>::iterator>& feat_list_1,
//                             const std::vector<node_ptr>& feat_list_0,
//                             unsigned long int& feat_ind,
//                             const PRECISION l_bound,
//                             const PRECISION u_bound);

/**
 * @brief generate potential feature lists of A and B features.
 *
 * @param new_feat_1_list list of A features of potential new feature
 * @param new_feat_2_list list of B features of potential new feature
 * @param feat_it_list_1 The list of feature (A)
 * @param feat_list_0 The list of features in lower rungs.
 * @param max_leaves Maximum number of leaves in the binary expression tree
 */
void generateFeatListPairSub(std::vector<size_t>& new_feat_1_list,
                             std::vector<size_t>& new_feat_2_list,
                             const std::vector<std::vector<node_ptr>::iterator>& feat_it_list_1,
                             const std::vector<node_ptr>& feat_list_0,
                             const int max_leaves);

/**
 * @brief Attempt to generate new subtract nodes (A - B) and add it to feat_list
 *
 * @param feat_list list of features already generated
 * @param feat_list_1 The list of feature (A)
 * @param feat_list_0 The list of features in lower rungs.
 * @param feat_ind Index of the new feature
 * @param max_leaves Maximum number of leaves in the binary expression tree
 * @param l_bound Minimum absolute value allowed for the feature.
 * @param u_bound Maximum absolute value allowed for the feature.
 */
template<typename PRECISION>
void generateSubNodeBatched(std::vector<node_ptr>& feat_list,
                            const std::vector<std::vector<node_ptr>::iterator>& feat_it_list_1,
                            const std::vector<node_ptr>& feat_list_0,
                            unsigned long int& feat_ind,
                            const int max_leaves,
                            const PRECISION l_bound,
                            const PRECISION u_bound)
{
    std::vector<size_t> new_feat_1_list;
    std::vector<size_t> new_feat_2_list;
#ifdef PROFILE_W_CALIPER
    CALI_MARK_BEGIN("sub: generate_feat_list_pair");
#endif
    generateFeatListPairSub(new_feat_1_list, new_feat_2_list, feat_it_list_1, feat_list_0, max_leaves);

#ifdef PROFILE_W_CALIPER
    CALI_MARK_END("sub: generate_feat_list_pair");
#endif

#ifdef PROFILE_W_CALIPER
    CALI_MARK_BEGIN("sub: construct and check new feats");
#endif
    size_t n_feat_pair = new_feat_1_list.size();
    size_t n_sample = feat_list_0[0]->n_samp();

    size_t n_old_feat = feat_list_0.size();

    std::vector<PRECISION> values_old_feats(node_value_arrs::VALUES_ARR.begin(),
                                            node_value_arrs::VALUES_ARR.end());
    std::vector<int> tasks_sz_train(node_value_arrs::TASK_SZ_TRAIN.begin(),
                                    node_value_arrs::TASK_SZ_TRAIN.end());
    int n_tasks = tasks_sz_train.size();

    Kokkos::View<PRECISION**, Kokkos::HostSpace> values_old_feats_view(values_old_feats.data(), n_old_feat, n_sample);
    Kokkos::View<int*, Kokkos::HostSpace> tasks_sz_train_view(tasks_sz_train.data(), n_tasks);
    Kokkos::View<size_t*, Kokkos::HostSpace> new_feat_1_list_view(new_feat_1_list.data(), n_feat_pair);
    Kokkos::View<size_t*, Kokkos::HostSpace> new_feat_2_list_view(new_feat_2_list.data(), n_feat_pair);

    //std::cout << "n_feat_pair " << n_feat_pair << std::endl;

    Kokkos::View<bool*, Kokkos::HostSpace> valid_new_feats_view("valid_new_feats", n_feat_pair);
    size_t scratch_size = ScratchViewType<PRECISION>::shmem_size(n_sample) +
                          ScratchViewType<int>::shmem_size(n_tasks);

    auto values_old_feats_view_dev = create_mirror_view(Kokkos::DefaultExecutionSpace(), values_old_feats_view);
    auto tasks_sz_train_view_dev = create_mirror_view(Kokkos::DefaultExecutionSpace(), tasks_sz_train_view);
    auto new_feat_1_list_view_dev = create_mirror_view(Kokkos::DefaultExecutionSpace(), new_feat_1_list_view);
    auto new_feat_2_list_view_dev = create_mirror_view(Kokkos::DefaultExecutionSpace(), new_feat_2_list_view);
    auto valid_new_feats_view_dev = create_mirror_view(Kokkos::DefaultExecutionSpace(), valid_new_feats_view);
    Kokkos::deep_copy(values_old_feats_view_dev, values_old_feats_view);
    Kokkos::deep_copy(tasks_sz_train_view_dev, tasks_sz_train_view);
    Kokkos::deep_copy(new_feat_1_list_view_dev, new_feat_1_list_view);
    Kokkos::deep_copy(new_feat_2_list_view_dev, new_feat_2_list_view);
    Kokkos::parallel_for("cal new_feat_vals and check the values", 
        Kokkos::TeamPolicy<>(n_feat_pair, Kokkos::AUTO).set_scratch_size(0, Kokkos::PerTeam ( scratch_size)), 
            KOKKOS_LAMBDA (const Kokkos::TeamPolicy<>::member_type &teamMember) {
            const size_t i_feat_pair = teamMember.league_rank();

        ScratchViewType<PRECISION> values_new_feats_tmp( teamMember.team_scratch( 0 ), n_sample);
        ScratchViewType<int> tasks_sz_train_tmp( teamMember.team_scratch( 0 ), n_tasks);
        for (int i_task = 0; i_task < n_tasks; i_task++) {
            tasks_sz_train_tmp(i_task) = tasks_sz_train_view_dev(i_task);
        }

        // cal new_feat_vals
        Kokkos::parallel_for( Kokkos::TeamThreadRange( teamMember, n_sample ), 
            [=] ( const size_t i_sample ) {
            values_new_feats_tmp(i_sample) = 
            values_old_feats_view_dev(new_feat_1_list_view_dev(i_feat_pair), i_sample)
            - values_old_feats_view_dev(new_feat_2_list_view_dev(i_feat_pair), i_sample);
            });

        PRECISION mean = 0.0;
        PRECISION norm = 0.0;
        PRECISION stand_dev = 0.0;
        bool is_const = false;
        bool any_larger_than_u_bound = false;
        bool all_smaller_than_l_bound = false;

        int pos = 0;
        for (int i_task = 0; i_task < n_tasks; i_task++) {
            int sz = tasks_sz_train_tmp(i_task);
            mean = 0.0;
            norm = 0.0;
            stand_dev = 0.0;
            // get mean of new values
            calculate_mean_team_thread<PRECISION>( teamMember, pos, sz, 
                                        values_new_feats_tmp, mean);
            // get norm of new values
            calculate_norm_team_thread<PRECISION>( teamMember, pos, sz, 
                                        values_new_feats_tmp, norm);
            // get standard deviation of new values
            calculate_stand_dev_team_thread<PRECISION>( teamMember, pos, sz, 
                                        values_new_feats_tmp, mean, stand_dev);

            if (norm < 1e-10 || stand_dev / norm < 1e-10) {
                is_const = true;
            }
            pos += sz;
        }
        if (is_const) {
            valid_new_feats_view_dev(i_feat_pair) = false;
            return;
        }

        // check wether exceed u_bound
        check_any_larger_than_u_bound_team_thread<PRECISION>(teamMember, n_sample, 
                                                  values_new_feats_tmp, u_bound, any_larger_than_u_bound);
        if (any_larger_than_u_bound) {
           valid_new_feats_view_dev(i_feat_pair) = false;
           return;
        }

        // check whether always smaller than l_bound
        check_all_smaller_than_l_bound_team_thread<PRECISION>(teamMember, n_sample, 
                                                   values_new_feats_tmp, l_bound, all_smaller_than_l_bound);
        if (all_smaller_than_l_bound) {
           valid_new_feats_view_dev(i_feat_pair) = false;
           return;
        }

        valid_new_feats_view_dev(i_feat_pair) = true;
        });

    Kokkos::deep_copy(valid_new_feats_view, valid_new_feats_view_dev);
#ifdef PROFILE_W_CALIPER
    CALI_MARK_END("sub: construct and check new feats");
#endif

#ifdef PROFILE_W_CALIPER
    CALI_MARK_BEGIN("sub: create nodes");
#endif
    for(size_t i_feat_pair = 0; i_feat_pair < n_feat_pair; i_feat_pair++) {
        if (valid_new_feats_view(i_feat_pair)) {
          feat_list.emplace_back(std::make_shared<SubNode>(
            feat_list_0[new_feat_1_list[i_feat_pair]], 
            feat_list_0[new_feat_2_list[i_feat_pair]], 
            feat_ind + i_feat_pair + 1));
        }
    }
    feat_ind += n_feat_pair;
#ifdef PROFILE_W_CALIPER
    CALI_MARK_END("sub: create nodes");
#endif
}

#endif
