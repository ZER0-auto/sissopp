// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/allowed_operator_nodes/sub/parameterized_subtract.cpp
 *  @brief Implements a class for the parameterized version of subtraction operator
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This class represents the parameterized unary operator -> A - (alpha * B)
 */

#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sub/parameterized_subtract.hpp"

BOOST_SERIALIZATION_ASSUME_ABSTRACT(SubParamNode)

void generateSubParamNode(std::vector<node_ptr>& feat_list,
                          node_ptr feat_1,
                          const node_ptr feat_2,
                          unsigned long int& feat_ind,
                          const int max_leaves,
                          const double l_bound,
                          const double u_bound,
                          std::shared_ptr<NLOptimizer> optimizer)
{
    if (feat_1->n_leaves() + feat_2->n_leaves() > max_leaves)
    {
        return;
    }

    std::map<std::string, int> add_sub_leaves;
    int expected_abs_tot = 0;
    feat_1->update_add_sub_leaves(add_sub_leaves, 1, expected_abs_tot);
    feat_2->update_add_sub_leaves(add_sub_leaves, 1, expected_abs_tot);

    if ((add_sub_leaves.size() < 2))
    {
        return;
    }

    ++feat_ind;
    node_ptr new_feat = std::make_shared<SubParamNode>(feat_1, feat_2, feat_ind, optimizer);

    // If a scale parameter is 0.0 feature is invalid
    if ((std::abs(new_feat->parameters()[0]) <= 1e-10))
    {
        return;
    }

    new_feat->set_value();

    // Check if the feature is NaN, constant, greater than the allowed max of less than the allowed min
    if (new_feat->is_nan() || new_feat->is_const() ||
        (util_funcs::max_abs_val<double>(new_feat->value_ptr(), new_feat->n_samp()) > u_bound) ||
        (util_funcs::max_abs_val<double>(new_feat->value_ptr(), new_feat->n_samp()) < l_bound))
    {
        return;
    }

    feat_list.push_back(new_feat);
}

SubParamNode::SubParamNode() {}

SubParamNode::SubParamNode(const node_ptr feat_1,
                           const node_ptr feat_2,
                           const unsigned long int feat_ind,
                           const int max_leaves,
                           const double l_bound,
                           const double u_bound,
                           std::shared_ptr<NLOptimizer> optimizer)
    : SubNode(feat_1, feat_2, feat_ind)
{
    if (feat_1->n_leaves() + feat_2->n_leaves() > max_leaves)
    {
        throw InvalidFeatureException();
    }
    _params.resize(n_params_possible(), 0.0);
    get_parameters(optimizer);

    // Check if the scale parameter is 0.0 or if the feature is NaN, constant, greater than the allowed max of less than the allowed min
    if ((std::abs(_params[0]) <= 1e-10) || is_nan() || is_const() ||
        (util_funcs::max_abs_val<double>(value_ptr(), _n_samp) > u_bound) ||
        (util_funcs::max_abs_val<double>(value_ptr(), _n_samp) < l_bound))
    {
        throw InvalidFeatureException();
    }
}

SubParamNode::SubParamNode(const node_ptr feat_1,
                           const node_ptr feat_2,
                           const unsigned long int feat_ind,
                           std::shared_ptr<NLOptimizer> optimizer)
    : SubNode(feat_1, feat_2, feat_ind)
{
    _params.resize(n_params_possible(), 0.0);
    get_parameters(optimizer);
}

SubParamNode::SubParamNode(const node_ptr feat_1,
                           const node_ptr feat_2,
                           const unsigned long int feat_ind,
                           const int max_leaves,
                           const double l_bound,
                           const double u_bound)
    : SubNode(feat_1, feat_2, feat_ind)
{
    if (feat_1->n_leaves() + feat_2->n_leaves() > max_leaves)
    {
        throw InvalidFeatureException();
    }
    _params.resize(n_params_possible(), 0.0);
}

SubParamNode::SubParamNode(std::array<node_ptr, 2> feats,
                           const unsigned long int feat_ind,
                           std::vector<double> params)
    : SubNode(feats, feat_ind), _params(params)
{
}

node_ptr SubParamNode::hard_copy() const
{
    node_ptr cp = std::make_shared<SubParamNode>(
        _feats[0]->hard_copy(), _feats[1]->hard_copy(), _feat_ind);
    cp->set_selected(_selected);
    cp->set_d_mat_ind(_d_mat_ind);
    cp->set_parameters(_params.data());
    return cp;
}

void SubParamNode::get_parameters(std::shared_ptr<NLOptimizer> optimizer)
{
    double min_res = optimizer->optimize_feature_params(this);
    if (min_res == std::numeric_limits<double>::infinity())
    {
        _params[0] = 0.0;
    }
}

void SubNode::set_value(const double* params,
                        int offset,
                        const bool for_comp,
                        const int depth) const
{
    bool is_root = (offset == -1);
    offset += is_root;

    double* vp_0;
    if (depth < nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        vp_0 = _feats[0]->value_ptr(
            params + _feats[1]->n_params_possible() + 2, 2 * offset, for_comp, depth + 1);
    }
    else
    {
        vp_0 = _feats[0]->value_ptr(2 * offset, for_comp);
    }

    double* vp_1;
    if (depth < nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        vp_1 = _feats[1]->value_ptr(params + 2, 2 * offset + 1, for_comp, depth + 1);
    }
    else
    {
        vp_1 = _feats[1]->value_ptr(2 * offset + 1, for_comp);
    }

    double* val_ptr;
    if (_selected && is_root)
    {
        val_ptr = node_value_arrs::get_d_matrix_ptr(_d_mat_ind);
    }
    else
    {
        val_ptr = node_value_arrs::access_param_storage(rung(), offset, for_comp);
    }

    allowed_op_funcs::sub(_n_samp, vp_0, vp_1, params[0], params[1], val_ptr);
}

void SubNode::set_test_value(const double* params,
                             int offset,
                             const bool for_comp,
                             const int depth) const
{
    offset += (offset == -1);

    double* vp_0;
    if (depth < nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        vp_0 = _feats[0]->test_value_ptr(
            params + _feats[1]->n_params_possible() + 2, 2 * offset, for_comp, depth + 1);
    }
    else
    {
        vp_0 = _feats[0]->test_value_ptr(2 * offset, for_comp);
    }

    double* vp_1;
    if (depth < nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        vp_1 = _feats[1]->test_value_ptr(params + 2, 2 * offset + 1, for_comp, depth + 1);
    }
    else
    {
        vp_1 = _feats[1]->test_value_ptr(2 * offset + 1, for_comp);
    }
    allowed_op_funcs::sub(_n_samp_test,
                          vp_0,
                          vp_1,
                          params[0],
                          params[1],
                          node_value_arrs::access_param_storage_test(rung(), offset, for_comp));
}

void SubNode::set_bounds(double* lb, double* ub, const int depth) const
{
    lb[0] = 0.0;
    lb[1] = 0.0;
    ub[1] = 0.0;

    if (depth >= nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        return;
    }

    _feats[0]->set_bounds(lb + 2 + _feats[1]->n_params_possible(),
                          ub + 2 + _feats[1]->n_params_possible(),
                          depth + 1);
    _feats[1]->set_bounds(lb + 2, ub + 2, depth + 1);
}

void SubNode::initialize_params(double* params, const int depth) const
{
    params[0] = 1.0;
    params[1] = 0.0;

    if (depth >= nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        return;
    }

    _feats[0]->initialize_params(params + 2 + _feats[1]->n_params_possible(), depth + 1);
    _feats[1]->initialize_params(params + 2, depth + 1);
}

void SubParamNode::update_postfix(std::string& cur_expr, const bool add_params, const int depth) const
{
    std::stringstream postfix;
    postfix << get_postfix_term();
    if (add_params)
    {
        postfix << ":" << std::setprecision(13) << std::scientific << _params[0];
        for (size_t pp = 1; pp < _params.size(); ++pp)
        {
            postfix << "," << std::setprecision(13) << std::scientific << _params[pp];
        }
    }
    cur_expr = postfix.str() + "|" + cur_expr;
    _feats[1]->update_postfix(cur_expr, depth >= nlopt_wrapper::MAX_PARAM_DEPTH, depth + 1);
    _feats[0]->update_postfix(cur_expr, depth >= nlopt_wrapper::MAX_PARAM_DEPTH, depth + 1);
}

void SubNode::generate_grad(int depth, const double* params, const double* dfdp, const double* val_ptr, double* grad) const
{
    int np = n_params_possible(0, depth);
    int np_0 = _feats[1]->n_params_possible(0, depth + 1);

    double* val_ptr_2 = (depth < nlopt_wrapper::MAX_PARAM_DEPTH)
                            ? _feats[1]->value_ptr(params + 2, 1, true, depth + 1)
                            : _feats[1]->value_ptr(1, true);

    // d(f(x) - \alpha g(x) - \beta) / d\alpha = -g(x)
    std::transform(val_ptr_2,
                   val_ptr_2 + _n_samp,
                   grad,
                   grad,
                   [](double vp2, double g){return g * vp2 * -1.0;});

    // d(f(x) - \alpha g(x) - \beta) / d\beta = -1
    std::transform(dfdp,
                   dfdp + _n_samp,
                   grad + _n_samp,
                   grad + _n_samp,
                   std::multiplies<double>());

    // Set up for the chain rule
    // d(f(x) + alpha g(y)) / dp_g = \alpha dg/dp_g
    for (int pp = 2; pp < 2 + np_0; ++pp)
    {
        std::transform(dfdp,
                       dfdp + _n_samp,
                       grad + pp * _n_samp,
                       grad + pp * _n_samp,
                       [params](double d, double g) { return params[0] * d * g; });
    }

    // d(f(x) + alpha g(y)) / dp_f = df/dp_f
    for (int pp = 2 + np_0; pp < np; ++pp)
    {
        std::transform(dfdp,
                       dfdp + _n_samp,
                       grad + pp * _n_samp,
                       grad + pp * _n_samp,
                       [](double d, double g) { return d * g; });
    }
}
