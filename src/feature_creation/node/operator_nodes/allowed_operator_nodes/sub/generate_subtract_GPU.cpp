// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/allowed_operator_nodes/sub/generate_subtract_GPU.cpp
 *  @brief Kokkos GPU implementation of Generation the subtract nodes.
 *
 *  @author Yi Yao
 *  @bug No known bugs.
 *
 */

#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sub/generate_subtract_GPU.hpp"


void generateFeatListPairSub(std::vector<size_t>& new_feat_1_list,
                             std::vector<size_t>& new_feat_2_list,
                             const std::vector<std::vector<node_ptr>::iterator>& feat_it_list_1,
                             const std::vector<node_ptr>& feat_list_0,
                             const int max_leaves)
{
    // convert the units from map<string,double> to vector<double> for 
    // efficient comparison.
    std::vector<std::map<std::string, double>> unit_list_0_str;
    std::map<std::string,size_t> unique_units;
    std::vector<NODE_TYPE> node_type_list_0;
    for (auto it_feat_2 = feat_list_0.begin(); it_feat_2 != feat_list_0.end(); it_feat_2++) {
        const node_ptr& feat_2 = *it_feat_2;
        node_type_list_0.push_back(feat_2->type());
        std::map<std::string, double> unit_2 = feat_2->unit().dct();
        unit_list_0_str.push_back(unit_2);
        for (auto pair: unit_2) {
            if (unique_units.find(pair.first) == unique_units.end()) {
              unique_units[pair.first] = unique_units.size();
            }
        }
    }
    std::vector<std::vector<double>> unit_list_0; // unit list represented as vectors
    for (auto it_feat_2 = feat_list_0.begin(); it_feat_2 != feat_list_0.end(); it_feat_2++) {
        std::vector<double> unit_vec(unique_units.size(),0.0);
        for (auto pair: unit_list_0_str[it_feat_2-feat_list_0.begin()]) {
            unit_vec[unique_units[pair.first]] = pair.second;
        }
        unit_list_0.push_back(unit_vec);
    }

    #pragma omp parallel 
    {
    std::vector<size_t> new_feat_1_list_local;
    std::vector<size_t> new_feat_2_list_local;
    #pragma omp for schedule(monotonic : dynamic)
    for(auto it_it_feat_1 = feat_it_list_1.begin(); it_it_feat_1 != feat_it_list_1.end(); ++it_it_feat_1) {
      auto it_feat_1 = *it_it_feat_1;
      size_t idx_1 = it_feat_1-feat_list_0.begin();
      for (auto it_feat_2 = feat_list_0.begin(); it_feat_2 < it_feat_1; ++it_feat_2) {
        const node_ptr& feat_1 = *it_feat_1;
        const node_ptr& feat_2 = *it_feat_2;
        size_t idx_2 = it_feat_2-feat_list_0.begin();

        // If the input features are not of the same unit this operation is invalid
        if ((unit_list_0[idx_1] != unit_list_0[idx_2]) ||
            (feat_1->n_leaves() + feat_2->n_leaves() > max_leaves) ||
            ( node_type_list_0[idx_1] == NODE_TYPE::PARAM_ADD) ||
            ( node_type_list_0[idx_1] == NODE_TYPE::PARAM_SUB) || (node_type_list_0[idx_2] == NODE_TYPE::PARAM_ADD) ||
            ( node_type_list_0[idx_2] == NODE_TYPE::PARAM_SUB) ||
            ((node_type_list_0[idx_1] == NODE_TYPE::LOG) && (node_type_list_0[idx_2] == NODE_TYPE::LOG)))
        {
            continue;
        }

        // Check if the feature would simplify to a less complicated one
        std::map<std::string, int> add_sub_leaves;
        int expected_abs_tot = 0;
        feat_1->update_add_sub_leaves(add_sub_leaves, 1, expected_abs_tot);
        feat_2->update_add_sub_leaves(add_sub_leaves, -1, expected_abs_tot);

        int leaves_v_expected = std::accumulate(
            add_sub_leaves.begin(), add_sub_leaves.end(), -1 * expected_abs_tot, [](int tot, auto el) {
                return tot + std::abs(el.second);
            });
        if ((add_sub_leaves.size() < 2) || (std::abs(leaves_v_expected) != 0))
        {
            continue;
        }

        // Check if feature is a constant multiple of a previous feature
        int add_sub_tot_first = std::abs(add_sub_leaves.begin()->second);
        if ((std::abs(add_sub_tot_first) > 1) &&
            std::all_of(add_sub_leaves.begin(), add_sub_leaves.end(), [&add_sub_tot_first](auto el) {
                return std::abs(el.second) == add_sub_tot_first;
            }))
        {
            continue;
        }
        new_feat_1_list_local.push_back(idx_1);
        new_feat_2_list_local.push_back(idx_2);

      }
    }
    #pragma omp critical
    {
        new_feat_1_list.insert(new_feat_1_list.end(), new_feat_1_list_local.begin(), new_feat_1_list_local.end());
        new_feat_2_list.insert(new_feat_2_list.end(), new_feat_2_list_local.begin(), new_feat_2_list_local.end());
    }

    }
}
