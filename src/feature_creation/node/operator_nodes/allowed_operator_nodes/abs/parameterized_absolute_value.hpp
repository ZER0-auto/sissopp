// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/allowed_operator_nodes/abs/parameterized_absolute_value.hpp
 *  @brief Defines a class for the parameterized version of the absolute value operator
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This class represents the parameterized unary operator -> |A + a|
 */

#ifndef PARAM_ABS_VAL_NODE
#define PARAM_ABS_VAL_NODE

#include "feature_creation/node/operator_nodes/allowed_operator_nodes/abs/absolute_value.hpp"
#include "nl_opt/utils.hpp"

// DocString: cls_abs_param_node
/**
 * @brief Node for the parameterized absolute value operator
 *
 * @details Defines the operation |A + \\beta| (inherits from AbsNode)
 */
class AbsParamNode : public AbsNode
{
    friend class boost::serialization::access;

    /**
     * @brief Serialization function to send over MPI
     *
     * @param ar Archive representation of node
     */
    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar& boost::serialization::base_object<AbsNode>(*this);
        ar& _params;
    }

protected:
    using AbsNode::domain;
    using AbsNode::expr;
    using AbsNode::get_latex_expr;
    using AbsNode::matlab_fxn_expr;
    using AbsNode::set_test_value;
    using AbsNode::set_value;
    using AbsNode::test_value_ptr;
    using AbsNode::value_ptr;

    std::vector<double> _params;  //!< The vector containing the scale and bias terms for the Node

public:
    /**
     * @brief Base Constructor
     * @details This is only used for serialization
     */
    AbsParamNode();

    /**
     * @brief Constructor including bounds on the maximum absolute value of the Node
     *
     * @param feat (Node) shared_ptr of the feature to operate on (A)
     * @param feat_ind (int) Index of the new feature
     * @param l_bound (float) Minimum absolute value allowed for the feature.
     * @param u_bound (float) Maximum absolute value allowed for the feature.
     * @param optimizer (NLOptimizer) The optimizer to find the optimal parameters for the features
     */
    AbsParamNode(const node_ptr feat,
                 const unsigned long int feat_ind,
                 const double l_bound,
                 const double u_bound,
                 std::shared_ptr<NLOptimizer> optimizer);

    /**
     * @brief Constructor excluding bounds on the maximum absolute value of the Node
     *
     * @param feat (Node) shared_ptr of the feature to operate on (A)
     * @param feat_ind (int) Index of the new feature
     * @param optimizer (NLOptimizer) The optimizer to find the optimal parameters for the features
     */
    AbsParamNode(const node_ptr feat,
                 const unsigned long int feat_ind,
                 std::shared_ptr<NLOptimizer> optimizer);

    // DocString: abs_param_node_init
    /**
     * @brief Constructor including bounds on the maximum absolute value of the Node
     *
     * @param feat (Node) shared_ptr of the feature to operate on (A)
     * @param feat_ind (int) Index of the new feature
     * @param l_bound (float) Minimum absolute value allowed for the feature.
     * @param u_bound (float) Maximum absolute value allowed for the feature.
     */
    AbsParamNode(const node_ptr feat,
                 const unsigned long int feat_ind,
                 double const l_bound = 1e-50,
                 const double u_bound = 1e50);

    /**
     * @brief Constructor including bounds on the maximum absolute value of the Node
     *
     * @param feats arry to the shared_ptr of the features to operate on
     * @param feat_ind (int) Index of the new feature
     * @param params (std::vector<double>) The parameters for the node
     */
    AbsParamNode(std::array<node_ptr, 1> feats,
                 const unsigned long int feat_ind,
                 std::vector<double> params);

    /**
     * @brief Makes a hard copy node (All members of the Node are independent of the original one)
     * @return A shared_ptr to the copied node
     */
    node_ptr hard_copy() const override;

    // DocString: abs_param_node_set_value
    /**
     * @brief Set the value of all training samples for the feature inside the central data storage array
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     */
    inline void set_value(int offset = -1, const bool for_comp = false) const override
    {
        set_value(_params.data(), offset, for_comp);
    }

    // DocString: abs_param_node_set_test_value
    /**
     * @brief Set the value of all test samples for the feature inside the central data storage array
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     */
    inline void set_test_value(int offset = -1, const bool for_comp = false) const override
    {
        set_test_value(_params.data(), offset, for_comp);
    }

    /**
     * @brief Get the pointer to the feature's training data
     * @details If the feature is not already stored in memory, then calculate the feature and return the pointer to the data
     *
     * @param offset (int) the integer value to offset the location in the temporary storage array
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     *
     * @return A pointer to the feature's training data
     */
    inline double* value_ptr(int offset = -1, const bool for_comp = false) const override
    {
        return value_ptr(_params.data(), offset, for_comp);
    }

    /**
     * @brief Get the pointer to the feature's test data
     * @details If the feature is not already stored in memory, then calculate the feature and return the pointer to the data
     *
     * @param offset (int) the integer value to offset the location in the temporary storage array
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     *
     * @return pointer to the feature's test values
     */
    inline double* test_value_ptr(int offset = -1, const bool for_comp = false) const override
    {
        return test_value_ptr(_params.data(), offset, for_comp);
    }

    /**
     * @brief Returns the type of node this is
     */
    inline NODE_TYPE type() const override { return NODE_TYPE::PARAM_ABS; }

    // DocString: abs_param_node_expr
    /**
     * @brief A human readable equation representing the feature
     */
    inline std::string expr() const override { return expr(_params.data()); }

    // DocString: abs_param_node_get_latex_expr
    /**
     * @brief Get the valid LaTeX expression that represents the feature
     */
    inline std::string get_latex_expr() const override { return get_latex_expr(_params.data()); }

    // DocString: abs_param_node_matlab_expr
    /**
     * @brief Get the string that corresponds to the code needed to evaluate the node in matlab
     *
     * @return The matlab code for the feature
     */
    inline std::string matlab_fxn_expr() const override { return matlab_fxn_expr(_params.data()); }

    /**
     * @brief The parameters used for including individual scale and bias terms to each operator in the Node
     */
    inline std::vector<double> parameters() const override { return _params; }

    /**
     * @brief The pointer to the head of the parameters used for including individual scale and bias terms to each operator in the Node
     */
    virtual const double* param_pointer() const override { return _params.data(); }

    /**
     * @brief Optimize the scale and bias terms for each operation in the Node.
     * @details Use optimizer to find the scale and bias terms that minimizes the associated loss function
     *
     * @param optimizer The optimizer used to evaluate the loss function for each optimization and find the optimal parameters
     */
    void get_parameters(std::shared_ptr<NLOptimizer> optimizer) override;

    /**
     * @brief Set the non-linear parameters
     *
     * @param params The new parameters to use for the feature
     * @param check_sz If true make sure the number of parameters matches the expected value
     */
    inline void set_parameters(std::vector<double> params, bool check_sz = true) override
    {
        if (check_sz && (params.size() != static_cast<unsigned int>(n_params_possible())))
        {
            throw std::logic_error("Wrong number of parameters passed to set_parameters.");
        }
        _params = params;
    }

    /**
     * @brief Set the non-linear parameters
     * @param params The new scale and bias terms of this node
     */
    inline void set_parameters(const double* params) override
    {
        std::copy_n(params, _params.size(), _params.data());
    }

    // DocString: abs_param_node_n_params
    /**
     * @brief returns the number of actual parameters for this feature
     */
    inline int n_params() const override { return _params.size(); }

    /**
     * @brief Converts a feature into a postfix expression (reverse polish notation)
     *
     * @details Recursively creates a postfix representation of the string
     *
     * @param cur_expr The current expression
     * @param add_params Add parameters to the expression
     * @return The current postfix expression of the feature
     */
    void update_postfix(std::string& cur_expr, const bool add_params = true, const int depth = 1) const override;

    /**
     * @brief Get the domain of a feature
     *
     * @return The domain of the feature
     */
    inline Domain domain() const override { return domain(_params.data()); }
};

/**
 * @brief Attempt to generate a new parameterized absolute value node (|A + a|) and add it to feat_list
 *
 * @param feat_list list of features already generated
 * @param feat feature to attempt to take the absolute value of
 * @param feat_ind Index of the new feature
 * @param l_bound Minimum absolute value allowed for the feature.
 * @param u_bound Maximum absolute value allowed for the feature.
 * @param optimizer The optimizer used to find the parameters of the node
 */
void generateAbsParamNode(std::vector<node_ptr>& feat_list,
                          const node_ptr feat,
                          unsigned long int& feat_ind,
                          const double l_bound,
                          const double u_bound,
                          std::shared_ptr<NLOptimizer> optimizer);

#endif
