// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/allowed_operator_nodes/absolute_value.cpp
 *  @brief Implements a class for the absolute value operator
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This class represents the unary operator -> |A|
 */

#include "feature_creation/node/operator_nodes/allowed_operator_nodes/abs/absolute_value.hpp"
BOOST_SERIALIZATION_ASSUME_ABSTRACT(AbsNode)

void generateAbsNode(std::vector<node_ptr>& feat_list,
                     const node_ptr feat,
                     unsigned long int& feat_ind,
                     const double l_bound,
                     const double u_bound)
{
    // Absolute value of an absolute value is the same thing
    if ((feat->type() == NODE_TYPE::ABS) || (feat->type() == NODE_TYPE::ABS_DIFF) ||
        (feat->type() == NODE_TYPE::PARAM_ABS) || (feat->type() == NODE_TYPE::PARAM_ABS_DIFF))
    {
        return;
    }

    int offset = -1;
    double* val_ptr = feat->value_ptr(offset);
    // If the feature is strictly positive absolute values do nothing
    if (*std::min_element(val_ptr, val_ptr + feat->n_samp()) > 0.0)
    {
        return;
    }

    ++feat_ind;
    node_ptr new_feat = std::make_shared<AbsNode>(feat, feat_ind);
    val_ptr = new_feat->value_ptr();
    // Domain dom = new_feat->domain();

    // Check if the feature is NaN, constant, greater than the allowed max of less than the allowed min
    if (new_feat->is_const() ||
        std::any_of(
            val_ptr,
            val_ptr + new_feat->n_samp(),
            [&u_bound](double d) { return !std::isfinite(d) || (std::abs(d) > u_bound); }) ||
        (util_funcs::max_abs_val<double>(val_ptr, new_feat->n_samp()) < l_bound)
        // ((!dom.is_empty()) && (std::isnan(dom.end_points()[0]) || std::isnan(dom.end_points()[1])))
    )
    {
        return;
    }

    feat_list.push_back(new_feat);
}

AbsNode::AbsNode() {}

AbsNode::AbsNode(const node_ptr feat, const unsigned long int feat_ind)
    : OperatorNode({feat}, feat_ind)
{
}

AbsNode::AbsNode(std::array<node_ptr, 1> feats, const unsigned long int feat_ind)
    : OperatorNode(feats, feat_ind)
{
}

AbsNode::AbsNode(const node_ptr feat,
                 const unsigned long int feat_ind,
                 const double l_bound,
                 const double u_bound)
    : OperatorNode({feat}, feat_ind)
{
    // Absolute value of an absolute value is the same thing
    if ((feat->type() == NODE_TYPE::ABS) || (feat->type() == NODE_TYPE::ABS_DIFF) ||
        (feat->type() == NODE_TYPE::PARAM_ABS) || (feat->type() == NODE_TYPE::PARAM_ABS_DIFF))
    {
        throw InvalidFeatureException();
    }

    // If the feature is strictly positive absolute values do nothing
    double* val_ptr = feat->value_ptr(0);
    if (*std::min_element(val_ptr, val_ptr + _n_samp) > 0.0)
    {
        throw InvalidFeatureException();
    }

    set_value();

    // Domain dom = domain();
    // Check if the feature is constant, NaN, greater than the allowed max of less than the allowed min
    if (is_nan() || is_const() ||
        (util_funcs::max_abs_val<double>(value_ptr(), _n_samp) > u_bound) ||
        (util_funcs::max_abs_val<double>(value_ptr(), _n_samp) < l_bound)
        // ((!dom.is_empty()) && (std::isnan(dom.end_points()[0]) || std::isnan(dom.end_points()[1])))
    )
    {
        throw InvalidFeatureException();
    }
}

node_ptr AbsNode::hard_copy() const
{
    node_ptr cp = std::make_shared<AbsNode>(_feats[0]->hard_copy(), _feat_ind);
    cp->set_selected(_selected);
    cp->set_d_mat_ind(_d_mat_ind);
    return cp;
}

void AbsNode::update_add_sub_leaves(std::map<std::string, int>& add_sub_leaves,
                                    const int pl_mn,
                                    int& expected_abs_tot) const
{
    std::string key = expr();
    if (add_sub_leaves.count(key) > 0)
    {
        add_sub_leaves[key] += pl_mn;
    }
    else
    {
        add_sub_leaves[key] = pl_mn;
    }

    ++expected_abs_tot;
}

void AbsNode::update_div_mult_leaves(std::map<std::string, double>& div_mult_leaves,
                                     const double fact,
                                     double& expected_abs_tot) const
{
    std::string key = expr();
    if (div_mult_leaves.count(key) > 0)
    {
        div_mult_leaves[key] += fact;
    }
    else
    {
        div_mult_leaves[key] = fact;
    }

    expected_abs_tot += std::abs(fact);
}

void AbsNode::set_value(int offset, const bool for_comp) const
{
    double* val_ptr;
    if (_selected && (offset == -1))
    {
        offset += (offset == -1);
        val_ptr = node_value_arrs::get_d_matrix_ptr(_d_mat_ind);
    }
    else
    {
        offset += (offset == -1);
        val_ptr = node_value_arrs::get_value_ptr(_arr_ind, _feat_ind, rung(), offset, for_comp);
    }

    allowed_op_funcs::abs(_n_samp, _feats[0]->value_ptr(2 * offset, for_comp), 1.0, 0.0, val_ptr);
}

void AbsNode::set_test_value(int offset, const bool for_comp) const
{
    offset += (offset == -1);
    allowed_op_funcs::abs(
        _n_samp_test,
        _feats[0]->test_value_ptr(2 * offset, for_comp),
        1.0,
        0.0,
        node_value_arrs::get_test_value_ptr(_arr_ind, _feat_ind, rung(), offset, for_comp));
}
