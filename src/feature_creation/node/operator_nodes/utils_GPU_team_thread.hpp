
// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/utils_GPU_team_thread.hpp
 *  @brief inline helper utils for the team thread level of Kokkos implementation.
 *
 *  @author Yi Yao
 *  @bug No known bugs.
 *
 */


#ifndef UTILS_GPU_TEAM_THREAD
#define UTILS_GPU_TEAM_THREAD

template<typename PRECISION>
using ScratchViewType =  Kokkos::View< PRECISION*,
                  Kokkos::DefaultExecutionSpace::scratch_memory_space,
                  Kokkos::MemoryTraits<Kokkos::Unmanaged> >;

template<typename PRECISION>
KOKKOS_FORCEINLINE_FUNCTION void calculate_mean_team_thread(
                                const Kokkos::TeamPolicy<>::member_type& teamMember, 
                                const int& pos, 
                                const int& n_sample, 
                                ScratchViewType<PRECISION>& values_new_feats_tmp, 
                                PRECISION& mean) {
        Kokkos::parallel_reduce(Kokkos::TeamThreadRange( teamMember, pos, n_sample ), 
            [=] ( const size_t i_sample, PRECISION &innerUpdate ) {
            innerUpdate += values_new_feats_tmp(i_sample);
            }, mean );
        mean = mean / n_sample;
}

template<typename PRECISION>
KOKKOS_FORCEINLINE_FUNCTION void calculate_norm_team_thread(
                                const Kokkos::TeamPolicy<>::member_type& teamMember, 
                                const int& pos, 
                                const int& n_sample, 
                                ScratchViewType<PRECISION>& values_new_feats_tmp, 
                                PRECISION& norm) {
        Kokkos::parallel_reduce(Kokkos::TeamThreadRange( teamMember, pos, n_sample ), 
            [=] ( const size_t i_sample, PRECISION &innerUpdate ) {
            innerUpdate += values_new_feats_tmp(i_sample) * values_new_feats_tmp(i_sample);
            }, norm );
        norm = Kokkos::Experimental::sqrt(norm);
}

template<typename PRECISION>
KOKKOS_FORCEINLINE_FUNCTION void calculate_stand_dev_team_thread(
                                const Kokkos::TeamPolicy<>::member_type& teamMember, 
                                const int& pos,
                                const int& n_sample, 
                                ScratchViewType<PRECISION>& values_new_feats_tmp, 
                                PRECISION& mean,
                                PRECISION& stand_dev) {
        Kokkos::parallel_reduce(Kokkos::TeamThreadRange( teamMember, pos, n_sample ), 
            [=] ( const size_t i_sample, PRECISION &innerUpdate ) {
            innerUpdate += (values_new_feats_tmp(i_sample) - mean) * (values_new_feats_tmp(i_sample) - mean);
            }, stand_dev );
        stand_dev = Kokkos::Experimental::sqrt(stand_dev / n_sample);
}

template<typename PRECISION>
KOKKOS_FORCEINLINE_FUNCTION void check_const_team_thread(
                                const Kokkos::TeamPolicy<>::member_type& teamMember, 
                                const size_t& n_sample, 
                                ScratchViewType<PRECISION>& values_new_feats_tmp, 
                                const PRECISION& mean,
                                bool& is_const) {
        Kokkos::parallel_reduce(Kokkos::TeamThreadRange( teamMember, n_sample ), 
            [=] ( const size_t i_sample, bool &innerUpdate ) {
            innerUpdate = innerUpdate && (Kokkos::Experimental::abs(
             values_new_feats_tmp(i_sample) - mean) < 1e-12);
            }, Kokkos::LAnd<bool>(is_const) );
}

template<typename PRECISION>
KOKKOS_FORCEINLINE_FUNCTION void check_any_larger_than_u_bound_team_thread(
                                const Kokkos::TeamPolicy<>::member_type& teamMember, 
                                const size_t& n_sample, 
                                ScratchViewType<PRECISION>& values_new_feats_tmp, 
                                const PRECISION& u_bound,
                                bool& any_larger_than_u_bound) {
        Kokkos::parallel_reduce(Kokkos::TeamThreadRange( teamMember, n_sample ), 
            [=] ( const size_t i_sample, bool &innerUpdate ) {
            innerUpdate = innerUpdate || (Kokkos::Experimental::abs(
             values_new_feats_tmp(i_sample)) > u_bound);
            }, Kokkos::LOr<bool>(any_larger_than_u_bound) );
}

template<typename PRECISION>
KOKKOS_FORCEINLINE_FUNCTION void check_all_smaller_than_l_bound_team_thread(
                                const Kokkos::TeamPolicy<>::member_type& teamMember, 
                                const size_t& n_sample, 
                                ScratchViewType<PRECISION>& values_new_feats_tmp, 
                                const PRECISION& l_bound,
                                bool& all_smaller_than_l_bound) {
        Kokkos::parallel_reduce(Kokkos::TeamThreadRange( teamMember, n_sample ), 
            [=] ( const size_t i_sample, bool &innerUpdate ) {
            innerUpdate = innerUpdate && (Kokkos::Experimental::abs(
             values_new_feats_tmp(i_sample)) < l_bound);
            }, Kokkos::LAnd<bool>(all_smaller_than_l_bound) );
}

template<typename PRECISION>
KOKKOS_FORCEINLINE_FUNCTION void check_any_not_finite_team_thread(
                                const Kokkos::TeamPolicy<>::member_type& teamMember, 
                                const size_t& n_sample, 
                                ScratchViewType<PRECISION>& values_new_feats_tmp, 
                                bool& any_not_finite) {
        Kokkos::parallel_reduce( Kokkos::TeamThreadRange( teamMember, n_sample ), 
            [=] ( const size_t i_sample, bool &innerUpdate ) {
            innerUpdate = innerUpdate || !(Kokkos::Experimental::isfinite(
             values_new_feats_tmp(i_sample)));
            }, Kokkos::LOr<bool>(any_not_finite) );
}

template<typename PRECISION>
KOKKOS_FORCEINLINE_FUNCTION void check_all_positive_team_thread(
                                const Kokkos::TeamPolicy<>::member_type& teamMember, 
                                const size_t& n_sample, 
                                ScratchViewType<PRECISION>& values_new_feats_tmp, 
                                const PRECISION& l_bound,
                                bool& all_positive) {
        Kokkos::parallel_reduce( Kokkos::TeamThreadRange( teamMember, n_sample ), 
            [=] ( const size_t i_sample, bool &innerUpdate ) {
            innerUpdate = innerUpdate && (
             values_new_feats_tmp(i_sample) > l_bound);
            }, Kokkos::LAnd<bool>(all_positive) );
}

#endif