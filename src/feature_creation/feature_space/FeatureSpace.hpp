// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/feature_space/FeatureSpace.hpp
 *  @brief Defines the class for creating/operating on a feature space in SISSO
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef FEATURE_SPACE
#define FEATURE_SPACE

#include <boost/filesystem.hpp>
#include <utility>

#ifdef PROFILE_W_CALIPER
#include <caliper/cali.h>
#endif

#include <typeinfo>
#include <feature_creation/node/operator_nodes/allowed_ops.hpp>

#include "feature_creation/node/utils.hpp"
#include "inputs/InputParser.hpp"
#include "mpi_interface/MPI_Interface.hpp"
#include "mpi_interface/MPI_Ops.hpp"
#include "mpi_interface/serialize_tuple.h"
#include "openmp_reduction/ScoreObjectPair.hpp"
#include "openmp_reduction/openmp_reduction.hpp"
// #include "utils/project.hpp"
#include "projector/utils.hpp"

#ifdef PY_BINDINGS
namespace py = pybind11;
#endif


// DocString: cls_feat_space
/**
 * @brief Feature Space for SISSO calculations. It stores and performs all actions on the feature space for SISSO including SIS.
 */
class FeatureSpace
{
    // clang-format off
    std::vector<node_ptr> _phi_selected; //!< A vector containing all of the selected features
    std::vector<node_ptr> _phi; //!< A vector containing all features generated (Not including those created on the Fly during SIS)
    std::vector<node_ptr> _phi_0; //!< A vector containing all of the Primary features

#ifdef PARAMETERIZE
    std::vector<node_ptr> _phi_reparam; //!< A vector containing the features created when reparameterizating using the residuals
    std::vector<int> _end_no_params; //!< A vector containing the indexes of each rung where parameterized nodes start
    std::vector<int> _start_rung_reparam; //!< A vector containing the indexes where each rung starts in _phi_reparm

    std::vector<un_param_op_node_gen> _un_param_operators; //!< Vector containing all parameterized unary operators with free parameters
    std::vector<bin_param_op_node_gen> _com_bin_param_operators; //!< Vector containing all parameterized commutable binary operators with free parameters
    std::vector<bin_param_op_node_gen> _bin_param_operators; //!< Vector containing all parameterized binary operators with free parameters
    std::vector<std::string> _allowed_param_ops; //!< Vector containing all allowed operators strings for operators with free parameters
#endif

    std::vector<std::string> _allowed_ops; //!< Vector containing all allowed operators strings
    std::vector<un_op_node_gen> _un_operators; //!< Vector containing all unary operators
    std::vector<bin_op_node_gen> _com_bin_operators; //!< Vector containing all commutable binary operators
    std::vector<bin_op_node_gen> _bin_operators; //!< Vector containing all binary operators

    std::vector<double> _prop_train; //!< The value of the property vector for each training sample
    std::vector<double> _scores; //!< The projection scores for each feature

    const std::vector<int> _task_sizes_train; //!< Number of training samples per task
    std::vector<int> _start_rung; //!< Vector containing the indexes where each rung starts in _phi
    const std::string _project_type; //!< The type of Projector to use when projecting the features onto a property
    const std::string _feature_space_file; //!< File to output the computer readable representation of the selected features to
    const std::string _feature_space_summary_file; //!< File to output the human readable representation of the selected features to
    const std::string _phi_out_file; //!< Filename of the file to output the feature set to

    std::function<bool(const double*, const int, const double, const std::vector<double>&, const std::vector<int>&, const double, const int, const int)> _is_valid; //!< Function used to determine of a feature is too correlated to previously selected features
    std::function<int(const double*, const int, const double, const std::vector<node_sc_pair>&, const double)> _is_valid_feat_list; //!< Function used to determine of a feature is too correlated to previously selected features within a given list

    std::shared_ptr<MPI_Interface> _mpi_comm; //!< the MPI communicator for the calculation

    const double _cross_cor_max; //!< Maximum cross-correlation used for selecting features
    const double _l_bound; //!< The lower bound for the maximum absolute value of the features
    const double _u_bound; //!< The upper bound for the maximum absolute value of the features

    int _n_rung_store; //!< The number of rungs to calculate and store the value of the features for all samples
    unsigned long int _n_feat; //!< Total number of features in the feature space
    int _max_rung; //!< Maximum rung for the feature creation
    int _max_leaves; //!< The maximum number of primary features (with replacement) allowed in the features

    unsigned int _n_sis_select; //!< Number of features to select during each SIS iteration
    const int _n_samp_train; //!< Number of samples in the training set
    const int _n_rung_generate; //!< Either 0 or 1, and is the number of rungs to generate on the fly during SIS

    const bool _override_n_sis_select; //!< Override the n_sis_select if not enough features are present

    const bool _fix_intercept; //If true then the bias term is fixed at 0

    const bool _feature_generation_kokkos; //!< Use Kokkos optimized code for feature generation.
    const bool _feature_generation_kokkos_single_precision; //!< Use single precision version of the Kokkos code.
    const size_t _feature_generation_kokkos_proj_batch_size; //!< The batch size for the batched projection code with Kokkos.

    bool _verbose; //!< If true run the code without print statements

#ifdef PARAMETERIZE
    int _max_param_depth; //!< The maximum depth in the binary expression tree to set non-linear optimization
    bool _reparam_residual; //!< If True then reparameterize features using the residuals of each model
#endif
    // clang-format on
public:
    // DocString: feat_space_init
    /**
     * @brief Construct a FeatureSpace using an InputParser object
     *
     * @param inputs InputParser object used to build the FeatureSpace
     */
    FeatureSpace(InputParser inputs);

    // DocString: feat_space_init_file_py_list
    /**
     * @brief FeatureSpace constructor that uses a file containing postfix feature expressions to describe all features in Phi, and a primary feature setn <python/feature_creation/FeatureSpace.cpp>)
     *
     * @param feature_file (str) The file containing the postfix expressions of all features in the FeatureSpace
     * @param phi_0 (list of FeatureNode) The set of primary features
     * @param prop (list of float) List containing the property vector (training data only)
     * @param task_sizes_train (list of int) The number of samples in the training data per task
     * @param project_type (str) The type of projector to use
     * @param n_sis_select (int) The number of features to select during each SIS step
     * @param cross_corr_max (float) The maximum allowed cross-correlation value between selected features
     * @param override_n_sis_select If True then allow n_sis_select to be overridden during SIS
     * @param feature_generation_kokkos If True using Kokkos optimized feature generation code
     * @param feature_generation_kokkos_single_precision If True using single precision version.
     * @param feature_generation_kokkos_proj_batch_size The batch size for the feature generation projection w/ Kokkos.
     */
    FeatureSpace(std::string feature_file,
                 std::vector<node_ptr> phi_0,
                 std::vector<double> prop,
                 std::vector<int> task_sizes_train,
                 std::string project_type = "regression",
                 int n_sis_select = 1,
                 double cross_corr_max = 1.0,
                 std::vector<int> excluded_inds = std::vector<int>(),
                 bool override_n_sis_select = false,
                 bool restart=false,
                 bool fix_intercept=false,
                 bool feature_generation_kokkos = false,
                 bool feature_generation_kokkos_single_precision = false,
                 size_t feature_generation_kokkos_proj_batch_size = 50000000);

    /**
     * @brief FeatureSpace constructor used for unpickling
     *
     * @param phi_selected A vector containing all of the selected features
     * @param phi A vector containing all features generated (Not including those created on the Fly during SIS)
     * @param phi0 A vector containing all of the Primary features
     * @param allowed_ops Vector containing all allowed operators strings
     * @param allowed_param_ops Vector containing all allowed operators strings for operators with free parameters
     * @param prop_train The value of the property vector for each training sample
     * @param scores The projection scores for each feature
     * @param task_sizes_train Number of training samples per task
     * @param start_rung Vector containing the indexes where each rung starts in _phi
     * @param project_type The type of Projector to use when projecting the features onto a property
     * @param feature_space_file File to output the computer readable representation of the selected features to
     * @param feature_space_summary_file File to output the human readable representation of the selected features to
     * @param phi_out_file Filename of the file to output the feature set to
     * @param cross_cor_max Maximum cross-correlation used for selecting features
     * @param l_bound The lower bound for the maximum absolute value of the features
     * @param u_bound The upper bound for the maximum absolute value of the features
     * @param n_rung_store The number of rungs to calculate and store the value of the features for all samples
     * @param n_rung_generate Either 0 or 1, and is the number of rungs to generate on the fly during SIS
     * @param max_rung Maximum rung for the feature creation
     * @param n_sis_select Number of features to select during each SIS iteration
     * @param phi_reparam A vector containing the features created when reparameterizating using the residuals
     * @param end_no_params A vector containing the indexes of each rung where parameterized nodes start
     * @param start_rung_reparam A vector containing the indexes where each rung starts in _phi_reparm
     * @param max_param_depth The maximum depth in the binary expression tree to set non-linear optimization
     * @param reparam_residual If True then reparameterize features using the residuals of each model
     * @param override_n_sis_select If True then allow n_sis_select to be overridden during SIS
     * @param feature_generation_kokkos If True using Kokkos optimized feature generation code
     * @param feature_generation_kokkos_single_precision If True using single precision version.
     * @param feature_generation_kokkos_proj_batch_size The batch size for the feature generation projection w/ Kokkos.
     */
    FeatureSpace(std::vector<node_ptr> phi_selected,
                 std::vector<node_ptr> phi,
                 std::vector<node_ptr> phi0,
                 std::vector<std::string> allowed_ops,
                 std::vector<std::string> allowed_param_ops,
                 std::vector<double> prop_train,
                 std::vector<double> scores,
                 std::vector<int> task_sizes_train,
                 std::vector<int> start_rung,
                 std::string project_type,
                 std::string feature_space_file,
                 std::string feature_space_summary_file,
                 std::string phi_out_file,
                 double cross_cor_max,
                 double l_bound,
                 double u_bound,
                 int n_rung_store,
                 int n_rung_generate,
                 int max_rung,
                 int n_sis_select,
                 std::vector<node_ptr> phi_reparam,
                 std::vector<int> end_no_params,
                 std::vector<int> start_rung_reparam,
                 int max_param_depth,
                 bool reparam_residual,
                 bool override_n_sis_select = false,
                 bool fix_intercept=false,
                 bool feature_generation_kokkos = false,
                 bool feature_generation_kokkos_single_precision = false,
                 size_t feature_generation_kokkos_proj_batch_size = 50000000);

    /**
     * @brief Destructor
     */
    ~FeatureSpace();

    /**
     * @brief Initializes the centeral storage arrays
     */
    void initaialize_storage();

    /**
     * @brief Populate the operator lists using _allowed_ops and _allowed_param_ops
     */
    void set_op_lists();

    /**
     * @brief Create SIS output files and write their headers
     */
    void initialize_fs_output_files() const;

    /**
     * @brief Remove duplicate features from the feature space
     *
     * @param feat_set Feature space to remove the duplicates from
     * @param start The index to start the removal from
     */
    void remove_duplicate_features(std::vector<node_ptr>& feat_set, int start);

#ifdef PARAMETERIZE
    /**
     * @brief Reorder features based on the number of parameters they have (smallest to largest)
     *
     * @param feat_set Feature space to remove the duplicates from
     * @param start The index to start the removal from
     */
    int reorder_by_n_params(std::vector<node_ptr>& feat_set, int start);
#endif

    /**
     * @brief Populate _phi using _phi_0 and the allowed operators up to (_max_rung - _n_rung_generate)^th rung
     */
    void generate_feature_space(std::vector<node_ptr>& feat_set,
                                std::vector<int>& start_rung,
                                const std::vector<double>& prop,
                                bool reparam = false);

    /**
     * @brief Set the selected feature space for a restarted calculation
     *
     * @param phi_sel The selected features
     */
    void set_phi_selected(std::vector<node_ptr>& phi_sel);

    // DocString: feat_space_phi_selected_py
    /**
     * @brief A vector containing all of the selected features
     */
    inline std::vector<node_ptr> phi_selected() const { return _phi_selected; };

    // DocString: feat_space_phi_py
    /**
     * @brief A vector containing all features generated (Not including those created on the Fly during SIS)
     */
    inline std::vector<node_ptr> phi() const { return _phi; };

    // DocString: feat_space_phi0_py
    /**
     * @brief A vector containing all of the Primary features
     */
    inline std::vector<node_ptr> phi0() const { return _phi_0; };

    /**
     * @brief The projection scores for each feature in _phi
     */
    inline std::vector<double> scores() const { return _scores; }

    /**
     * @brief The MPI Communicator
     */
    inline std::shared_ptr<MPI_Interface> mpi_comm() const { return _mpi_comm; }

    // DocString: feat_space_task_sizes_train_py
    /**
     * @brief Number of training samples per task
     */
    inline std::vector<int> task_sizes_train() const { return _task_sizes_train; }

    // DocString: feat_space_allowed_ops_py
    /**
     * @brief The list of allowed operators
     */
    inline std::vector<std::string> allowed_ops() const { return _allowed_ops; }

    // DocString: feat_space_feature_space_file
    /**
     * @brief Filename of the file to output the computer readable representation of the selected features to
     */
    inline std::string feature_space_file() const { return _feature_space_file; }

    // DocString: feat_space_feature_space_summary_file
    /**
     * @brief Filename of the file to output the human readable representation of the selected features to
     */
    inline std::string feature_space_summary_file() const { return _feature_space_summary_file; }

    // DocString: feat_space_phi_out_file
    /**
     * @brief Filename of the file to output the feature set to
     */
    inline std::string phi_out_file() const { return _phi_out_file; }

    // DocString: feat_space_cross_cor_max
    /**
     * @brief Maximum cross-correlation used for selecting features
     */
    inline double cross_cor_max() const { return _cross_cor_max; }

    // DocString: feat_space_l_bound
    /**
     * @brief The mlower bound for the maximum absolute value of the features
     */
    inline double l_bound() const { return _l_bound; }

    // DocString: feat_space_u_bound
    /**
     * @brief The upper bound for the maximum absolute value of the features
     */
    inline double u_bound() const { return _u_bound; }

    // DocString: feat_space_max_rung
    /**
     * @brief The maximum rung for the feature creation
     */
    inline int max_rung() const { return _max_rung; }

    // DocString: feat_space_n_sis_select
    /**
     * @brief The number of features to select during each SIS iteration
     */
    inline unsigned int n_sis_select() const { return _n_sis_select; }

    // DocString: feat_space_n_samp_train
    /**
     * @brief The nuumber of samples in the training set
     */
    inline int n_samp_train() const { return _n_samp_train; }

    // DocString: feat_space_n_feat
    /**
     * @brief The total number of features in the feature space
     */
    inline unsigned long int n_feat() const { return _n_feat; }

    // DocString: feat_space_n_rung_store
    /**
     * @brief The number of rungs to calculate and store the value of the features for all samples
     */
    inline int n_rung_store() const { return _n_rung_store; }

    // DocString: feat_space_n_rung_generate
    /**
     * @brief Either 0 or 1, and is the number of rungs to generate on the fly during SIS
     */
    inline int n_rung_generate() const { return _n_rung_generate; }
    /**
     * @brief The type of projector to use
     */
    inline std::string project_type() const { return _project_type; }

    // DocString: feat_space_prop
    /**
     * @brief The value of the property vector for each training sample
     */
    inline std::vector<double> prop_train() const { return _prop_train; }

    // DocString: feat_space_max_leaves
    /**
     * @brief Return the maximum number of leaves (primary features with regression) allowed in a feature
     */
    inline int max_leaves() const { return _max_leaves; }

    // DocString: feat_space_fix_intercept
    /**
     * @brief True if the bias term is always 0.0
     */
    inline bool fix_intercept() const {return _fix_intercept;}

    /**
     * @brief Generate a new set of non-parameterized features from a single feature
     * @details Perform all valid algebraic operations on the passed feature and all features that appear before it in _phi.
     *
     * @param feat The feature to spawn new features from
     * @param feat_set The feature set to pull features from for binary operations
     * @param start The point in feat_set to begin pulling features from for binary operations
     * @param feat_ind starting index for the next feature generated
     * @param l_bound lower bound for the maximum absolute value of the feature
     * @param u_bound upper bound for the maximum abosulte value of the feature
     */
    void generate_non_param_feats(std::vector<node_ptr>::iterator& feat,
                                  std::vector<node_ptr>& feat_set,
                                  const std::vector<node_ptr>::iterator& start,
                                  unsigned long int& feat_ind,
                                  const double l_bound = 1e-50,
                                  const double u_bound = 1e50);

    /**
     * @brief Generate a new set of non-parameterized features from a single feature
     * @details Perform all valid algebraic operations on the passed feature and all features that appear before it in _phi.
     *
     * @param feat_set The feature set to pull features from for binary operations
     * @param feat_ind starting index for the next feature generated
     * @param l_bound lower bound for the maximum absolute value of the feature
     * @param u_bound upper bound for the maximum abosulte value of the feature
     * @param start The point in feat_set to begin pulling features from for binary operations
     * @param end The point in feat_set to end pulling features from for binary operations
     * @param inc The point in feat_set to inc pulling features from for binary operations
     */
#ifndef PARAMETERIZE
    template<typename PRECISION>
    void generate_non_param_feats_kokkos(std::vector<node_ptr>& feat_set_0,
                                         std::vector<node_ptr>& feat_set,
                                         unsigned long int& feat_ind,
                                         const PRECISION l_bound,
                                         const PRECISION u_bound,
                                         const size_t start,
                                         const size_t end,
                                         const size_t inc)
    {
        // The loop order is changed from feat1->op->feat2 to op->feat1->feat2;
        // In the Kokkos optimized code, the loop over feat1 and feat2 are in the
        // generateXXXNodeBatched file.
        // The Kokkos optimized code is used for mult, div, add, sub, absdiff nodes.
        //
        //CALI_CXX_MARK_FUNCTION;
#ifdef PROFILE_W_CALIPER
        CALI_MARK_BEGIN("generate_non_param_feats_kokkos");
#endif
            {
#ifdef PROFILE_W_CALIPER
                CALI_MARK_BEGIN("generate_non_param_feats_kokkos_un_op");
#endif
                for (auto& op : _un_operators)
                {
                    for (auto idx = start; idx < end; idx += inc)
                    {
                        auto feat_1 = feat_set_0.begin() + idx;
                            op(feat_set, *feat_1, feat_ind, l_bound, u_bound);
                    }
                }
                //// YY test
                //std::cout << "feat_set.size() after un: " << feat_set.size() << std::endl;
                //// YY test
#ifdef PROFILE_W_CALIPER
                CALI_MARK_END("generate_non_param_feats_kokkos_un_op");
#endif

#ifdef PROFILE_W_CALIPER
                CALI_MARK_BEGIN("generate_non_param_feats_kokkos_com_bin_op");
#endif
                for (auto& op : _com_bin_operators)
                {
                    std::vector<std::vector<node_ptr>::iterator> feat_list_1;
                    if (op_to_address::getOpAddress(op) == reinterpret_cast<size_t>(&generateMultNode) ||
                        op_to_address::getOpAddress(op) == reinterpret_cast<size_t>(&generateAddNode) ||
                        op_to_address::getOpAddress(op) == reinterpret_cast<size_t>(&generateSubNode) ||
                        op_to_address::getOpAddress(op) == reinterpret_cast<size_t>(&generateAbsDiffNode) )
                        {
                          for (auto idx = start; idx < end; idx += inc)
                          {
                              auto feat_1 = feat_set_0.begin() + idx;
                              feat_list_1.push_back(feat_1);
                          }
                        }
                    if (op_to_address::getOpAddress(op) == reinterpret_cast<size_t>(&generateMultNode)) {
#ifdef PROFILE_W_CALIPER
                        CALI_MARK_BEGIN("generate_non_param_feats_kokkos_mult");
#endif
                        generateMultNodeBatched<PRECISION>(feat_set, feat_list_1, feat_set_0, feat_ind, _max_leaves, l_bound, u_bound);
#ifdef PROFILE_W_CALIPER
                        CALI_MARK_END("generate_non_param_feats_kokkos_mult");
#endif
                    } else if (op_to_address::getOpAddress(op) == reinterpret_cast<size_t>(&generateAddNode)) {
#ifdef PROFILE_W_CALIPER
                        CALI_MARK_BEGIN("generate_non_param_feats_kokkos_add");
#endif
                        generateAddNodeBatched<PRECISION>(feat_set, feat_list_1, feat_set_0, feat_ind, _max_leaves, l_bound, u_bound);
#ifdef PROFILE_W_CALIPER
                        CALI_MARK_END("generate_non_param_feats_kokkos_add");
#endif
                    } else if (op_to_address::getOpAddress(op) == reinterpret_cast<size_t>(&generateSubNode)) {
#ifdef PROFILE_W_CALIPER
                        CALI_MARK_BEGIN("generate_non_param_feats_kokkos_sub");
#endif
                        generateSubNodeBatched<PRECISION>(feat_set, feat_list_1, feat_set_0, feat_ind, _max_leaves, l_bound, u_bound);
#ifdef PROFILE_W_CALIPER
                        CALI_MARK_END("generate_non_param_feats_kokkos_sub");
#endif
                    } else if (op_to_address::getOpAddress(op) == reinterpret_cast<size_t>(&generateAbsDiffNode)) {
#ifdef PROFILE_W_CALIPER
                        CALI_MARK_BEGIN("generate_non_param_feats_kokkos_absdiff");
#endif
                        generateAbsDiffNodeBatched<PRECISION>(feat_set, feat_list_1, feat_set_0, feat_ind, _max_leaves, l_bound, u_bound);
#ifdef PROFILE_W_CALIPER
                        CALI_MARK_END("generate_non_param_feats_kokkos_absdiff");
#endif
                    } else {
                        for (auto idx = start; idx < end; idx += inc)
                        {
                            auto feat_1 = feat_set_0.begin() + idx;

                            for (auto feat_2 = feat_set_0.begin(); feat_2 < feat_1; ++feat_2)
                            {
                                op(feat_set, *feat_1, *feat_2, feat_ind, _max_leaves, l_bound, u_bound);
                            }
                        }
                    }

                }
                //// YY test
                //std::cout << "feat_set.size() after com bin: " << feat_set.size() << std::endl;
                //// YY test
#ifdef PROFILE_W_CALIPER
                CALI_MARK_END("generate_non_param_feats_kokkos_com_bin_op");
#endif

#ifdef PROFILE_W_CALIPER
                CALI_MARK_BEGIN("generate_non_param_feats_kokkos_bin_op");
#endif
                for (auto& op : _bin_operators)
                {
                    if (op_to_address::getOpAddress(op) == reinterpret_cast<size_t>(&generateDivNode)) {
#ifdef PROFILE_W_CALIPER
                        CALI_MARK_BEGIN("generate_non_param_feats_kokkos_div");
#endif
                        std::vector<std::vector<node_ptr>::iterator> feat_list_1;
                        for (auto idx = start; idx < end; idx += inc)
                        {
                            auto feat_1 = feat_set_0.begin() + idx;
                            feat_list_1.push_back(feat_1);
                        }
                        generateDivNodeBatched<PRECISION>(feat_set, feat_list_1, feat_set_0, feat_ind, _max_leaves, l_bound, u_bound);
#ifdef PROFILE_W_CALIPER
                        CALI_MARK_END("generate_non_param_feats_kokkos_div");
#endif
                    } else {
                        for (auto idx = start; idx < end; idx += inc)
                        {
                        auto feat_1 = feat_set_0.begin() + idx;

                            for (auto feat_2 = feat_set_0.begin(); feat_2 < feat_1; ++feat_2)
                            {
                                op(feat_set, *feat_1, *feat_2, feat_ind, _max_leaves, l_bound, u_bound);
                                op(feat_set, *feat_2, *feat_1, feat_ind, _max_leaves, l_bound, u_bound);
                            }
                        }
                    }
                }
                //// YY test
                //std::cout << "feat_set.size() after bin: " << feat_set.size() << std::endl;
                //// YY test
#ifdef PROFILE_W_CALIPER
                CALI_MARK_END("generate_non_param_feats_kokkos_bin_op");
#endif
            }


#ifdef PROFILE_W_CALIPER
        CALI_MARK_END("generate_non_param_feats_kokkos");
#endif
    }
#endif

    // DocString: feat_space_output_phi
    /**
     * @brief Output the feature set to a file of a passed filename
     */
    void output_phi();

    // DocString: feat_space_start_rung_py
    /**
     * @brief A list containing the index of the first feature of each rung in the feature space.
     */
    std::vector<int> start_rung() const { return _start_rung; }

#ifdef PARAMETERIZE
    /**
     * @brief Generate a new set of parameterized features from a single feature
     * @details Perform all valid algebraic operations on the passed feature and all features that appear before it in _phi.
     *
     * @param feat The feature to spawn new features from
     * @param feat_set The feature set to pull features from for binary operations
     * @param start The point in feat_set to begin pulling features from for binary operations
     * @param feat_ind starting index for the next feature generated
     * @param optimizer The object used to optimize the parameterized features
     * @param l_bound lower bound for the maximum absolute value of the feature
     * @param u_bound upper bound for the maximum abosulte value of the feature
     */
    void generate_param_feats(std::vector<node_ptr>::iterator& feat,
                              std::vector<node_ptr>& feat_set,
                              const std::vector<node_ptr>::iterator& start,
                              unsigned long int& feat_ind,
                              std::shared_ptr<NLOptimizer> optimizer,
                              const double l_bound = 1e-50,
                              const double u_bound = 1e50);

    /**
     * @brief Generate a new set of parameterized features for the residuals
     *
     * @param feat The feature to spawn new features from
     * @param feat_set The feature set to pull features from for binary operations
     * @param feat_ind starting index for the next feature generated
     * @param optimizer The object used to optimize the parameterized features
     * @param l_bound lower bound for the maximum absolute value of the feature
     * @param u_bound upper bound for the maximum abosulte value of the feature
     */
    void generate_reparam_feats(std::vector<node_ptr>::iterator& feat,
                                std::vector<node_ptr>& feat_set,
                                unsigned long int& feat_ind,
                                std::shared_ptr<NLOptimizer> optimizer,
                                const double l_bound = 1e-50,
                                const double u_bound = 1e50);
#endif

    /**
     * @brief Generate the final rung of features on the fly and calculate their projection scores for SISat can be selected by SIS.
     *
     * @param projector The Projector used to project over all of the features
     * @param phi_selected The set of features that would be selected excluding the final rung
     * @param scores_selected The projection scores of all features in phi_selected
     * @param scores_selected_sort_inds The indexes of the sorted scores
     */
    void generate_and_project(std::shared_ptr<Projector> projector,
                              std::vector<node_sc_pair>& phi_sc_sel,
                              const std::vector<int>& scores_selected_sort_inds);
    /**
     * @brief Generate the final rung of features on the fly and calculate their projection scores for SISat can be selected by SIS.
     *
     * @param projector The Projector used to project over all of the features
     * @param phi_selected The set of features that would be selected excluding the final rung
     * @param scores_selected The projection scores of all features in phi_selected
     * @param scores_selected_sort_inds The indexes of the sorted scores
     */
    void generate_and_project_kokkos(std::shared_ptr<Projector> projector,
                              std::vector<node_sc_pair>& phi_sc_sel,
                              const std::vector<int>& scores_selected_sort_inds);


    // DocString: feat_space_sis_list
    /**
     * @brief Perform Sure-Independence Screening over the FeatureSpace. The features are ranked using a projection operator constructed using _project_type and the Property vector
     *
     * @param prop (list) Vector containing the property vector (training data only)
     */
    void sis(const std::vector<double>& prop);

    /**
     * @brief Perform Sure-Independence Screening over the FeatureSpace. The features are ranked using a projection operator defined in projector
     *
     * @param projector The Projector used to project over all of the features
     */
    void sis(std::shared_ptr<Projector> projector);

    // DocString: feat_space_feat_in_phi
    /**
     * @brief Is a feature in this process' _phi?
     *
     * @param ind (int) The index of the feature
     *
     * @return True if feature is in this rank's _phi
     */
    inline bool feat_in_phi(unsigned long int ind) const
    {
        return (ind >= _phi[0]->feat_ind()) && (ind <= _phi.back()->feat_ind());
    }

    // DocString: feat_space_remove_feature
    /**
     * @brief Remove a feature from phi
     *
     * @param ind (int) index of feature to remove
     */
    void remove_feature(const int ind);

#ifdef PARAMETERIZE
    // DocString: feat_space_param_feats_allowed
    /**
     * @brief True if built with -DBUILD_PARAMS (used for python tests)
     */
    bool parameterized_feats_allowed() const { return true; }
#else
    // DocString: feat_space_param_feats_allowed
    /**
     * @brief True if built with -DBUILD_PARAMS (used for python tests)
     */
    bool parameterized_feats_allowed() const { return false; }

#endif
    // Python Interface Functions
#ifdef PY_BINDINGS

    // DocString: feat_space_scores_py
    /**
     * @brief An array of all stored projection scores from SIS
     */
    inline py::array_t<double> scores_py() { return py::cast(_scores); };

    // DocString: feat_space_get_feature
    /**
     * @brief Access the feature in _phi with an index ind
     *
     * @param ind (int) The index of the feature to get
     * @return A ModelNode of the feature at index ind
     */
    inline ModelNode get_feature(const int ind) const { return ModelNode(_phi[ind]); }

#ifdef PARAMETERIZE
    // DocString: feat_space_allowed_param_ops_py
    /**
     * @brief The list of allowed operators with optimized free-parameters
     */
    inline std::vector<std::string> allowed_param_ops() const { return _allowed_param_ops; }

    /**
     * @brief A vector containing the features created when reparameterizating using the residuals
     */
    inline std::vector<node_ptr> phi_reparam() const { return _phi_reparam; }

    /**
     * @brief A vector containing the indexes of each rung where parameterized nodes start
     */
    inline std::vector<int> end_no_params() const { return _end_no_params; }

    /**
     * @brief A vector containing the indexes where each rung starts in _phi_reparm
     */
    inline std::vector<int> start_rung_reparam() const { return _start_rung_reparam; }

    /**
     * @brief The maximum depth in the binary expression tree to set non-linear optimization
     */
    inline int max_param_depth() const { return _max_param_depth; }

    /**
     * @brief If True then reparameterize features using the residuals of each model
     */
    inline bool reparam_residual() const { return _reparam_residual; }
#else
    // DocString: feat_space_allowed_ops_py
    /**
     * @brief The list of allowed operators
     */
    inline std::vector<std::string> allowed_param_ops() const
    {
        return std::vector<std::string>(0);
    }
#endif
#endif
};


#endif
