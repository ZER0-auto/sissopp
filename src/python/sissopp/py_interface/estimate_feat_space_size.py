# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Estimate the size of the final feature space given the input parameters for FeatureSpace

Members:

excluded_operations (dict): A map of ineligible operator pairings

Functions

get_max_number_feats: Get the maximum number of features for a given set of operators, primary features, and rung
get_estimate_n_feat_next_rung: Get an estimate of the number of new features generated in the next rung
"""

import numpy as np

excluded_operations = {
    "add": [],
    "sub": [],
    "abs_diff": [],
    "mult": ["inv"],
    "div": ["inv"],
    "abs": ["abd", "abs"],
    "inv": ["div", "inv", "exp", "nexp"],
    "exp": ["exp", "nexp", "log", "add", "sub"],
    "neg_exp": ["exp", "nexp", "log", "add", "sub"],
    "log": [
        "exp",
        "nexp",
        "log",
        "mult",
        "div",
        "inv",
        "sq",
        "cb",
        "sp",
        "sqrt",
        "cbrt",
    ],
    "cos": ["sin", "cos"],
    "sin": ["sin", "cos"],
    "cbrt": ["inv", "sq", "cb", "sp"],
    "sqrt": ["inv", "cbrt", "sq", "cb", "sp"],
    "sq": ["inv", "sqrt"],
    "cb": ["inv", "cbrt", "sq"],
    "six_pow": ["inv", "cbrt", "sqrt", "sq", "cb"],
}


def get_max_number_feats(ops, n_prim_feat, rung):
    """Get the maximum number of features for a given set of operators, primary features, and rung

    Args:
        ops (list of str): all operators in the system
        n_prim_feat (int): number of primary features
        rung(int): maximum rung for the features

    Returns:
        int: Maximum number of features possible
    """
    n_feat = n_prim_feat
    n_feat_prev_gen = n_prim_feat
    for rr in range(rung):
        new_feats = 0
        for op in ops:
            if op == "div":
                a = n_feat - n_feat_prev_gen
                b = n_feat_prev_gen
                new_feats += 2 * a * b + b * (b - 1) // 2
            elif (op == "add") or (op == "sub") or (op == "abs_diff") or (op == "mult"):
                new_feats += n_feat * n_feat_prev_gen // 2
            else:
                new_feats += n_feat_prev_gen
        n_feat += new_feats
        n_feat_prev_gen = new_feats
    return int(n_feat)


def get_estimate_n_feat_next_rung(ops, phi, start):
    """Get an estimate of the number of new features generated in the next rung

    Args:
        ops (list of str): All operators in the system
        phi (list of Node Objects): All features previously generated
        start (int): Start of the previous rung

    Returns:
        int: An estimate of the number of features generated this rung
    """
    phi_decomp = {}
    prev_rung_phi_decomp = {}
    for op in ops:
        if op == "abs_diff":
            phi_decomp["abd"] = {}
            prev_rung_phi_decomp["abd"] = {}
        elif op == "neg_exp":
            phi_decomp["nexp"] = {}
            prev_rung_phi_decomp["nexp"] = {}
        elif op == "six_pow":
            phi_decomp["sp"] = {}
            prev_rung_phi_decomp["sp"] = {}
        else:
            phi_decomp[op] = {}
            prev_rung_phi_decomp[op] = {}

    n_prim_feat = 0
    while (n_prim_feat < len(phi)) and (
        len(phi[n_prim_feat].postfix_expr.split("|")) == 1
    ):
        n_prim_feat += 1

    phi_decomp["feat"] = {}
    if start < n_prim_feat:
        prev_rung_phi_decomp["feat"] = {}

    for ff, feat in enumerate(phi):
        if len(feat.postfix_expr.split("|")) == 1:
            op = "feat"
        else:
            op = feat.postfix_expr.split("|")[-1]
        unit = str(feat.unit)
        if unit in phi_decomp[op]:
            phi_decomp[op][unit] += 1
        else:
            phi_decomp[op][unit] = 1

        if ff >= start:
            if unit in prev_rung_phi_decomp[op]:
                prev_rung_phi_decomp[op][unit] += 1
            else:
                prev_rung_phi_decomp[op][unit] = 1

    phi_unit_decomp = {}
    for val in phi_decomp.values():
        for key, n_feat in val.items():
            if key in phi_unit_decomp:
                phi_unit_decomp[key] += n_feat
            else:
                phi_unit_decomp[key] = n_feat

    phi_prev_rung_unit_decomp = {}
    for val in prev_rung_phi_decomp.values():
        for key, n_feat in val.items():
            if key in phi_prev_rung_unit_decomp:
                phi_prev_rung_unit_decomp[key] += n_feat
            else:
                phi_prev_rung_unit_decomp[key] = n_feat

    new_feats = 0
    for op in ops:
        if op in ["add", "sub", "abs_diff"]:
            for key in phi_prev_rung_unit_decomp.keys():
                a = phi_unit_decomp[key] - phi_prev_rung_unit_decomp[key]
                b = phi_prev_rung_unit_decomp[key]
                new_feats += a * b + b * (b - 1) / 2
        elif op in ["exp", "log", "sin", "cos", "neg_exp"]:
            new_feats += np.sum(
                [
                    prev_rung_phi_decomp[key][""]
                    if "" in prev_rung_phi_decomp[key]
                    else 0
                    for key in prev_rung_phi_decomp
                    if key not in excluded_operations[op]
                ]
            )
        elif op in ["inv", "abs", "cbrt", "sqrt", "cb", "sq", "six_pow"]:
            new_feats += np.sum(
                [
                    np.sum(list(prev_rung_phi_decomp[key].values()))
                    for key in prev_rung_phi_decomp
                    if key not in excluded_operations[op]
                ]
            )
        elif op == "mult":
            n_feat = np.sum(
                [
                    np.sum(list(phi_decomp[key].values()))
                    for key in phi_decomp
                    if key not in excluded_operations[op]
                ]
            )
            n_feat_prev_gen = np.sum(
                [
                    np.sum(list(prev_rung_phi_decomp[key].values()))
                    for key in prev_rung_phi_decomp
                    if key not in excluded_operations[op]
                ]
            )

            a = n_feat - n_feat_prev_gen
            b = n_feat_prev_gen

            new_feats += a * b + b * (b - 1) / 2

            if "div" in ops:
                n_div = np.sum(list(phi_decomp["div"].values()))
                n_div_prev_gen = np.sum(list(prev_rung_phi_decomp["div"].values()))

                a = n_div - n_div_prev_gen
                b = n_div_prev_gen
                new_feats -= a * b + b * (b - 1) / 2
        elif op == "div":
            n_feat = np.sum(
                [
                    np.sum(list(phi_decomp[key].values()))
                    for key in phi_decomp
                    if key not in excluded_operations[op]
                ]
            )
            n_feat_prev_gen = np.sum(
                [
                    np.sum(list(prev_rung_phi_decomp[key].values()))
                    for key in prev_rung_phi_decomp
                    if key not in excluded_operations[op]
                ]
            )
            n_div = np.sum(list(phi_decomp["div"].values()))
            n_div_prev_gen = np.sum(list(prev_rung_phi_decomp["div"].values()))

            a = n_feat - n_feat_prev_gen
            b = n_feat_prev_gen

            a_div = n_div - n_div_prev_gen
            b_div = n_div_prev_gen

            new_feats += 2 * a * b + b ** 2.0
            new_feats -= a_div * b + a * b_div
            new_feats -= b * b_div
            new_feats -= b - b_div
        else:
            raise ValueError(
                "Invliad operation passed to get_estimate_n_feat_next_rung"
            )
    return int(new_feats)
