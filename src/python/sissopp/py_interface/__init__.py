# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Provide a python interface to the SISSO++ Code

Create the relevant SISSO FeatureSpace and SISSOSolver classes from standard python objects.

Modules:

estimate_feat_space_size: Provide functions to estimate the final feature space size given the input parameters used to generate the FeatureSpace object.
get_solver: Functions to get the FeatureSpace and SISSOSolver objects needed to run the code.
import_dataframe: Functions used to convert a data.csv file into a primary feature space

Functions:

read_csv: Create initial feature set from a csv file
get_fs: Generate a FeatureSpace for the calculation
get_fs_solver: Generate a FeatureSpace and SISSOSolver for the calculation
"""
from sissopp.py_interface.import_dataframe import read_csv, create_inputs
from sissopp.py_interface.get_solver import get_fs_solver
