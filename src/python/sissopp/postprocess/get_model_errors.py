# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Get a numpy array of all training and validation error for a given set of models for each sample

Functions:

get_model_errors: Take in a set of models and populate a data array with the error for each sample
"""
import numpy as np
from sissopp.postprocess.load_models import get_models


def get_model_errors(models, ae=True):
    """Take in a set of models and populate a data array with the error for each sample

    Args:
        models (list of Model or str): The set of models to populate the data array with
        ae (bool): If True then use absolute error

    Returns:
        tuple: A tuple containing:
            - train_error (np.ndarray): The training error for each sample in each model (np.nan if the sample was in the validation set)
            - validation_error (np.ndarray): The validation error for each sample in each model (np.nan if the sample was in the training set)
    """
    if isinstance(models, str):
        models = get_models(models)

    train_error = np.zeros(
        (
            models[0][0].n_samp_train + models[0][0].n_samp_test,
            len(models[0]),
            len(models),
        )
    )
    validation_error = np.zeros(
        (
            models[0][0].n_samp_train + models[0][0].n_samp_test,
            len(models[0]),
            len(models),
        )
    )
    train_error[:, :, :] = np.nan
    validation_error[:, :, :] = np.nan
    all_inds = list(range(train_error.shape[0]))
    for m1, model_list in enumerate(models):
        for m2, model in enumerate(model_list):
            train_inds = np.delete(all_inds, model.leave_out_inds)
            validation_error[model.leave_out_inds, m2, m1] = model.test_error
            train_error[train_inds, m2, m1] = model.train_error

    if ae:
        train_error = np.abs(train_error)
        validation_error = np.abs(validation_error)

    return train_error, validation_error
