# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Check the convergence of cross-validation results

Functions:

jackknife_cv_conv_est: Get the jackknife variance of the CV test error
"""
import numpy as np
from glob import glob

from sissopp.postprocess.load_models import get_models


def jackknife_cv_conv_est(models):
    """Get the jackknife variance of the CV test error

    Args:
        models (str or list of Model): Models to evaluate

    Returns:
        tuple: A tuple containing:
            - avg_error (np.ndarray): The average rmse of the test error
            - variance (np.ndarray): The jackknife estimate of the variance of the test RMSE
    """
    if isinstance(models, str):
        models = get_models(models)

    n_test_samples = models[0][0].n_samp_test

    if n_test_samples == 0:
        raise ValueError("The models do not have any test data.")

    if any([model[0].n_samp_test != n_test_samples for model in models]):
        raise ValueError(
            "Not all of the models have the same number of testing samples."
        )

    n_dim = np.max([model_list[0].n_dim for model_list in models])
    test_rmse = np.array([model.test_rmse for model in models.flatten()]).reshape(
        n_dim, -1
    )
    x_bar_i = []
    for dim_error in test_rmse:
        x_bar_i.append([])
        for ii in range(len(dim_error)):
            x_bar_i[-1].append(np.delete(dim_error, ii).mean())

    x_bar_i = np.array(x_bar_i)
    avg_error = x_bar_i.mean(axis=1)
    variance = (
        (test_rmse.shape[1] - 1.0)
        / test_rmse.shape[1]
        * np.sum((x_bar_i - avg_error.reshape(n_dim, 1)) ** 2.0, axis=1)
    )
    return avg_error, variance
