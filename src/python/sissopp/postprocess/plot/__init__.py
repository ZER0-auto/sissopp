# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Create various plots of the SISSO models

Modules:

config: Configure plots into a standardized format
cv_error_plot: Visualize the distribution of errors from Cross-Validation
maps: Create a 2D map of the model over two selected features
parity_plot: Create a parity plot for a model
utils: Utility functions used for generating plots
"""
from sissopp.postprocess.plot.config import *
