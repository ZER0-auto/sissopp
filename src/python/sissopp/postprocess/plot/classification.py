import numpy as np
from scipy.spatial import ConvexHull
import toml

from sissopp import ModelClassifier
from sissopp.postprocess.load_models import load_model
from sissopp.postprocess.plot import plt, config
from sissopp.postprocess.plot.utils import setup_plot_ax


def plot_classification(model, task=0, filename=None, fig_settings=None):
    """Plots a 2D classification map from a model

    Args:
        model (str, tuple, ModelClassifier): The model to plot
        task (int): The task to visualize
        filename (str): Name of the file to store the plot in
        fig_settings (dict): non-default plot settings

    Returns:
        matplotlib.pyplot.Figure: A plot visualizing the classification scheme
    """
    if isinstance(model, str):
        model = load_model(model)
    elif isinstance(model, tuple) or isinstance(model, tuple):
        assert len(model) == 2
        model = load_model(model[0], model[1])

    assert isinstance(model, ModelClassifier)

    if len(model.feats) != 2:
        raise ValueError("Plotting only works for 2D models")

    classes = np.unique(model.prop_train)
    verts = []
    points = []

    task_start = np.sum(model.task_sizes_train[:task], dtype=int)
    task_end = task_start + model.task_sizes_train[task]
    for c in classes:
        inds = np.where(model.prop_train[task_start:task_end] == c)[0] + task_start
        pts = np.zeros((len(inds), len(model.feats)))
        for ff, feat in enumerate(model.feats):
            pts[:, ff] = feat.value[inds]

        hull = ConvexHull(pts)
        verts.append(
            np.concatenate((pts[hull.vertices], pts[hull.vertices[0]].reshape((1, 2))))
        )
        points.append(pts)

    if isinstance(fig_settings, str):
        fig_settings = toml.load(fig_settings)

    fig_config, fig, ax = setup_plot_ax(fig_settings)
    colors = fig_config["classification"]["colors"]
    markers = fig_config["classification"]["markers"]

    ax.set_xlabel(model.feats[0].latex_expr)
    ax.set_ylabel(model.feats[1].latex_expr)
    for vv, v in enumerate(verts):
        ax.fill_between(
            v[:, 0], v[:, 1], color=colors[vv % len(colors)], alpha=0.2, linewidth=0.0
        )

    for pp, p in enumerate(points):
        ax.plot(
            p[:, 0],
            p[:, 1],
            color=colors[pp % len(colors)],
            marker=markers[pp % len(markers)],
            linewidth=0.0,
        )

    x_lims = ax.get_xlim()
    ax.set_xlim(x_lims)
    ax.set_ylim(ax.get_ylim())

    x_pts = np.linspace(x_lims[0], x_lims[1], 101)
    class_comb = len(classes) * (len(classes) - 1) // 2
    for coef in model.coefs[task * class_comb : (task + 1) * class_comb]:
        plt.plot(x_pts, -1 * (coef[0] * x_pts + coef[-1]) / coef[1], "k--")

    if filename:
        fig.savefig(filename)

    return fig
