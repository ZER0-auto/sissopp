# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Create a 2D map of the model over two selected features

Functions:

plot_2d_map: Plot a 2D map of a model (2 selected features)
"""
import pandas as pd
import numpy as np

from sissopp.postprocess.load_models import load_model
from sissopp.postprocess.plot.utils import setup_plot_ax
from sissopp.py_interface.import_dataframe import strip_units


def plot_2d_map(
    model,
    df,
    feats,
    feat_bounds=None,
    index=None,
    data_filename=None,
    filename=None,
    fig_settings=None,
    n_points=1001,
    levels=32,
    cmap="PuBu",
    vmin=None,
    vmax=None,
    contour_lc=None,
    fig=None,
    ax=None,
    colorbar=True,
):
    """Plot a 2D map of a model (2 selected features)

    Args:
        model (Model): Model to plot the map of
        df (pd.DataFrame): Dataframe from data.csv file
        feats (list of str): List of 2 features to plot
        index (str): row index to take default values for features not being plotted (from df, default is avg)
        data_filename (str): Filename to store the data in
        filename (str): Filename for the figure
        fig_settings (dict): Settings used to augment the plot
        n_points (int): Number of points to plot
        levels (int): Number of levels to plot for the contour plot
        cmap (matplotlib.pyplot.Colormap): Colormap to use for the figure
        vmin (float): Minimum value of the colorbar axis
        vmax (float): Maximum value of the colorbar axis
        contour_lc (None or str): Color of the contour lines (if None then don't use them)
        fig (matplotlib.pyplot.Figure): The matplotlib Figure object
        ax (matplotlib.pyplot.Axis): The matplotlib axis for the plot
        colorbar (bool): If True add a colorbar
    Returns:
        tuple: A tuple containing:
            - fig (matplotlib.pyplot.Figure): The pyplot Figure for the plot
            - axes (matplotlib.pyplot.Axis): The pyplot axis for the plot
            - colorbar (matplotlib.pyplot.Colorbar): The pyplot colorbar for the plot
    """
    if len(feats) != 2:
        raise ValueError("feats must be of length 2")

    df = strip_units(df)

    if index and index not in df.index:
        raise ValueError("Requested index not in the passed DataFrame")

    if any([feat not in df.columns for feat in feats]):
        raise ValueError("One of the requested features is not in the df column list")

    if isinstance(model, str):
        model = load_model(model)

    if isinstance(model, ModelClassifier):
        raise ValueError("2D maps are designed for regression type plots")

    if fig is None:
        fig_config, fig, ax = setup_plot_ax(fig_settings)
        fig.subplots_adjust(right=0.85)
    elif ax is None:
        ax = fig.get_axes()[0]

    ax.set_xlabel(feats[0].replace("_", " "))
    ax.set_ylabel(feats[1].replace("_", " "))

    if not feat_bounds:
        x = np.linspace(df[feats[0]].values.min(), df[feats[0]].values.max(), n_points)
        y = np.linspace(df[feats[1]].values.min(), df[feats[1]].values.max(), n_points)
    else:
        x = np.linspace(feat_bounds[0][0], feat_bounds[0][-1], n_points)
        y = np.linspace(feat_bounds[1][0], feat_bounds[1][-1], n_points)

    xx, yy = np.meshgrid(x, y)

    xx = xx.flatten()
    yy = yy.flatten()

    data_dict = {}
    for feat in np.unique(
        [x_in_expr for feat in model.feats for x_in_expr in feat.x_in_expr_list]
    ):
        if index is None:
            data_dict[feat] = np.ones(n_points ** 2) * df[feat].values.mean()
        else:
            data_dict[feat] = np.ones(n_points ** 2) * df.loc[index, feat]

    data_dict[feats[0]] = xx
    data_dict[feats[1]] = yy
    zz = model.eval_many(data_dict)

    if data_filename:
        np.savetxt(data_filename, np.column_stack((xx, yy, zz)))

    if vmin and vmax:
        levels = np.linspace(vmin, vmax, levels)
    cnt = ax.contourf(
        x,
        y,
        zz.reshape((n_points, n_points)),
        cmap=cmap,
        levels=levels,
        vmin=vmin,
        vmax=vmax,
    )
    if contour_lc:
        for c in cnt.collections:
            c.set_edgecolor(contour_lc)
    else:
        for c in cnt.collections:
            c.set_edgecolor("face")

    ax.set_xlim([xx[0], xx[-1]])
    ax.set_ylim([yy[0], yy[-1]])

    ax.tick_params(direction="in", which="both", right=True, top=True)

    divider = make_axes_locatable(ax)

    if colorbar:
        cax = divider.append_axes("right", size="5%", pad=0.15)
        cbar = fig.colorbar(cnt, cax=cax, orientation="vertical")
        cbar.ax.tick_params(axis="y", direction="in", right=True)

    if filename:
        fig.savefig(filename)

    if colorbar:
        return fig, ax, cbar
    else:
        return fig, ax, None
