# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Create a parity plot for a model

Functions:

plot_model_parity_plot: Wrapper to plot_model for a set of training and testing data
"""
import numpy as np
import toml
from sissopp.postprocess.check_cv_convergence import jackknife_cv_conv_est
from sissopp.postprocess.load_models import load_model
from sissopp.postprocess.plot.utils import setup_plot_ax, latexify
from sissopp import ModelClassifier


def plot_model_parity_plot(model, filename=None, fig_settings=None):
    """Wrapper to plot_model for a set of training and testing data

    Args:
        model (Model, str, or tuple): The model to plot
        filename (str): Name of the file to store the plot in
        fig_settings (dict): non-default plot settings

    Returns:
        matplotlib.pyplot.Figure: The machine learning plot of the given model
    """
    if isinstance(model, str):
        model = load_model(model)
    elif isinstance(model, tuple) or isinstance(model, tuple):
        assert len(model) == 2
        model = load_model(model[0], model[1])

    if isinstance(model, ModelClassifier):
        raise ValueError(
            "Machine learning plots are designed for regression type plots"
        )

    if isinstance(fig_settings, str):
        fig_settings = toml.load(fig_settings)

    fig_config, fig, ax = setup_plot_ax(fig_settings)

    ax.set_xlabel(latexify(model.prop_label) + " (" + model.prop_unit.latex_str + ")")
    ax.set_ylabel(
        f"Estimated {latexify(model.prop_label)}"
        + " ("
        + model.prop_unit.latex_str
        + ")"
    )
    if len(model.prop_test) > 0:
        lims = [
            min(
                min(model.prop_train.min(), model.prop_test.min()),
                min(model.fit.min(), model.predict.min()),
            ),
            max(
                max(model.prop_train.max(), model.prop_test.max()),
                max(model.fit.max(), model.predict.max()),
            ),
        ]
    else:
        lims = [
            min(model.prop_train.min(), model.fit.min()),
            max(model.prop_train.max(), model.fit.max()),
        ]
    lim_range = lims[1] - lims[0]
    lims[0] -= lim_range * 0.1
    lims[1] += lim_range * 0.1

    ax.set_xlim(lims)
    ax.set_ylim(lims)

    xx = ax.plot(lims, lims, "--", color="#555555")
    error_label = f"Training $R^2$: {model.r2: .2f}"
    ax.plot(
        model.prop_train,
        model.fit,
        "o",
        color=fig_config["colors"]["box_train_face"],
        label="train",
    )
    if len(model.prop_test) > 0:
        ax.plot(
            model.prop_test,
            model.predict,
            "s",
            color=fig_config["colors"]["box_test_face"],
            label="test",
        )
        ax.legend(loc="upper left")
        error_label += f"\nTesting $R^2$: {model.test_r2 :.2f}"

    ax.text(
        0.99,
        0.01,
        error_label,
        horizontalalignment="right",
        verticalalignment="bottom",
        transform=ax.transAxes,
    )
    if filename:
        fig.savefig(filename)

    return fig
