# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Analyze the prevalence of the primary features in the selected feature space

Functions:

get_prevelance_of_primary_features: Get the prevalence of features inside of phi_selected
"""

import numpy as np

from sissopp import phi_selected_from_file


def get_prevelance_of_primary_features(sisso_file, phi_0):
    """Get the prevalence of features inside of phi_selected

    Args:
        sisso_file (str): The selected feature file
        phi_0 (list): The primary feature list

    Returns:
        dict (key=str, val=double): Fraction each primary feature appears in the selected features
    """
    phi_selected = phi_selected_from_file(sisso_file, phi_0)
    phi_0_in_phi_sel = {}
    for feat in phi_0:
        phi_0_in_phi_sel[str(feat)] = 0.0

    for feat in phi_selected:
        for key in np.unique(feat.primary_feat_decomp.keys()):
            phi_0_in_phi_sel[str(phi_0[key])] += 1.0 / len(phi_selected)
    return phi_0_in_phi_sel
