// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file python/py_bindings_cpp_def/feature_creation/FeatureSoace.cpp
 *  @brief Implements the python based functionality of the class used to transfer features (Nodes) to the Model objects
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "feature_creation/node/ModelNode.hpp"

py::array_t<double> ModelNode::eval_many_py(py::array_t<double> x_in)
{
    std::vector<std::vector<double>> x_in_vec = python_conv_utils::from_2D_array_transpose<double>(
        x_in);
    return py::cast(eval(x_in_vec));
}

py::array_t<double> ModelNode::eval_many_py(std::map<std::string, py::array_t<double>> x_in)
{
    std::map<std::string, std::vector<double>>
        x = python_conv_utils::convert_map<std::string, double>(x_in);
    return py::cast(eval(x));
}
