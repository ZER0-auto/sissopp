// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @brief Defines the class that uses a Pearson correlation projection operator and a least-squares regression objective function
 *
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include <Kokkos_Core.hpp>
#include <type_traits>

#include "loss_function/LossFunction.hpp"
#include "utils/DescriptorMatrix.hpp"
#include "utils/PropertiesVector.hpp"
#include "utils/assumption.hpp"
#include "utils/cuda.hpp"
#include "utils/mkl_interface.hpp"

// DocString: cls_loss_function_pearson_rmse
/**
 * @brief The loss function used for regression problems on GPU
 *
 */
template <typename DEVICE, typename PRECISION = float>
class RMSECPU
{
public:
    using MemorySpace = typename DEVICE::memory_space;
    using ExecutionSpace = typename DEVICE::execution_space;
    using BatchedMatrix = Kokkos::View<PRECISION***, Kokkos::LayoutLeft, MemorySpace>;
    using BatchedVector = Kokkos::View<PRECISION**, Kokkos::LayoutLeft, MemorySpace>;
    using BatchedScalar = Kokkos::View<PRECISION*, Kokkos::LayoutLeft, MemorySpace>;
    using Matrix = Kokkos::View<PRECISION**, Kokkos::LayoutLeft, MemorySpace>;
    using Vector = Kokkos::View<PRECISION*, Kokkos::LayoutLeft, MemorySpace>;
    using Scalar = Kokkos::View<PRECISION, Kokkos::LayoutLeft, MemorySpace>;
    using ModelBatch = Kokkos::View<int**, Kokkos::LayoutLeft, MemorySpace>;
    using TaskSizes = Kokkos::View<int*, Kokkos::LayoutLeft, MemorySpace>;

    struct WrappedPointer
    {
        PRECISION* ptr;
    };

private:
    Matrix _descriptor_matrix;
    Vector _properties;
    std::vector<int> _task_sizes;

    bool _fix_intercept;  //!< If true then the bias term is fixed at 0
    int _n_feat;          //!< Number features in the linear model
    int _n_dim;           //!< Total number of constants to fit (scale and bias terms)
    int _n_samp;          //!< Number of samples in the training set
    int _n_task;          //!< Number of tasks
    int _max_batches;

    /// dim 0: material samples
    /// dim 1: features
    /// dim 2: batch
    BatchedMatrix _a;
    /// dim 0: material properties
    /// dim 1: batch
    BatchedVector _b;
    BatchedScalar _batched_scores;
    Kokkos::View<int**, Kokkos::LayoutLeft, MemorySpace> _models;

    /// The estimated value of the property to evaluate the loss function against for the training set
    BatchedVector _estimated_training_properties;

    Kokkos::View<WrappedPointer*, MemorySpace> _batched_As;
    Kokkos::View<WrappedPointer*, MemorySpace> _batched_bs;

    static constexpr int lwork = 256;
    std::vector<PRECISION> work = std::vector<PRECISION>(lwork);

    void gels(const char trans,
              const int m,
              const int n,
              const int nrhs,
              const float* a,
              const int lda,
              const float* b,
              const int ldb,
              const float* work_fxn_var,
              const int lwork_fxn_var,
              const int* info)
    {
        ::sgels_(&trans, &m, &n, &nrhs, a, &lda, b, &ldb, work_fxn_var, &lwork_fxn_var, info);
    }
    void gels(const char trans,
              const int m,
              const int n,
              const int nrhs,
              const double* a,
              const int lda,
              const double* b,
              const int ldb,
              const double* work_fxn_var,
              const int lwork_fxn_var,
              const int* info)
    {
        ::dgels_(&trans, &m, &n, &nrhs, a, &lda, b, &ldb, work_fxn_var, &lwork_fxn_var, info);
    }

    void gemv(const char trans,
              const int m,
              const int n,
              const float alpha,
              const float* a,
              const int lda,
              const float* x,
              const int incx,
              const float beta,
              float* y,
              const int incy)
    {
        ::sgemv_(&trans, &m, &n, &alpha, a, &lda, x, &incx, &beta, y, &incy);
    }
    void gemv(const char trans,
              const int m,
              const int n,
              const double alpha,
              const double* a,
              const int lda,
              const double* x,
              const int incx,
              const double beta,
              double* y,
              const int incy)
    {
        ::dgemv_(&trans, &m, &n, &alpha, a, &lda, x, &incx, &beta, y, &incy);
    }

public:
    /**
     * @brief Constructor
     *
     * @param descriptor_matrix descriptor matrix
     * @param properties properties vector
     * @param task_sizes number of items per task
     * @param fix_intercept use a fixed offset?
     * @param n_feat Number features in the linear model
     */
    RMSECPU(const int max_batches,
            const Matrix& descriptor_matrix,
            const Vector& properties,
            const std::vector<int>& task_sizes,
            bool fix_intercept = false,
            int n_feat = 1);

    RMSECPU(const RMSECPU& rmse) = delete;
    RMSECPU& operator=(const RMSECPU& rmse) = delete;

    ~RMSECPU();

    /**
     * @brief Evaluate the loss function for a set of features
     *
     * @param feature_indices index tuples pointing into the descriptor matrix
     * @return Final score for every index tuple
     */
    Vector operator()(const Kokkos::View<int**, Kokkos::LayoutLeft, MemorySpace>& feature_indices,
                      int64_t num_models);

    /**
     * @brief Set the A matrix used for solving the least squares regression
     *
     * @param models index tuples
     * @param taskind The task used for the least squares regression
     * @param start The offset needed from the head of the feature's training data to where the task starts
     * @param batch_size number of systems to solve simultaneously
     */
    void set_a(
        const Kokkos::View<const int**, Kokkos::LayoutLeft, typename DEVICE::memory_space>& models,
        int taskind,
        int start,
        int batch_size);

    /**
     * @brief Set the right hand side of the least square systems
     *
     * @param taskind The task used for the least squares regression
     * @param start The offset needed from the head of the feature's training data to where the task starts
     * @param batch_size number of systems to solve simultaneously
     */
    void set_b(int taskind, int start, int batch_size);

    /**
     * @brief Calculate estimated properties
     *
     * @param estimated_training_properties estimated properties
     * @param taskind The task for used for the least squares regression
     * @param start The offset needed from the head of the feature's training data to where the task starts
     * @param batch_size number of systems to solve simultaneously
     */
    void set_prop_train_est(BatchedVector estimated_training_properties,
                            int taskind,
                            int start,
                            int batch_size);

    /**
     * @brief Perform least squares regression
     *
     * @param taskind The task for used for the least squares regression
     * @param start The offset needed from the head of the feature's test data to where the task starts
     * @return info The final info value from dgels
     */
    int least_squares(int taskind, int start, int batch_size);

    void get_mean_squared_difference(const Vector& output,
                                     const Vector& input1,
                                     const Matrix& input2,
                                     const int64_t batch_offset,
                                     const int64_t batch_size);
};

template <typename DEVICE, typename PRECISION>
RMSECPU<DEVICE, PRECISION>::RMSECPU(const int max_batches,
                                    const Matrix& descriptor_matrix,
                                    const Vector& properties,
                                    const std::vector<int>& task_sizes,
                                    bool fix_intercept,
                                    int n_feat)
    : _descriptor_matrix(descriptor_matrix),
      _properties(properties),
      _task_sizes(task_sizes),
      _fix_intercept(fix_intercept),
      _n_feat(n_feat),
      _n_dim(n_feat + (!fix_intercept)),
      _n_samp(std::accumulate(task_sizes.begin(), task_sizes.end(), 0)),
      _n_task(task_sizes.size()),
      _max_batches(max_batches),
      _a("A", _n_samp, _n_dim, max_batches),
      _b("b", _n_samp, max_batches),
      _batched_scores("batched_scores", max_batches),
      _models("models", n_feat, max_batches),
      _estimated_training_properties(
          "estimated_training_properties", properties.extent(0), max_batches),
      _batched_As("batched_As", max_batches),
      _batched_bs("batched_bs", max_batches)
{
    for (size_t batch_idx = 0; batch_idx < _max_batches; ++batch_idx)
    {
        _batched_As(batch_idx).ptr = &_a(0, 0, batch_idx);
        _batched_bs(batch_idx).ptr = &_b(0, batch_idx);
    }
}

template <typename DEVICE, typename PRECISION>
RMSECPU<DEVICE, PRECISION>::~RMSECPU()
{
}

template <typename DEVICE, typename PRECISION>
typename RMSECPU<DEVICE, PRECISION>::Vector RMSECPU<DEVICE, PRECISION>::operator()(
    const Kokkos::View<int**, Kokkos::LayoutLeft, MemorySpace>& feature_indices, int64_t num_models)
{
    const int input_batch_size = static_cast<int>(num_models);
    if (_batched_scores.extent(0) < input_batch_size)
        Kokkos::resize(_batched_scores, input_batch_size);
    int batch_offset = 0;

    while (batch_offset < input_batch_size)
    {
        auto processed_batch_size = std::min(_max_batches, input_batch_size - batch_offset);

        CHECK_GREATER_EQUAL(_models.extent(1), processed_batch_size, "");
        CHECK_EQUAL(feature_indices.extent(0), _models.extent(0), "");
        for (int model_idx = 0; model_idx < processed_batch_size; ++model_idx)
        {
            for (int ff = 0; ff < feature_indices.extent(0); ++ff)
            {
                _models(ff, model_idx) = feature_indices(ff, batch_offset + model_idx);
            }
        }

        int start = 0;
        for (int task_idx = 0; task_idx < _n_task; ++task_idx)
        {
            set_a(_models, task_idx, start, processed_batch_size);
            set_b(task_idx, start, processed_batch_size);
            least_squares(task_idx, start, processed_batch_size);

            set_a(_models, task_idx, start, processed_batch_size);
            set_prop_train_est(
                _estimated_training_properties, task_idx, start, processed_batch_size);

            start += _task_sizes[task_idx];
        }

        get_mean_squared_difference(_batched_scores,
                                    _properties,
                                    _estimated_training_properties,
                                    batch_offset,
                                    processed_batch_size);
        batch_offset += processed_batch_size;
    }

    return _batched_scores;
}

template <typename DEVICE, typename PRECISION>
void RMSECPU<DEVICE, PRECISION>::set_a(
    const Kokkos::View<const int**, Kokkos::LayoutLeft, typename DEVICE::memory_space>& models,
    int taskind,
    int start,
    int batch_size)
{
    CHECK_GREATER_EQUAL(_descriptor_matrix.extent(0), _task_sizes[taskind] + start, "");

    int num_features = static_cast<int>(models.extent(0));

    for (int model_idx = 0; model_idx < batch_size; ++model_idx)
    {
        for (auto feature_idx = 0; feature_idx < num_features; ++feature_idx)
        {
            std::copy_n(&_descriptor_matrix(start, models(feature_idx, model_idx)),
                        _task_sizes[taskind],
                        &_a(0, feature_idx, model_idx));
        }
        if (num_features != _n_dim)
        {
            std::fill_n(&_a(0, num_features, model_idx), _task_sizes[taskind], 1);
        }
    }
}

template <typename DEVICE, typename PRECISION>
void RMSECPU<DEVICE, PRECISION>::set_b(int taskind, int start, int batch_size)
{
    for (int batch_idx = 0; batch_idx < batch_size; ++batch_idx)
    {
        std::copy_n(&_properties(start), _task_sizes[taskind], &_b(0, batch_idx));
    }
}

template <typename DEVICE, typename PRECISION>
int RMSECPU<DEVICE, PRECISION>::least_squares(int taskind, int start, int batch_size)
{
    int info = -100;

    for (auto batch_idx = 0; batch_idx < batch_size; ++batch_idx)
    {
        gels('N',
             _task_sizes[taskind],
             _n_dim,
             1,
             &_a(0, 0, batch_idx),
             _a.stride_1(),
             &_b(0, batch_idx),
             _task_sizes[taskind],
             work.data(),
             lwork,
             &info);
    }

    return info;
}

template <typename DEVICE, typename PRECISION>
void RMSECPU<DEVICE, PRECISION>::set_prop_train_est(BatchedVector estimated_training_properties,
                                                    int taskind,
                                                    int start,
                                                    int batch_size)
{
    assert(estimated_training_properties.extent(0) >= start + _task_sizes[taskind]);
    assert(estimated_training_properties.extent(1) <= _max_batches);

    for (int model_idx = 0; model_idx < batch_size; ++model_idx)
    {
        gemv('N',
             _task_sizes[taskind],
             _n_dim,
             1,
             &_a(0, 0, model_idx),
             _a.stride_1(),
             &_b(0, model_idx),
             1,
             0,
             &estimated_training_properties(start, model_idx),
             1);
    }
}

template <typename DEVICE, typename PRECISION>
void RMSECPU<DEVICE, PRECISION>::get_mean_squared_difference(const Vector& output,
                                                             const Vector& input1,
                                                             const Matrix& input2,
                                                             const int64_t batch_offset,
                                                             const int64_t batch_size)
{
    CHECK_GREATER_EQUAL(output.extent(0), batch_offset + batch_size, "");
    CHECK_EQUAL(input1.extent(0), input2.extent(0), "");
    CHECK_GREATER_EQUAL(input2.extent(1), batch_size, "");

    for (int batch_idx = 0; batch_idx < batch_size; ++batch_idx)
    {
        auto a = &input1(0);
        auto b = &input2(0, batch_idx);
        PRECISION squared_difference = 0;

#pragma omp simd reduction(+ : squared_difference)
        for (auto idx = 0; idx < input1.extent(0); ++idx)
        {
            squared_difference += (a[idx] - b[idx]) * (a[idx] - b[idx]);
        }

        output(batch_offset + batch_idx) = std::sqrt(squared_difference /
                                                     static_cast<PRECISION>(input1.extent(0)));
    }
}
