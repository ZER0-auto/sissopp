// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @brief Defines the class that uses a Pearson correlation projection operator and a least-squares regression objective function
 *
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include "utils/KokkosDefines.hpp"

#ifndef SISSO_ENABLE_GPU
#include "RMSECPU.hpp"

template <typename DEVICE, typename PRECISION = double>
using RMSEGPU = RMSECPU<DEVICE, PRECISION>;

#else

#include <Kokkos_Core.hpp>
#include <type_traits>

#include "loss_function/LossFunction.hpp"
#include "utils/DescriptorMatrix.hpp"
#include "utils/PropertiesVector.hpp"

// DocString: cls_loss_function_pearson_rmse
/**
 * @brief The loss function used for regression problems on GPU
 *
 */
template <typename DEVICE, typename PRECISION = double>
class RMSEGPU
{
public:
    using MemorySpace = typename DEVICE::memory_space;
    using ExecutionSpace = typename DEVICE::execution_space;
    using BatchedMatrix = Kokkos::View<PRECISION***, Kokkos::LayoutLeft, MemorySpace>;
    using BatchedVector = Kokkos::View<PRECISION**, Kokkos::LayoutLeft, MemorySpace>;
    using BatchedScalar = Kokkos::View<PRECISION*, Kokkos::LayoutLeft, MemorySpace>;
    using Matrix = Kokkos::View<PRECISION**, Kokkos::LayoutLeft, MemorySpace>;
    using Vector = Kokkos::View<PRECISION*, Kokkos::LayoutLeft, MemorySpace>;
    using Scalar = Kokkos::View<PRECISION, Kokkos::LayoutLeft, MemorySpace>;
    using ModelBatch = Kokkos::View<int**, Kokkos::LayoutLeft, MemorySpace>;
    using TaskSizes = Kokkos::View<int*, Kokkos::LayoutLeft, MemorySpace>;

private:
    Matrix _descriptor_matrix;
    Vector _properties;

    bool _fix_intercept;  //!< If true then the bias term is fixed at 0
    int _n_feat;          //!< Number features in the linear model
    int _n_dim;           //!< Total number of constants to fit (scale and bias terms)
    int _n_samp;          //!< Number of samples in the training set
    int _n_task;          //!< Number of tasks
    int _max_batches;

    TaskSizes _task_sizes;
    /// dim 0: material samples
    /// dim 1: features
    /// dim 2: batch
    BatchedMatrix _a;
    /// dim 0: material properties
    /// dim 1: batch
    BatchedVector _b;
    BatchedScalar _batched_scores;
    ModelBatch _models;

public:
    /**
     * @brief Constructor
     *
     * @param descriptor_matrix descriptor matrix
     * @param properties properties vector
     * @param task_sizes number of items per task
     * @param fix_intercept use a fixed offset?
     * @param n_feat Number features in the linear model
     */
    RMSEGPU(const int max_batches,
            const Matrix& descriptor_matrix,
            const Vector& properties,
            const std::vector<int>& task_sizes,
            bool fix_intercept = false,
            int n_feat = 1);

    RMSEGPU(const RMSEGPU& rmse) = delete;
    RMSEGPU& operator=(const RMSEGPU& rmse) = delete;

    ~RMSEGPU() = default;

    /**
     * @brief Evaluate the loss function for a set of features
     *
     * @param feature_indices index tuples pointing into the descriptor matrix
     * @param num_models number of models if size of feature_indices is too large
     * @return Final score for every index tuple
     */
    Vector operator()(const ModelBatch& feature_indices, int64_t num_models);

    void score_models(ModelBatch models,
                      Matrix descriptorMatrix,
                      Vector properties,
                      BatchedMatrix scratchMatrices,
                      BatchedVector scratchRHS,
                      BatchedScalar batched_scores,
                      const int numModels,
                      const int numFeatures,
                      const int numSamples,
                      typename RMSEGPU<DEVICE, PRECISION>::TaskSizes taskSizes,
                      const int n_dim);
};

template <typename DEVICE, typename PRECISION>
RMSEGPU<DEVICE, PRECISION>::RMSEGPU(const int max_batches,
                                    const Matrix& descriptor_matrix,
                                    const Vector& properties,
                                    const std::vector<int>& task_sizes,
                                    bool fix_intercept,
                                    int n_feat)
    : _descriptor_matrix(descriptor_matrix),
      _properties(properties),
      _task_sizes("task_sizes", task_sizes.size()),
      _fix_intercept(fix_intercept),
      _n_feat(n_feat),
      _n_dim(n_feat + (!fix_intercept)),
      _n_samp(std::accumulate(task_sizes.begin(), task_sizes.end(), 0)),
      _n_task(task_sizes.size()),
      _max_batches(max_batches),
      _a("A", _n_samp, _n_dim, max_batches),
      _b("b", _n_samp, max_batches),
      _batched_scores("batched_scores", max_batches),
      _models("models", n_feat, max_batches)
{
    for (size_t task_idx = 0; task_idx < task_sizes.size(); ++task_idx)
    {
        _task_sizes(task_idx) = task_sizes[task_idx];
    }
}

/**
 * Returns the socres of all models.
 *
 * @code
foreach model
  assemble RHS and matrix in scratch space
  foreach task
    qr using Housholder reflections
    least squares solution
  calculate mean deviation
 * @endcode
 */
template <typename DEVICE, typename PRECISION>
void RMSEGPU<DEVICE, PRECISION>::score_models(
    ModelBatch models,
    Matrix descriptorMatrix,
    Vector properties,
    BatchedMatrix scratchMatrices,
    BatchedVector scratchRHS,
    BatchedScalar batched_scores,
    const int numModels,
    const int numFeatures,
    const int numSamples,
    typename RMSEGPU<DEVICE, PRECISION>::TaskSizes taskSizes,
    const int n_dim)
{
    using POLICY_TYPE = Kokkos::TeamPolicy<>;
    auto scratch_memory_requirement = sizeof(PRECISION) * numSamples;
    auto scratch_memory_level = scratch_memory_requirement < POLICY_TYPE::scratch_size_max(0) * 0.8
                                    ? 0
                                    : 1;
    if (scratch_memory_requirement > POLICY_TYPE::scratch_size_max(1) * 0.8)
    {
        // out of scratch memory error
        std::cerr << fmt::format("out of scratch memory: {} / {}",
                                 scratch_memory_requirement,
                                 POLICY_TYPE::scratch_size_max(1) * 0.8);
        exit(EXIT_FAILURE);
    }
    auto policy = POLICY_TYPE(numModels, 256)
                      .set_scratch_size(scratch_memory_level,
                                        Kokkos::PerTeam(scratch_memory_requirement));
    auto kernel = KOKKOS_LAMBDA(const typename POLICY_TYPE::member_type& team)
    {
        using namespace Kokkos;
        int ts = team.team_size();     // returns TEAM_SIZE
        int tid = team.team_rank();    // returns a number between 0 and TEAM_SIZE
        int ls = team.league_size();   // returns N
        int lid = team.league_rank();  // returns a number between 0 and N

        const int stride = team.team_size();
        const int batchIdx = team.league_rank();

        /// loops over all samples
        auto all_policy = Kokkos::TeamThreadRange(team, 0, numSamples);

        PRECISION* const A = &scratchMatrices(0, 0, batchIdx);
        const int lda = scratchMatrices.stride_1();
        PRECISION* const b = &scratchRHS(0, batchIdx);
        auto v = Kokkos::View<PRECISION*, POLICY_TYPE::execution_space::scratch_memory_space>(
            team.team_scratch(0), numSamples);
        int* const model = &models(0, batchIdx);

        // assemble scratch matrices and rhs
        parallel_for(all_policy,
                     [&](const int& rowIdx)
                     {
                         for (auto feature_idx = 0; feature_idx < numFeatures; ++feature_idx)
                         {
                             A[rowIdx + lda * feature_idx] = descriptorMatrix(rowIdx,
                                                                              model[feature_idx]);
                         }
                         if (numFeatures != n_dim)
                         {
                             A[rowIdx + lda * numFeatures] = 1;
                         }
                         b[rowIdx] = properties(rowIdx);
                     });

        team.team_barrier();

        int start = 0;
        for (int task_idx = 0; task_idx < taskSizes.extent(0); ++task_idx)
        {
            /// loops over all samples within this task
            auto task_policy = Kokkos::TeamThreadRange(team, start, start + taskSizes[task_idx]);

            for (auto columnIdx = 0; columnIdx < n_dim; ++columnIdx)
            {
                // copy column into v
                parallel_for(
                    task_policy,
                    [&](const int& rowIdx) {
                        v[rowIdx] = rowIdx - start >= columnIdx ? A[rowIdx + columnIdx * lda] : 0.0;
                    });

                // modify diagonal element
                PRECISION sumSquared = 0;
                parallel_reduce(
                    task_policy,
                    [&](const int& rowIdx, PRECISION& sum) { sum += v(rowIdx) * v(rowIdx); },
                    Kokkos::Sum<PRECISION>(sumSquared));
                parallel_for(
                    task_policy,
                    [&](const int& rowIdx)
                    {
                        if (rowIdx - start == columnIdx)  // only one element needs to be changed
                        {
                            v[rowIdx] += (v[rowIdx] >= 0 ? 1.0 : -1.0) * Kokkos::sqrt(sumSquared);
                        }
                    });

                // normalize
                sumSquared = 0;
                parallel_reduce(
                    task_policy,
                    [&](const int& rowIdx, PRECISION& sum) { sum += v(rowIdx) * v(rowIdx); },
                    Kokkos::Sum<PRECISION>(sumSquared));
                auto invNorm = 1.0 / Kokkos::sqrt(sumSquared);
                parallel_for(task_policy, [&](const int& rowIdx) { v[rowIdx] *= invNorm; });

                // calc Q* b
                PRECISION dotB = 0;
                parallel_reduce(
                    task_policy,
                    [&](const int& rowIdx, PRECISION& sum) { sum += v(rowIdx) * b[rowIdx]; },
                    Kokkos::Sum<PRECISION>(dotB));
                parallel_for(task_policy,
                             [&](const int& rowIdx)
                             {
                                 if (rowIdx - start >= columnIdx)
                                 {
                                     b[rowIdx] -= 2.0 * v[rowIdx] * dotB;
                                 }
                             });

                for (auto innerColumnIdx = columnIdx; innerColumnIdx < n_dim; ++innerColumnIdx)
                {
                    // dot product to get length
                    PRECISION v_dot_A = 0;
                    parallel_reduce(
                        task_policy,
                        [&](const int& rowIdx, PRECISION& sum)
                        { sum += v(rowIdx) * A[rowIdx + innerColumnIdx * lda]; },
                        Kokkos::Sum<PRECISION>(v_dot_A));

                    // subtract twice
                    parallel_for(task_policy,
                                 [&](const int& rowIdx) {
                                     A[rowIdx + innerColumnIdx * lda] -= 2.0 * v[rowIdx] * v_dot_A;
                                 });
                }
            }

            // back substitution
            for (auto columnIdx = n_dim - 1; columnIdx >= 0; --columnIdx)
            {
                parallel_for(task_policy,
                             [&](const int& rowIdx)
                             {
                                 if (rowIdx - start == columnIdx)
                                 {
                                     b[rowIdx] /= A[rowIdx + columnIdx * lda];
                                 }
                             });
                team.team_barrier();
                parallel_for(task_policy,
                             [&](const int& rowIdx)
                             {
                                 if (rowIdx - start < columnIdx)
                                 {
                                     b[rowIdx] -= b[columnIdx + start] *
                                                  A[rowIdx + columnIdx * lda];
                                 }
                             });
            }

            // calculate estimated
            parallel_for(task_policy,
                         [&](const int& rowIdx)
                         {
                             PRECISION sum = 0;
                             for (auto feature_idx = 0; feature_idx < numFeatures; ++feature_idx)
                             {
                                 sum += descriptorMatrix(rowIdx, model[feature_idx]) *
                                        b[feature_idx + start];
                             }
                             if (numFeatures != n_dim)
                             {
                                 sum += b[numFeatures + start];
                             }
                             v[rowIdx] = sum;
                         });

            start += taskSizes(task_idx);
        }

        team.team_barrier();

        PRECISION squared_difference = 0;
        parallel_reduce(
            all_policy,
            [&](const int rowIdx, PRECISION& innerUpdate)
            { innerUpdate += (v[rowIdx] - properties(rowIdx)) * (v[rowIdx] - properties(rowIdx)); },
            squared_difference);

        team.team_barrier();
        if (team.team_rank() == 0)
            batched_scores(batchIdx) = Kokkos::sqrt(squared_difference /
                                                    static_cast<PRECISION>(properties.extent(0)));
    };
    Kokkos::parallel_for("score_models", policy, kernel);
}

template <typename DEVICE, typename PRECISION>
typename RMSEGPU<DEVICE, PRECISION>::Vector RMSEGPU<DEVICE, PRECISION>::operator()(
    const ModelBatch& feature_indices, int64_t num_models)
{
    const int input_batch_size = static_cast<int>(num_models);
    // If output view is too small, we need to resize it.
    if (_batched_scores.extent(0) < input_batch_size)
        Kokkos::resize(_batched_scores, input_batch_size);

    // Handle models in batches to fit into allocated scratch space.
    int batch_offset = 0;
    while (batch_offset < input_batch_size)
    {
        auto processed_batch_size = std::min(_max_batches, input_batch_size - batch_offset);

        score_models(
            ModelBatch(feature_indices,
                       Kokkos::ALL(),
                       std::make_pair<size_t, size_t>(batch_offset, feature_indices.extent(1))),
            _descriptor_matrix,
            _properties,
            _a,
            _b,
            BatchedScalar(_batched_scores,
                          std::make_pair<size_t, size_t>(batch_offset, _batched_scores.extent(0))),
            processed_batch_size,
            feature_indices.extent(0),
            _n_samp,
            _task_sizes,
            _n_dim);

        batch_offset += processed_batch_size;
    }

    Kokkos::fence();
    return _batched_scores;
}

#endif
