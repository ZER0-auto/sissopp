// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file mpi_interface/MPI_Interface.hpp
 *  @brief Implements the MPI interface for the calculations
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This class modifies the boost mpi::communicator for easier communication
 */

#include "mpi_interface/MPI_Interface.hpp"

MPI_Interface::MPI_Interface() : boost::mpi::communicator() {}

std::array<int, 2> MPI_Interface::get_start_end_from_list(const int sz, const int start)
{
    int els_per_rank = sz / size();
    int remaineder = sz % size();

    std::array<int, 2> start_end;
    start_end[0] = start + els_per_rank * rank() + std::min(rank(), remaineder);
    start_end[1] = start + els_per_rank * (rank() + 1) + std::min(rank() + 1, remaineder);

    return start_end;
}

std::shared_ptr<MPI_Interface> mpi_setup::comm;

void mpi_setup::init_mpi_env()
{
    if (env == 0)
    {
#ifdef BOOST_MPI_HAS_NOARG_INITIALIZATION
        env = new boost::mpi::environment();
        comm = std::make_shared<MPI_Interface>();
#else
        throw std::runtime_error("MPI cannot be initialized without arguments");
#endif
    }
}

void mpi_setup::finalize_mpi_env()
{
    delete env;
    env = 0;
}
