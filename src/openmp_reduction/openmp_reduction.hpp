// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file utils/ScObjPair.hpp
 *  @brief Functions to be used in OpenMP
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef UTILS_OPENMP_RED
#define UTILS_OPENMP_RED

#include <vector>

namespace openmp_red
{
template <typename T>
std::vector<T> n_max(std::vector<T> omp_out, std::vector<T> omp_in)
{
    int N = omp_out.size();

    omp_out.insert(omp_out.end(), omp_in.begin(), omp_in.end());
    std::sort(omp_out.begin(), omp_out.end());

    omp_out.resize(N);
    return omp_out;
}

template <typename T>
std::vector<T> merge(std::vector<T> omp_out, std::vector<T> omp_in)
{
    omp_out.insert(omp_out.end(), omp_in.begin(), omp_in.end());
    return omp_out;
}
}  // namespace openmp_red
#endif
