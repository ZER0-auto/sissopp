// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include "KSmallest.hpp"
#include "ModelSet.hpp"
#include "loss_function/RMSECPU.hpp"
#include "loss_function/RMSEGPU.hpp"
#include "openmp_reduction/ScoreObjectPair.hpp"
#include "utils/DescriptorMatrix.hpp"
#include "utils/EnumerateUniqueCombinations.hpp"
#include "utils/KokkosDefines.hpp"
#include "utils/PropertiesVector.hpp"

template <typename DEVICE, typename PRECISION>
using RMSEDevice = std::conditional_t<std::is_same_v<DEVICE, GPUDevice>,
                                      RMSEGPU<DEVICE, PRECISION>,
                                      RMSECPU<DEVICE, PRECISION>>;

class ISelectBestFeatures
{
public:
    ISelectBestFeatures(int64_t num_best_models) : best_feature_combinations(num_best_models) {}
    virtual ~ISelectBestFeatures() = default;

    virtual void evaluate(const inds_sc_pair& lower_dimensional_model,
                          int64_t num_features,
                          int64_t package_idx) = 0;
    virtual void evaluate(const EnumerateUniqueCombinations& feature_combinations,
                          int batch_size,
                          int increment,
                          int64_t package_idx) = 0;

    KSmallest<inds_sc_pair> best_feature_combinations;
};

template <typename DEVICE, typename PRECISION>
class SelectBestFeatures : public ISelectBestFeatures
{
public:
    using Matrix = typename DescriptorMatrix<typename DEVICE::memory_space, PRECISION>::Matrix;
    using Vector = typename PropertiesVector<typename DEVICE::memory_space, PRECISION>::Vector;

    /**
     * @param num_best_models How many models should be saved?
     * @param batch_size How many models are processed in one go?
     * @param chunk_size How many models are processed in this work package?
     */
    SelectBestFeatures(int64_t num_best_models,
                       int64_t batch_size,
                       int64_t chunk_size,
                       const Matrix& descriptor_matrix,
                       const Vector& properties,
                       const std::vector<int>& task_sizes,
                       bool fix_intercept,
                       int n_feat)
        : ISelectBestFeatures(num_best_models),
          _model_set(n_feat, chunk_size),
          loss(batch_size, descriptor_matrix, properties, task_sizes, fix_intercept, n_feat)
    {
    }

    void evaluate(const inds_sc_pair& lower_dimensional_model,
                  int64_t num_features,
                  int64_t package_idx) override
    {
        auto num_models = _model_set.generate_new_models(lower_dimensional_model, num_features);
        auto scores = loss(_model_set._models, num_models);
        rank_models(scores, num_models);
    }

    void evaluate(const EnumerateUniqueCombinations& feature_combinations,
                  int batch_size,
                  int increment,
                  int64_t package_idx) override
    {
        auto num_models = _model_set.generate_new_models(
            feature_combinations, batch_size, increment);
        auto scores = loss(_model_set._models, num_models);
        rank_models(scores, num_models);
    }

    ModelSet<DEVICE> _model_set;
    RMSEDevice<DEVICE, PRECISION> loss;

private:
    void rank_models(const typename RMSEDevice<DEVICE, PRECISION>::Vector& scores, const int64_t& num_models)
    {
        CHECK_GREATER_EQUAL(scores.extent(0), num_models, "");
        CHECK_GREATER_EQUAL(_model_set._models.extent(1), num_models, "");

        std::vector<int> vec(_model_set._models.extent(0), 0);
        inds_sc_pair tmp(vec, 0);
        for (size_t model_idx = 0; model_idx < num_models; ++model_idx)
        {
            for (auto featureIdx = 0; featureIdx < _model_set._models.extent(0); ++featureIdx)
                vec[featureIdx] = _model_set._models(featureIdx, model_idx);
            tmp.set_obj(vec);
            tmp.set_score(scores[model_idx]);
            best_feature_combinations.insert(tmp);
        }
    }
};

template <typename PRECISION>
using SelectBestFeaturesCPU = SelectBestFeatures<CPUDevice, PRECISION>;

template <typename PRECISION>
using SelectBestFeaturesGPU = SelectBestFeatures<GPUDevice, PRECISION>;
