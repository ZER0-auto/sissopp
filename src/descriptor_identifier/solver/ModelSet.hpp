// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include "openmp_reduction/ScoreObjectPair.hpp"
#include "utils/EnumerateUniqueCombinations.hpp"
#include "utils/KokkosDefines.hpp"
#include "utils/PropertiesVector.hpp"
#include "utils/assumption.hpp"

template <typename DEVICE>
class ModelSet
{
public:
    ModelSet(int64_t num_features, int64_t num_models) : _models("models", num_features, num_models)
    {
    }

    /**
     * Generate new models from a previous lower dimensional model by adding another dimension.
     *
     * All possible features will be tried for the new dimension. If the feature is already present in the model
     * it will be skipped.
     *
     * @return Number of models generated. Can be less than the size of _models!
     */
    int64_t generate_new_models(const inds_sc_pair& lower_dimensional_model, int64_t num_features)
    {
        CHECK_EQUAL(lower_dimensional_model.obj().size() + 1, _models.extent(0), "");
        CHECK_LESS_EQUAL(num_features, _models.extent(1), "");

        int64_t counter = 0;
        for (auto feature = 0; feature < num_features; ++feature)
        {
            if (std::find(lower_dimensional_model.obj().cbegin(),
                          lower_dimensional_model.obj().cend(),
                          feature) == lower_dimensional_model.obj().cend())
            {
                for (auto featureIdx = 0; featureIdx < _models.extent(0) - 1; ++featureIdx)
                {
                    _models(featureIdx, counter) = lower_dimensional_model.obj()[featureIdx];
                }
                _models(_models.extent(0) - 1, counter) = feature;
                ++counter;
            }
        }
        return counter;
    }

    /**
     * Generate new models by enumerating all possible combinations.
     *
     * @param batch_size Number of models that should be generated.
     * @param increment How much should feature_combinations in between models.
     * @return Number of models generated. Can be less than the size of _models!
     */
    int64_t generate_new_models(EnumerateUniqueCombinations feature_combinations,
                                int batch_size,
                                int increment)
    {
        CHECK_EQUAL(feature_combinations.get_current_combination().size(), _models.extent(0), "");
        CHECK_LESS_EQUAL(batch_size, _models.extent(1), "");
        int64_t counter = 0;
        for (counter = 0; counter < batch_size; ++counter)
        {
            if (feature_combinations.is_finished()) break;
            for (auto featureIdx = 0; featureIdx < _models.extent(0); ++featureIdx)
            {
                _models(featureIdx,
                        counter) = feature_combinations.get_current_combination()[featureIdx];
            }
            feature_combinations += increment;
        }
        return counter;
    }

    Kokkos::View<int**, Kokkos::LayoutLeft, typename DEVICE::memory_space> _models;
};
