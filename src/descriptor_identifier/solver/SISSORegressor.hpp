// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file descriptor_identifier/SISSORegressor.hpp
 *  @brief Defines a class to solve regression problems with SISSO
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef SISSO_REGRESSOR
#define SISSO_REGRESSOR

#include "descriptor_identifier/model/ModelRegressor.hpp"
#include "descriptor_identifier/solver/SISSOSolver.hpp"

#ifdef PY_BINDINGS
namespace py = pybind11;
#endif

// DocString: cls_sisso_reg
/**
 * @brief The solver for regression problems
 *
 * @details A slover that uses the least-squares regression to find the best low-dimensional linear model for the DI step (inherits from SISSOSolver)
 */
class SISSORegressor : public SISSOSolver
{
private:
    // clang-format off
    std::vector<std::vector<ModelRegressor>> _models; //!< List of models
    // clang-format on

public:
    // DocString: sisso_reg_init
    /**
     * @brief Constructor for the regressor
     *
     * @param inputs (Inputs) The InputParser object for the calculation
     * @param feat_space (FeatureSpace) Feature Space for the problem
     */
    SISSORegressor(const InputParser inputs, const std::shared_ptr<FeatureSpace> feat_space);

    /**
     * @brief Constructor for the SISSORegressor from unpickling
     *
     * @param sample_ids_train Vector storing all sample ids for the training samples
     * @param sample_ids_test Vector storing all sample ids for the test samples
     * @param task_names Vector storing the ID of the task names
     * @param task_sizes_train Number of training samples per task
     * @param task_sizes_test Number of testing samples per task
     * @param leave_out_inds List of indexes from the initial data file in the test set
     * @param prop_unit The Unit of the property
     * @param prop_label The label of the property
     * @param prop_train The property for all training samples
     * @param prop_test The property for all the test samples
     * @param feat_space Feature Space for the problem
     * @param n_dim The maximum number of features allowed in the linear model
     * @param n_residual Number of residuals to pass to the next sis model
     * @param n_models_store The number of models to output to files
     * @param fix_intercept If true the bias term is fixed at 0
     * @param models The final models of the analysis
     * @param calc_type The type of calculation
     */
    SISSORegressor(std::vector<std::string> sample_ids_train,
                   std::vector<std::string> sample_ids_test,
                   std::vector<std::string> task_names,
                   std::vector<int> task_sizes_train,
                   std::vector<int> task_sizes_test,
                   std::vector<int> leave_out_inds,
                   Unit prop_unit,
                   std::string prop_label,
                   std::vector<double> prop_train,
                   std::vector<double> prop_test,
                   std::shared_ptr<FeatureSpace> feat_space,
                   int n_dim,
                   int n_residual,
                   int n_models_store,
                   bool fix_intercept,
                   std::vector<std::vector<ModelRegressor>> models =
                       std::vector<std::vector<ModelRegressor>>(),
                   std::string calc_type = "regression");

    virtual ~SISSORegressor() = default;

    /**
     * @brief Returns the current dimension
     * @return current dimension
     */
    virtual inline int cur_dim() override {return _models.size() + 1;}

    /**
     * @brief Create a Model for a given set of features and store them in _models
     *
     * @param indexes Vector storing all of the indexes of features in _feat_space->phi_selected() to use for the model
     */
    virtual void add_models(const std::vector<std::vector<int>> indexes) override;

    /**
     * @brief Add Models from generated output files
     *
     * @param train_files list of all train files
     * @param test_files list of all test files
     */
    virtual void add_models(std::vector<std::vector<std::string>> train_files,
                            std::vector<std::vector<std::string>> test_files) override;

    /**
     * @brief Output the models to files and copy the residuals
     */
    virtual void output_models() override;

    virtual std::vector<std::shared_ptr<Model>> get_cur_modles() override;

    /**
     * @brief Set the min_scores and min_inds vectors given a score and max_error_ind
     *
     * @param inds The current set of indexes
     * @param score The score for the current set of indexes
     * @param max_error_ind The current index of the maximum score among the best models
     * @param min_sc_inds Current list of the indexes that make the best models and their respective scores
     */
    void update_min_inds_scores(const std::vector<int>& inds,
                                double score,
                                int max_error_ind,
                                std::vector<inds_sc_pair>& min_sc_inds) override;

    // DocString: sisso_reg_models_py
    /**
     * @brief The selected models (n_dim, n_models_store)
     */
    inline std::vector<std::vector<ModelRegressor>> models() const { return _models; }
};

// GCOV_EXCL_START      GCOVR_EXCL_START        LCOV_EXCL_START
#ifdef PY_BINDINGS
namespace py = pybind11;
template <class SISSORegressorBase = SISSORegressor>
class PySISSORegressor : public PySISSOSolver<SISSORegressorBase>
{
public:
    using PySISSOSolver<SISSORegressorBase>::PySISSOSolver;

    int cur_dim() override
    {
        PYBIND11_OVERRIDE(int, SISSORegressorBase, cur_dim,);
    }

    void output_models() override { PYBIND11_OVERRIDE(void, SISSORegressorBase, output_models, ); }

    void update_min_inds_scores(const std::vector<int>& inds,
                                double score,
                                int max_error_ind,
                                std::vector<inds_sc_pair>& min_sc_inds) override
    {
        PYBIND11_OVERRIDE(void,
                          SISSORegressorBase,
                          update_min_inds_scores,
                          inds,
                          score,
                          max_error_ind,
                          min_sc_inds);
    }

    void add_models(const std::vector<std::vector<int>> indexes) override
    {
        PYBIND11_OVERRIDE(void, SISSORegressorBase, add_models, indexes);
    }

    void add_models(std::vector<std::vector<std::string>> train_files,
                    std::vector<std::vector<std::string>> test_files) override
    {
        PYBIND11_OVERRIDE(void, SISSORegressorBase, add_models, train_files, test_files);
    }

    std::vector<std::shared_ptr<Model>> get_cur_modles() override
    {
        PYBIND11_OVERRIDE(std::vector<std::shared_ptr<Model>>, SISSORegressorBase, get_cur_modles, );
    }
};
#endif
// GCOV_EXCL_STOP      GCOVR_EXCL_STOP        LCOV_EXCL_STOP

#endif
