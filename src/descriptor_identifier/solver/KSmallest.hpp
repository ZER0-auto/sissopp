// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include <cstdint>
#include <vector>

#include "utils/assumption.hpp"

/**
 * Keep the k smallest elements inserted into this container.
 */
template <typename T>
class KSmallest
{
public:
    KSmallest(const int64_t k) : _max_items(k), _max_idx(0)
    {
        CHECK_GREATER(k, 0, "at least one element is needed");
        _items.reserve(k);
    }
    bool insert(const T& item)
    {
        if (_items.size() < _max_items)
        {
            _items.push_back(item);
            _max_idx = std::max_element(_items.begin(), _items.end()) - _items.begin();
            return true;
        }
        if (_items[_max_idx] > item)
        {
            _items[_max_idx] = item;
            _max_idx = std::max_element(_items.begin(), _items.end()) - _items.begin();
            return true;
        }
        return false;
    }
    /**
     * Merge other into this instance.
     */
    void merge(const KSmallest& other)
    {
        const auto& vec = other.data();
        for (const auto& e : vec)
        {
            insert(e);
        }
    }
    const auto& data() const { return _items; }

private:
    int64_t _max_items;
    int64_t _max_idx;
    std::vector<T> _items;
};