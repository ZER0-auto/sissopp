// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file main.cpp
 *  @brief The main executable used to run SISSO++
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include <Kokkos_Core.hpp>

#ifdef PROFILE_W_CALIPER
#include <caliper/cali.h>
#endif

#include "descriptor_identifier/solver/SISSOClassifier.hpp"
#include "descriptor_identifier/solver/SISSOLogRegressor.hpp"
#include "inputs/InputParser.hpp"

int main(int argc, char* argv[])
{
#ifdef PROFILE_W_CALIPER
    CALI_CXX_MARK_FUNCTION;
#endif

    allowed_op_maps::set_node_maps();
#ifdef PARAMETERIZE
    allowed_op_maps::set_param_node_maps();
#endif

    mpi_setup::init_mpi_env();

    Kokkos::ScopeGuard kokkos(argc, argv);

    std::string filename;
    double duration = 0.0;
    if (argc < 2)
    {
        filename = "sisso.json";
    }
    else
    {
        filename = argv[1];
    }

    double start = omp_get_wtime();
    InputParser inputs(filename);
    mpi_setup::comm->barrier();
    duration = omp_get_wtime() - start;
    if (inputs.verbose() && (mpi_setup::comm->rank() == 0))
    {
        std::cout << "time input_parsing: " << duration << " s" << std::endl;
    }

    std::shared_ptr<FeatureSpace> feat_space = nullptr;
    if (inputs.restart())
    {
        feat_space = std::make_shared<FeatureSpace>(inputs.phi_out_file(),
                                                    inputs.phi_0_ptrs(),
                                                    inputs.prop_train_copy(),
                                                    inputs.task_sizes_train_copy(),
                                                    inputs.calc_type(),
                                                    inputs.n_sis_select(),
                                                    inputs.cross_cor_max(),
                                                    std::vector<int>(),
                                                    inputs.override_n_sis_select(),
                                                    true);

        if (feat_space->phi().size() <= feat_space->phi0().size())
        {
            feat_space = std::make_shared<FeatureSpace>(inputs);
            node_value_arrs::initialize_d_matrix_arr();
        }
        else
        {
            node_value_arrs::initialize_d_matrix_arr();
            std::vector<int> excluded_inds = {};
            std::vector<node_ptr> phi_sel = str2node::phi_selected_from_file("feature_space/selected_features.txt", feat_space->phi0(), excluded_inds);
            feat_space->set_phi_selected(phi_sel);
        }
    }
    else
    {
        feat_space = std::make_shared<FeatureSpace>(inputs);
        node_value_arrs::initialize_d_matrix_arr();
    }
    std::vector<std::string> model_file_paths;
    std::vector<std::vector<std::string>> model_train_file_paths(inputs.n_dim());
    std::vector<std::vector<std::string>> model_test_file_paths(inputs.n_dim());
    if (inputs.restart())
    {
        const boost::filesystem::path models_path{"models"};
        for (auto const& dir_entry : boost::filesystem::directory_iterator{models_path})
        {
            model_file_paths.push_back(dir_entry.path().string());
        }
        std::sort(
            model_file_paths.begin(), model_file_paths.end(), [](std::string f1, std::string f2) {
                std::vector<std::string> split_1 = str_utils::split_string_trim(f1, "_/");
                std::vector<std::string> split_2 = str_utils::split_string_trim(f2, "_/");
                int i1 = std::stoi(split_1[3]) * 100000000 + std::stoi(split_1[5]) * 2 +
                         static_cast<int>(split_1[1].size() < 5);
                int i2 = std::stoi(split_2[3]) * 100000000 + std::stoi(split_2[5]) * 2 +
                         static_cast<int>(split_2[1].size() < 5);
                return i1 <= i2;
            });

        if(model_file_paths.size() > 1 && (model_file_paths[0].substr(0, 12) != model_file_paths[1].substr(0, 12)))
        {
            for (int mf = 0; mf < model_file_paths.size() / 2; ++mf)
            {
                std::vector<std::string> split_1 = str_utils::split_string_trim(
                    model_file_paths[2 * mf], "_/");
                std::vector<std::string> split_2 = str_utils::split_string_trim(
                    model_file_paths[2 * mf + 1], "_/");

                int d1 = std::stoi(split_1[3]) - 1;
                int d2 = std::stoi(split_2[3]) - 1;

                int mn1 = std::stoi(split_1[5]);
                int mn2 = std::stoi(split_2[5]);

                if (d1 != d2)
                {
                    throw std::logic_error(
                        "Dimension of the train and test files are not the same for files" +
                        model_file_paths[2 * mf] + "and" + model_file_paths[2 * mf + 1] + ".");
                }
                if (mn1 != mn2)
                {
                    throw std::logic_error(
                        "Model number of the train and test files are not the same for files" +
                        model_file_paths[2 * mf] + "and" + model_file_paths[2 * mf + 1] + ".");
                }
                model_train_file_paths[d1].push_back(model_file_paths[2 * mf]);
                model_test_file_paths[d2].push_back(model_file_paths[2 * mf + 1]);
            }

            for (auto& model_list : model_test_file_paths)
            {
                std::sort(model_list.begin(), model_list.end(), [](std::string f1, std::string f2) {
                    std::vector<std::string> split_1 = str_utils::split_string_trim(f1, "_/");
                    std::vector<std::string> split_2 = str_utils::split_string_trim(f2, "_/");
                    int i1 = std::stoi(split_1[5]);
                    int i2 = std::stoi(split_2[5]);
                    return i1 < i2;
                });
            }
        }
        else
        {
            model_test_file_paths.clear();
            for (int mf = 0; mf < model_train_file_paths.size(); ++mf)
            {
                std::vector<std::string> split_1 = str_utils::split_string_trim(
                    model_file_paths[mf], "_/");
                int d1 = std::stoi(split_1[3]) - 1;
                model_train_file_paths[d1].push_back(model_file_paths[mf]);
            }
        }
        for (auto& model_list : model_train_file_paths)
        {
            std::sort(model_list.begin(), model_list.end(), [](std::string f1, std::string f2) {
                std::vector<std::string> split_1 = str_utils::split_string_trim(f1, "_/");
                std::vector<std::string> split_2 = str_utils::split_string_trim(f2, "_/");
                int i1 = std::stoi(split_1[5]);
                int i2 = std::stoi(split_2[5]);
                return i1 < i2;
            });
        }
    }

    if ((inputs.calc_type().compare("regression") == 0) || (inputs.calc_type().compare("regression_gpu") == 0))
    {
        SISSORegressor sisso(inputs, feat_space);
        sisso.add_models(model_train_file_paths, model_test_file_paths);
        sisso.fit();

        if (mpi_setup::comm->rank() == 0)
        {
            for (size_t ii = 0; ii < sisso.models().size(); ++ii)
            {
                std::cout << "Train RMSE: " << sisso.models()[ii][0].rmse() << " "
                          << inputs.prop_unit();
                if (inputs.prop_test().size() > 0)
                {
                    std::cout << "; Test RMSE: " << sisso.models()[ii][0].test_rmse() << " "
                              << inputs.prop_unit() << std::endl;
                }
                else
                {
                    std::cout << std::endl;
                }
                std::cout << sisso.models()[ii][0] << "\n" << std::endl;
            }
        }
    }
    else if (inputs.calc_type().compare("log_regression") == 0)
    {
        SISSOLogRegressor sisso(inputs, feat_space);
        sisso.add_models(model_train_file_paths, model_test_file_paths);
        sisso.fit();

        if (mpi_setup::comm->rank() == 0)
        {
            for (size_t ii = 0; ii < sisso.models().size(); ++ii)
            {
                std::cout << "Train RMSE: " << sisso.models()[ii][0].rmse();
                if (inputs.prop_test().size() > 0)
                {
                    std::cout << "; Test RMSE: " << sisso.models()[ii][0].test_rmse() << std::endl;
                }
                else
                {
                    std::cout << std::endl;
                }
                std::cout << sisso.models()[ii][0] << "\n" << std::endl;
            }
        }
    }
    else if (inputs.calc_type().compare("classification") == 0)
    {
        SISSOClassifier sisso(inputs, feat_space);
        sisso.add_models(model_train_file_paths, model_test_file_paths);
        sisso.fit();

        if (mpi_setup::comm->rank() == 0)
        {
            for (size_t ii = 0; ii < sisso.models().size(); ++ii)
            {
                std::cout << "Percent of training data in the convex overlap region: "
                          << sisso.models()[ii][0].percent_train_error() << "%";
                if (inputs.prop_test().size() > 0)
                {
                    std::cout << "; Percent of test data in the convex overlap region: "
                              << sisso.models()[ii][0].percent_test_error() << "%" << std::endl;
                }
                else
                {
                    std::cout << std::endl;
                }
                std::cout << sisso.models()[ii][0] << "\n" << std::endl;
            }
        }
        prop_sorted_d_mat::finalize_sorted_d_matrix_arr();
        comp_feats::reset_vectors();
    }
    mpi_setup::finalize_mpi_env();
    return 0;
}
